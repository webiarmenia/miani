<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('users')->insert([
            'postal_code' =>'postal_code',
            'phone' =>'phone',
            'address' => 'address',
            'name' => 'miani',
            'surname' => 'miani',
            'email' => "miani@gmail.com",
            'password' => bcrypt('password'),
            'status' => 30,
            'provider'=>'provider',
            'provider_id'=>1
        ]);
    }
}
