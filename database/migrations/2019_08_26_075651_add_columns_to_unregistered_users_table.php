<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToUnregisteredUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('unregistered_users', function (Blueprint $table) {
            $table->string('name')->nullable(false);
            $table->string('surname')->nullable(false);
            $table->string('address')->nullable(false);
            $table->string('phone')->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('unregistered_users', function (Blueprint $table) {
            //
        });
    }
}
