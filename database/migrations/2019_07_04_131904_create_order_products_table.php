<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('product_details_id')->nullable(false);
            $table->integer('quantity')->nullable(false);
            $table->unsignedBigInteger('order_id')->nullable(false);
            $table->float('product_price')->nullable(false);
            $table->timestamps();
            $table->foreign('product_details_id')->references('id')->on('product_details');
            $table->foreign('order_id')->references('id')->on('orders');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_products',function (Blueprint $table){
            $table->dropForeign('order_products_product_details_id_foreign');
            $table->dropColumn('product_details_id');
            $table->dropForeign('order_products_order_id_foreign');
            $table->dropColumn('order_id');
        });
        Schema::dropIfExists('order_products');
    }
}
