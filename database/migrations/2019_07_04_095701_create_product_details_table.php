<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('product_id')->nullable(false);
            $table->float('rate')->nullable();
            $table->smallInteger('size')->nullable(false);
            $table->string('color')->nullable();
            $table->integer('measurment_type')->nullable(false);
            $table->integer('quantity')->nullable(false);
            $table->boolean('available')->nullable();
            $table->text('other_details')->nullable();
            $table->float('price')->nullable();
            $table->timestamps();
            $table->foreign('product_id')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_details',function (Blueprint $table){
            $table->dropForeign('product_details_product_id_foreign');
            $table->dropColumn('product_id');
        });
        Schema::dropIfExists('product_details');
    }
}
