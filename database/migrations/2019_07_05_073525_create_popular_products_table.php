<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePopularProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('popular_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('product_detail_id');
            $table->timestamps();
            $table->foreign('product_detail_id')->references('id')->on('product_details');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('popular_products',function (Blueprint $table){
            $table->dropForeign('popular_products_product_detail_id_foreign');
            $table->dropColumn('product_detail_id');
        });
        Schema::dropIfExists('popular_products');
    }
}
