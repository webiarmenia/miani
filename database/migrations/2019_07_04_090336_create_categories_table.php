<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable(false);
            $table->integer('cover_id');
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->string('icon')->nullable();
            $table->integer('order')->unique();
            $table->unsignedBigInteger('discount_id')->nullable();
            $table->timestamps();
            $table->foreign('parent_id')->references('id')->on('categories');
            $table->foreign('discount_id')->references('id')->on('discounts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories',function (Blueprint $table){
            $table->dropForeign('categories_parent_id_foreign');
            $table->dropColumn('parent_id');
            $table->dropForeign('categories_discount_id_foreign');
            $table->dropColumn('discount_id');
        });
        Schema::dropIfExists('categories');
    }
}
