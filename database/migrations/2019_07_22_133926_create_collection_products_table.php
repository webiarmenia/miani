<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollectionProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collection_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('collection_id');
            $table->unsignedBigInteger('product_detail_id');
            $table->boolean('top')->default(false);
            $table->foreign('collection_id')->references('id')->on('collections');
            $table->foreign('product_detail_id')->references('id')->on('product_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('collection_products',function (Blueprint $table){
            $table->dropForeign('collection_products_collection_id_foreign');
            $table->dropColumn('collection_id');
            $table->dropForeign('collection_products_product_detail_id_foreign');
            $table->dropColumn('product_detail_id');
        });
        Schema::dropIfExists('collection_products');
    }
}
