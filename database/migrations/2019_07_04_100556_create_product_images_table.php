<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('product_detail_id')->nullable(false);
            $table->string('image')->nullable(false);
            $table->timestamps();
            $table->foreign('product_detail_id')->references('id')->on('product_details');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_images',function (Blueprint $table){
            $table->dropForeign('product_images_product_detail_id_foreign');
            $table->dropColumn('product_detail_id');
        });
        Schema::dropIfExists('product_images');
    }
}
