<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscountConditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discount_conditions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('discount_id')->nullable(false);
            $table->string('condition_type')->nullable(false);
            $table->integer('condition_key')->nullable(false);
            $table->string('condition_value')->nullable(false);
            $table->string('additional')->nullable();
            $table->timestamps();
            $table->foreign('discount_id')->references('id')->on('discounts');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('discount_conditions',function (Blueprint $table){
            $table->dropForeign('discount_conditions_discount_id_foreign');
            $table->dropColumn('discount_id');
        });
        Schema::dropIfExists('discount_conditions');
    }
}
