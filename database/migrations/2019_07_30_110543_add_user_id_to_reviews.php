<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserIdToReviews extends Migration{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::table('reviews', function (Blueprint $table) {
            $table->bigInteger('nickname')->change();
            $table->renameColumn('nickname', 'user_id');
            $table->integer('rate')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::table('reviews', function (Blueprint $table) {
        });
    }
}
