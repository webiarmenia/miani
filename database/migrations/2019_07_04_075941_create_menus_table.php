<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable(false);
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->smallInteger('position')->nullable(false);
            $table->integer('order')->unique()->nullable(false);
            $table->timestamps();
            $table->foreign('parent_id')->references('id')->on('menus');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('menus',function (Blueprint $table){
            $table->dropForeign('menus_parent_id_foreign');
            $table->dropColumn('parent_id');
        });
        Schema::dropIfExists('menus');
    }
}
