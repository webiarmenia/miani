<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('client_id')->nullable();
            $table->string('client_email')->nullable(false);
            $table->float('total_price')->nullable(false);
            $table->boolean('approved')->nullable(false);
            $table->integer('status')->nullable(false);
            $table->timestamps();
            $table->foreign('client_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders',function (Blueprint $table){
            $table->dropForeign('orders_client_id_foreign');
            $table->dropColumn('client_id');
        });
        Schema::dropIfExists('orders');
    }
}
