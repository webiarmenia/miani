<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('miani')->namespace('Api')->group(function () {

    // Menus
//    Route::post('/get-menus','ApiController@getMenus');
    Route::get('/get-header-menus','ApiController@getHeaderMenus');
    Route::get('/get-footer-menus','ApiController@getFooterMenus');

    // Pages
    Route::get('/get-page','ApiController@getPage');

    //wish
    Route::post('/wish','ApiController@addWish');
    Route::post('/delete-wish','ApiController@deleteWish');


    Route::post('/get-wish','ApiController@getWish');

    //save Review
    Route::post('/review','ApiController@saveReview');

    //Product Reviews
    Route::get('/productReviews','ApiController@getProductReviews');

    // Orders
    Route::post('/order-create','ApiController@createOrder');

    // User
    Route::post('/user-create','ApiController@userRegister');
    Route::post('/user-create-facebook','ApiController@userRegisterFacebook');
    Route::post('/user-confirmed','ApiController@confirmed');
    Route::post('/user-update','ApiController@updateUserInfo');
    Route::post('/user-delete','ApiController@deleteUser');
    Route::post('/login','ApiController@userLogin');
    Route::post('/forgot-password','ApiController@forgotPassword');
    Route::post('/reset-password','ApiController@resetPassword');
    Route::get('/get-user','ApiController@userAllData');
    Route::post('/get-wishes','ApiController@getUserWish');
    Route::post('/get-order','ApiController@getUserOrders');

    // Subscribers
    Route::post('/create-subscribers','ApiController@createSubscriber');
    Route::post('/unsubscribe','ApiController@unsubscribe');

    // Products
    Route::get('/product-price','ApiController@getProductsMaxMinPrice');
    Route::get('/get-product','ApiController@getProductAndSmiliarProduct');

    // Products
    Route::get('/get-products','ApiController@getProducts');
    Route::get('/category-products','ApiController@getCategoryByProducts');
    Route::get('/category-products-part','ApiController@getCategoryByPart');
    Route::get('/collection-products-part','ApiController@getCollectionByPart');

    //Home page
    Route::post('/get-home', 'ApiController@getHomePageData');

    //Contact form
    Route::post('/send-contact-email', 'ApiController@sendEmailContact');

    //Search Products

    Route::get('/search-products','ApiController@searchProducts');

    //cart

    Route::post('/add-cart','ApiController@addCart');
    Route::get('/get-cart','ApiController@getCart');
    Route::post('/delete-cart','ApiController@deleteCart');
    Route::post('/delete-cart-item','ApiController@deleteCartItem');


    //FAQ
    Route::any('/get-faq',function (){
        return abort(404);
    });
    Route::get('/get-faq','ApiController@getFaq');
});
