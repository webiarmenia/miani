<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

//version 1

//    $endpoint = "http://mianie.ru/api/miani/get-home";
//    $client = new \GuzzleHttp\Client();
//    $id = 5;
//    $value = "ABC";
//
//    $response = $client->request('GET', $endpoint, ['query' => [
//        'key1' => '$id',
//        'key2' => 'Test'
//    ]]);
//
//// url will be: http://my.domain.com/test.php?key1=5&key2=ABC;
//
//    $statusCode = $response->getStatusCode();
//    $content = $response->getBody();
//
//// or when your server returns json
// return json_decode($response->getBody(), true);

//version 2

//    $curl = curl_init();
//
//    curl_setopt_array($curl, array(
//        CURLOPT_URL => "http://mianie.ru/api/miani/get-home",
//        CURLOPT_RETURNTRANSFER => true,
//        CURLOPT_ENCODING => "",
//        CURLOPT_TIMEOUT => 30000,
//        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//        CURLOPT_CUSTOMREQUEST => "GET",
//        CURLOPT_HTTPHEADER => array(
//            // Set Here Your Requesred Headers
//            'Content-Type: application/json',
//        ),
//    ));
//    $response = curl_exec($curl);
//    $err = curl_error($curl);
//    curl_close($curl);
//
//    if ($err) {
//        echo "cURL Error #:" . $err;
//    } else {
//        echo '<pre>'.print_r(json_decode($response),true).'</pre>';
//    }

return view('welcome');

})->name('welcome');


Auth::routes();
//Route::get('storage/{dir}/{filename}', function ($dir,$filename)
//{
//    $path = storage_path('app/public/'.$dir.'/'.$filename);
//    if (!File::exists($path)) {
//        abort(404);
//    }
//    $file = File::get($path);
//    $type = File::mimeType($path);
//    $response = Response::make($file, 200);
//    $response->header("Content-Type", $type);
//    return $response;
//});
//Route::get('storage/{dir1}/{dir2}/{filename}', function ($dir1,$dir2,$filename)
//{
//    $path = storage_path('app/public/'.$dir1.'/'.$dir2.'/'.$filename);
//    if (!File::exists($path)) {
//        abort(404);
//    }
//    $file = File::get($path);
//    $type = File::mimeType($path);
//    $response = Response::make($file, 200);
//    $response->header("Content-Type", $type);
//    return $response;
//});
//    //ckeditor

Route::group(['middleware' => 'admin'], function () {
    Route::get('/laravel-filemanager', '\UniSharp\LaravelFilemanager\Controllers\LfmController@show');
    Route::post('/laravel-filemanager/upload', '\UniSharp\LaravelFilemanager\Controllers\UploadController@upload');
    Route::prefix('admin_v128_11')->group(function () {
        Route::get('/',function (){
            return redirect()->route('admin.users');
        })->name('admin.index');

        //users
        Route::get('users','UsersController@index')->name('admin.users');
        Route::post('users/userChange/{id}','UsersController@userChange')->name('admin.users.change');
        Route::get('users/show/{id}','UsersController@show')->name('admin.users.show');

        //subscribers
        Route::get('subscribers','UsersController@getSubscribers')->name('admin.subscribers');

        //subscriber messages
        Route::get('messages','MessagesController@index')->name('admin.messages');
        Route::get('messages/create','MessagesController@create')->name('admin.messages.create');
        Route::post('messages/store','MessagesController@store')->name('admin.messages.store');
        Route::get('messages/view/{id}','MessagesController@show')->name('admin.messages.view');
        Route::get('messages/edit/{id}','MessagesController@edit')->name('admin.messages.edit');
        Route::post('messages/update/{id}','MessagesController@update')->name('admin.messages.update');

        //pages
        Route::get('pages','PageController@index')->name('admin.pages');
        Route::post('pages', 'PageController@store')->name('admin.pages.store');
        Route::get('pages/create', 'PageController@create')->name('admin.pages.create');
        Route::get('pages/update/{id}', 'PageController@edit')->name('admin.pages.edit');
        Route::post('pages/update/{id}', 'PageController@update')->name('admin.pages.update');
        Route::post('pages/destroy/{id}', 'PageController@destroy')->name('admin.pages.destroy');
        Route::get('pages/fetch_data','PageController@fetch_data')->name('admin.pages.fetch_data');

        //faqs
        Route::get('faqs','FaqController@index')->name('admin.faqs');
        Route::post('faqs', 'FaqController@store')->name('admin.faqs.store');
        Route::get('faqs/create', 'FaqController@create')->name('admin.faqs.create');
        Route::get('faqs/update/{id}', 'FaqController@edit')->name('admin.faqs.edit');
        Route::post('faqs/update/{id}', 'FaqController@update')->name('admin.faqs.update');
        Route::post('faqs/destroy/{id}', 'FaqController@destroy')->name('admin.faqs.destroy');

        //media
        Route::get('media','MediaController@index')->name('admin.media');
        Route::post('media', 'MediaController@store')->name('admin.media.store');
        Route::get('media/create', 'MediaController@create')->name('admin.media.create');
        Route::get('media/update/{id}', 'MediaController@edit')->name('admin.media.edit');
        Route::post('media/update/{id}', 'MediaController@update')->name('admin.media.update');
        Route::post('media/destroy/{id}', 'MediaController@destroy')->name('admin.media.destroy');

        //discounts
        Route::get('discounts','DiscountsController@index')->name('admin.discounts');
        Route::get('discounts/create','DiscountsController@create')->name('admin.discounts.create');
        Route::post('discounts/store','DiscountsController@store')->name('admin.discounts.store');
        Route::get('discounts/view/{id}','DiscountsController@show')->name('admin.discounts.view');
        Route::get('discounts/edit/{id}','DiscountsController@edit')->name('admin.discounts.edit');
        Route::post('discounts/update/{id}','DiscountsController@update')->name('admin.discounts.update');
        Route::post('discounts/destroy/{id}','DiscountsController@destroy')->name('admin.discounts.destroy');

        //products
        Route::get('products','ProductsController@index')->name('admin.products');
        Route::get('products/create','ProductsController@create')->name('admin.products.create');
        Route::post('products/store','ProductsController@store')->name('admin.products.store');
        Route::get('products/edit/{id}','ProductsController@edit')->name('admin.products.edit');
        Route::post('products/update/{id}','ProductsController@update')->name('admin.products.update');
        Route::post('products/destroy/{id}','ProductsController@destroy')->name('admin.products.destroy');
        Route::get('products/destroy/{id}','ProductsController@destroy')->name('admin.products.destroy');
        Route::get('products/fetch_data','ProductsController@fetch_data')->name('admin.products.fetch_data');

        //menu
        Route::get('menus','MenusController@index')->name('admin.menus');
        Route::get('menus/create','MenusController@create')->name('admin.menus.create');
        Route::post('menus/store','MenusController@store')->name('admin.menus.store');
        Route::get('menus/edit/{id}','MenusController@edit')->name('admin.menus.edit');
        Route::post('menus/update/{id}','MenusController@update')->name('admin.menus.update');
        Route::post('menus/destroy/{id}','MenusController@destroy')->name('admin.menus.destroy');
//        Route::get('menus/sort','MenusController@sortableIndex')->name('admin.menus.sort');
//        Route::post('menus/sortable','MenusController@sortable')->name('admin.menus.sortable');

        //category
        Route::get('categories','CategoriesController@index')->name('admin.categories');
        Route::get('categories/create','CategoriesController@create')->name('admin.categories.create');
        Route::post('categories/store','CategoriesController@store')->name('admin.categories.store');
        Route::get('categories/edit/{id}','CategoriesController@edit')->name('admin.categories.edit');
        Route::post('categories/update/{id}','CategoriesController@update')->name('admin.categories.update');
        Route::post('categories/destroy/{id}','CategoriesController@destroy')->name('admin.categories.destroy');
        Route::get('categories/sort','CategoriesController@sortableIndex')->name('admin.categories.sort');
        Route::post('categories/sortable','CategoriesController@sortable')->name('admin.categories.sortable');

        //orders
        Route::get('orders','OrdersController@index')->name('admin.orders');
        Route::post('orders/index','OrdersController@index')->name('admin.orders.index');
        Route::get('orders/{type}/{id}','OrdersController@updateNotification');
        Route::get('order/edit/{id}','OrdersController@edit')->name('admin.orders.edit');
        Route::get('order/view/{id}','OrdersController@show')->name('admin.orders.view');
        Route::post('orders/update/{id}','OrdersController@update')->name('admin.orders.update');
        Route::post('orders/filter','OrdersController@filter')->name('admin.orders.filter');

        //settings
        Route::get('settings','SettingsController@index')->name('admin.settings');
        Route::get('settings/create','SettingsController@create')->name('admin.settings.create');
        Route::post('settings/store','SettingsController@store')->name('admin.settings.store');
        Route::get('settings/edit/{id}','SettingsController@edit')->name('admin.settings.edit');
        Route::post('settings/update/{id}','SettingsController@update')->name('admin.settings.update');

        //collections
        Route::get('collections','CollectionsController@index')->name('admin.collections');
        Route::get('collections/create','CollectionsController@create')->name('admin.collections.create');
        Route::post('collections/store','CollectionsController@store')->name('admin.collections.store');
        Route::get('collections/edit/{id}','CollectionsController@edit')->name('admin.collections.edit');
        Route::post('collections/update/{id}','CollectionsController@update')->name('admin.collections.update');
        Route::post('collections/destroy/{id}','CollectionsController@destroy')->name('admin.collections.destroy');
        Route::get('collections/fetch_data','CollectionsController@fetch_data')->name('admin.collections.fetch_data');

        //sliders
        Route::get('sliders','SliderController@index')->name('admin.sliders');
        Route::get('sliders/create','SliderController@create')->name('admin.sliders.create');
        Route::post('sliders/store','SliderController@store')->name('admin.sliders.store');
        Route::get('sliders/edit/{id}','SliderController@edit')->name('admin.sliders.edit');
        Route::post('sliders/update/{id}','SliderController@update')->name('admin.sliders.update');
        Route::post('sliders/destroy/{id}','SliderController@destroy')->name('admin.sliders.destroy');

        //carts

//        Route::get('/carts','CartsController@index')->name('admin.carts');
//        Route::get('/carts/view/{id}','CartsController@show')->name('admin.carts.view');

    });
});
