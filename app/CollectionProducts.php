<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollectionProducts extends Model
{
    //
    public function collection(){
        return $this->hasOne(Collections::class,'id','collection_id');
    }
    public function productDetail()
    {
        return $this->hasOne(ProductDetails::class,'id','product_detail_id');
    }

}
