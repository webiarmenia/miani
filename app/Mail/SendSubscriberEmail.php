<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendSubscriberEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $subject;
    public $message;
//    public $akc;
//    public $ds;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject,$message)
    {
        $this->subject = $subject;
        $this->message = $message;
//        $this->akc = $akc;
//        $this->ds = $ds;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $e_subject = $this->subject;
        $e_message = $this->message;
        return $this->view('emails.sendSubscriberEmail',compact("e_message","e_subject"))->subject($e_subject);
    }
}
