<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendUserEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $confirmed;
    public $email;
    public $name;
    public $surname;
    public $subject;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($confirmed,$email,$name,$surname,$subject)
    {
        $this->confirmed = $confirmed;
        $this->email = $email;
        $this->name = $name;
        $this->surname = $surname;
        $this->subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $confirmed = $this->confirmed;
        $email = $this->email;
        $name = $this->name;
        $surname = $this->surname;
        $subject = $this->subject;
        return $this->view('emails.sendUserEmail',compact('confirmed','email','name','surname'))->subject($subject);;
    }
}
