<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{

    public function parent(){
        return $this->hasOne(Menu::class,'id','parent_id');
    }

    public function page()
    {
        return $this->hasOne(Page::class, 'id', 'address');
    }


    protected $fillable = ['title','parent_id', 'position', 'address', 'slug', 'order'];
}
