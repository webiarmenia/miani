<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImages extends Model
{
    //

    public function productDetail(){
        return $this->hasOne(ProductDetails::class,'id','product_detail_id');
    }

    public function imageDetail(){
        return $this->hasOne(Media::class,'id','image');
    }
}
