<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    public function parent(){
        return $this->hasOne(Category::class,'id','parent_id');
    }

    public function discount(){
        return $this->hasOne(Discount::class,'id','discount_id');
    }

    public function subscribers(){
        return $this->hasMany(Subscriber::class);
    }

    public function childs() {
        return $this->hasMany('App\Category','parent_id','id') ;
    }

//    public function parents() {
//        return $this->belongsTo('App\Category','parent_id','id') ;
//    }

    protected $fillable = [
        'title',
//        'cover_id',
        'parent_id',
        'slug',
//        'icon',
//        'discount_id'
    ];

}

