<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Orders;
use View;

class Controller extends BaseController{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct(){
        header("Cache-Control: no-cache, no-store, must-revalidate");
        $orders_no_read = Orders::where("read", 'no')->orderBy('id','DESC')->get();
        $quantity = count($orders_no_read);
        View::share([
            'quantity' => $quantity,
            'orders_no_read' => $orders_no_read,
        ]);
    }
}
