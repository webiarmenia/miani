<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Settings;
use Carbon\Carbon;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = Settings::orderBy('id','DESC')->paginate(10);
        return view('admin.settings.index')->with([
            'settings' => $settings
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.settings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'key'   => 'required',
        ]);

        $creatSetting = Settings::insert([
            'key' => $request->key,
            'value' => $request->value,
            'created_at' =>  Carbon::now()
        ]);

        if($creatSetting){
            return redirect()->route('admin.settings')->with('successCreate', 'Настройка Успешно Создана');
        }else{
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Settings::where('id',$id)->firstOrfail();
        return view('admin.settings.edit')->with(['item' => $item]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'value' =>'required',
        ]);

        $updateSetting = Settings::where('id', $id)->update([
            'value'      => $request->value,
            'updated_at' => Carbon::now()
        ]);

        if($updateSetting){
            return redirect()->route('admin.settings')->with('successUpdate', 'Настройка Успешно Обновлено');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
