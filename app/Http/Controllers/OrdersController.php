<?php

namespace App\Http\Controllers;

use App\ProductDetails;
use Illuminate\Http\Request;
use App\Orders;
use App\UnregisteredUsers;
use App\User;

class OrdersController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//        header("Cache-Control: no-cache, no-store, must-revalidate");
        if(isset($request->status) && $request->status != null)
        {
            $selected = $request->status;
            $orders = Orders::where("status", $request->status)->orderBy('id', 'DESC')->paginate(10);
            return view('admin.orders.index')->with(['orders' => $orders, 'selected' => $selected]);
        }else{
            $selected = 10;
            $orders = Orders::orderBy('id', 'DESC')->paginate(10);
            return view('admin.orders.index')->with(['orders' => $orders,  'selected' => $selected]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = Orders::where('id',$id)->firstOrfail();
        $item = Orders::where('id',$id)->firstOrfail();
        if($item->client_id == null){
            $user = UnregisteredUsers::where("order_id",$item->id)->firstOrfail();
        }else{
            $user = User::where("id",$item->client_id)->firstOrfail();
        }
        $order_products = Orders::where('id',$id)->with('orderProducts.productDetail.product')->firstOrfail();
        $color = ProductDetails::getColor();
        return view('admin.orders.view')->with([
            'item' => $item,
            'color' => $color,
            'order_products' => $order_products,
            'user' => $user
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Orders::where('id',$id)->firstOrfail();
        return view('admin.orders.edit')->with(['item' => $item]);
    }

    /**
     * Update read column value in orders table.
     *
     * @param  string $type
     * @param  int  $id
     */
    public function updateNotification($type,$id)
    {
        $item = Orders::where('id',$id)->firstOrfail();
        $item->read = 'yes';
        $item->save();
        return redirect('admin_v128_11/order'."/".$type."/".$id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $order = Orders::where('id',$id)->with('orderProducts.productDetail')->firstOrfail();
        $order->approved = $request->get('approved');
        $order->status   = $request->get('status');
        $order->save();
        return redirect()->route('admin.orders')->with('success', 'Заказ Успешно Обновлено');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
