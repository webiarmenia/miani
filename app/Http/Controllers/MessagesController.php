<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Messages;
use App\Subscriber;
use App\Mail\SendSubscriberEmail;
use App\Mail\SendMianiEmail;



class MessagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $messages = Messages::orderBy('id', 'DESC')->paginate(10);
        return view('admin.subscribers.messages.index')->with(['messages' => $messages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.subscribers.messages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request['button'])
        {
            $request->validate([
                'subject' => 'required',
                'messageArea' => 'required',
            ]);

            $message = new Messages([
                'subject' => $request->get('subject'),
                'message' => $request->get('messageArea'),
            ]);

            if($request->button == "add_send"){
                $message_subject = $message->subject;
                $message_text = $message->message;
                $message->sent = true;

                $subscribers = Subscriber::where('active',true)->select('email')->get();
                $subscribers_array = [];
                foreach ($subscribers as $subscriber){
                    array_push($subscribers_array,$subscriber->email);
                }
                if (!empty($subscribers_array)){
                    \Mail::to($subscribers_array)->send(new SendSubscriberEmail($message_subject,$message_text));
                    $message->save();
                    return redirect()->route('admin.messages')->with('successCreate', 'Сообщение успешно добавлено и отпрaвлено');
                }else{
                    return back()->with('successCreate', 'Нет активых подписчиков');
                }

            }
            $message->save();
            return redirect()->route('admin.messages')->with('successCreate', 'Сообщение успешно добавлено');
        }else{
            return redirect()->route('admin.messages');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = Messages::where('id',$id)->firstOrfail();
        return view('admin.subscribers.messages.view')->with(['item' => $item]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Messages::where('id',$id)->firstOrfail();
        return view('admin.subscribers.messages.edit')->with(['item' => $item]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if($request['button'])
        {
            $request->validate([
                'subject' => 'required',
                'messageArea' => 'required',
            ]);

            $message = Messages::where('id',$id)->update([
                'subject' => $request->subject,
                'message' => $request->messageArea,
            ]);

            return redirect()->route('admin.messages')->with('successUpdate', 'Сообщение успешно обновлено');
        }else{
            return redirect()->route('admin.messages');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
