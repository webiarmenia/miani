<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    //
    public function index(){
        return file_get_contents( public_path('dist\index.html')) ;
    }
}
