<?php

namespace App\Http\Controllers\Api;

use App\Faq;
use App\Cart;
use App\CollectionProducts;
use App\Review;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Page;
use App\Menu;
use App\User;
use App\Orders;
use App\OrderProducts;
use App\ProductDetails;
use App\Products;
use App\DiscountCondition;
use App\Discount;
use App\Subscriber;
use App\UnregisteredUsers;
use App\History;
use App\Wish;
use App\ProductImages;
use App\ProductCategory;
use App\Category;
use App\Media;
use App\Slider;
use App\Settings;
use App\Collections;
use App\Mail\SendMianiEmail;
use App\Mail\SendClientEmail;
use App\Mail\SendUserEmail;
use App\Mail\SendForgotPasswordEmail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use DB;


class ApiController extends Controller
{

    public function __construct(Request $request, $sended_token = null)
    {
        $token = $request->header('token');

        if (isset($token)) {
            $user = User::where("auth_key", $request->header('token'))->first();
            if ($user) {
                $this->id = $user->id;
            } else {
                $this->id = false;
            }
        } else {
            $this->id = null;
        }
    }




///////////////////////////////////////
    //function for get pages//
///////////////////////////////////////

    public function getPage(Request $request)
    {
        $this->validate($request, [
            'slug' => 'required',
        ]);


        $slug = $request->get('slug');
        $menu = Menu::where('slug', $slug)->first();
        if ($menu) {
            $page = Page::where("id", $menu->address)->first();
            if ($page) {
                return response()->json([
                    'page' => [
                        'id' => $page->id,
                        'title' => $page->title,
                        'content' => $page->content,
                        'cover' => $page->media->uniq_name,
                        'ext' => $page->media->ext,
                        'path' => $page->media->path
                    ]
                ]);
            } else {
                return response()->json([
                    'status' => 'error',
                    'massage' => 'This page not found'
                ]);
            }
        } else {
            return response()->json([
                'status' => 'error',
                'massage' => 'This page not found'
            ]);
        }

    }

///////////////////////////////////////
    //function for add wish//
///////////////////////////////////////

    public function addWish(Request $request)
    {
        $request->validate([
            'product_detail_id' => 'required',
        ]);

        $isseted_wish = Wish::where('user_id', $this->id)->where('product_id', $request->product_detail_id)->first();
        if ($isseted_wish) {
            return response()->json('Продукт уже добавлен ');
        } else {
            $data = new Wish();
            $data->product_id = $request->product_detail_id;
            $data->user_id = $this->id;
            $data->save();
            $wish = $this->getWish($request);
            return $wish;
        }
    }

    /**
     * Create get wish
     */

    public function getWish(Request $request){

        $wishes = Wish::where("user_id", $this->id)->orderBy('created_at', 'DESC')->with('productDetail.productImages.imageDetail')->with('product')->limit(5)->get()->toArray();
        $get_wish = [];
        foreach ($wishes as $wish) {
            $images = [];
            foreach ($wish['product_detail']['product_images'] as $item) {
                array_push($images, array(
                    'path' => asset('/storage/' . $item['image_detail']['path'] . $item['image_detail']['uniq_name']),
                    'ext' => $item['image_detail']['ext']
                ));
            }
            array_push($get_wish, array(
                'wish_id' => $wish['id'],
                'product_id' => $wish['product_detail']['product_id'],
                'product_detail_id' =>  $wish['product_detail']['id'],
                'product_detail_quantity' => $wish['product_detail']['quantity'],
                'name' => $wish['product']['name'],
                'size' => $wish['product_detail']['size'],
                'color' => $wish['product_detail']['color'],
                'price' => $wish['product_detail']['price'],
                'image' => $images
            ));
        }
        if (!empty($wish)) {
            return response()->json([
                'wishes' => $get_wish,
            ]);
        } else {
            return response()->json();
        }
    }

    /**
     * Create delete Wish
     */

    public function deleteWish(Request $request)
    {
        $request->validate([
            'product_id' => 'required',
            'id' => 'required',
        ]);
        $removewish = Wish::where('product_id', $request->product_id)->where('id', $request->id)->where('user_id', $this->id)->first();
        if ($removewish) {
            $removewish->delete();
            return response()->json([
                'status' => 'Success',
                'message' => 'delete wish'
            ]);
        }
    }

    /**
     *  Create delete Cart
     */

    public function deleteCart(Request $request)
    {
        $items = Cart::where('user_id', $this->id)->delete();
        if ($items == true) {
            return response()->json(
                'Success'
            );
        } else {
            return response()->json(
                'no items'
            );
        }

    }

    public function deleteCartItem(Request $request)
    {
        $this->validate($request, [
            'product_detail_id' => 'required'
        ]);

        $item = Cart::where('user_id', $this->id)->where('product_detail_id', $request->product_detail_id)->delete();
        if ($item == true) {
            return response()->json([
                'status' => 'Success'
            ]);
        } else {
            return response()->json([
                'status' => 'Error'
            ]);
        }

    }

///////////////////////////////////////
    //function for get Review//
///////////////////////////////////////

    public function saveReview(Request $request)
    {
        $request->validate([
            'product_id' => 'required',
            'comment' => 'required',
            'rate' => 'required'
        ]);
        if ($request->header('token') == null) {
            return 'token required';
        }
        $orders = Orders::where('client_id', $this->id)->with('orderProducts')->get()->toArray();

        if (!$orders) {
            return response()->json([
                'status' => 'Error',
                'message' => 'Клиент не имеет покупок'
            ]);
        }
        $quantity = 0;
        $products_array = [];
        $user = User::where('id', $this->id)->first();
        foreach ($orders as $item) {
            $quantity++;
            if ($item['status'] == 2) {
                foreach ($item['order_products'] as $order_products) {
                    $productDetail = ProductDetails::where('id', $order_products['product_details_id'])->with('product')->first();
                    array_push($products_array, $productDetail->product->id);
                }
                $products_array = array_unique($products_array);
                if (in_array($request->product_id, $products_array)) {
                    $review = new Review();
                    $review->user_id = $user->name . ' ' . $user->surname;
                    $review->comment = $request->comment;
                    $review->rate = $request->rate;
                    $review->product_id = $request->product_id;
                    $review->save();
                    return response()->json([
                        'status' => 'Success',
                    ]);
                } else {
                    return response()->json([
                        'status' => 'Error',
                        'message' => 'Вы не можете оставить отзыв об этом товаре'
                    ]);
                }
            } else {
                if ($quantity == count($orders)) {
                    return response()->json([
                        'status' => 'Error',
                        'message' => 'Вы не можете оставить отзыв об этом товаре'
                    ]);
                }
            }
        }
    }

///////////////////////////////////////
    //function for get product reviews//
///////////////////////////////////////

    public function getProductReviews(Request $request)
    {
        $request->validate([
            'product_id' => 'required'
        ]);

        $arr = [];
        $product_reviews = [];
        $products = Products::where('id', $request->product_id)->with('productDetail.orderProducts.order.user')->first();
        $reviews = Review::where("product_id", $request->product_id)->where('user_id', $this->id)->with('user')->get()->toArray();
        if (!empty($reviews)) {
            foreach ($reviews as $review) {
                array_push($product_reviews, array(

                    'user_name' => $review['user']['name'],
                    'id' => $review['id'],
                    'product_id' => $review['product_id'],
                    'rate' => $review['rate'],
                    'comment' => $review['comment'],
                    'created_at' => $review['created_at'],
                    'updated_at' => $review['updated_at']
                ));
            }
        }

//        if (!empty($reviews)){
//            $product_reviews = $reviews;
//        }else{
//            $product_reviews = 'Нет коментарии';
//        }
        if (isset($this->id)) {
            $token = $this->id;
        } else {
            $token = false;
        }
        if ($products) {
            foreach ($products['productDetail'] as $key => $item) {
                if ($item['orderProducts']['order']['status'] == 2 && $item['orderProducts']['order']['user']->id == $token) {
                    $canLeaveReview = true;
                    break;
                } else {
                    $canLeaveReview = false;
                }
            }

            $arr = [
                'canLeaveReview' => $canLeaveReview,
                'reviews' => $product_reviews
            ];

            return response()->json($arr);
        } else {
            return response()->json($arr);
//            return response()->json('Продукт не найден');
        }

    }

///////////////////////////////////////
    //function for get user wishs//
///////////////////////////////////////

//    public function getUserWish(Request $request,$user=false){
//        if(!$user){
//            $request->validate([
//                'token' => 'required',
//            ]);
//        }
//        $arrayProductDetails = [];
//        $wishs = Wish::where("user_id", $this->id)->get();
//        $wishsCount = count($wishs);
//
//        for ($i = 0; $i < $wishsCount; $i++) {
//            $productDetail = ProductDetails::where("id", $wishs[$i]["product_id"])->firstOrfail();
//            $productImages = ProductImages::where("product_detail_id", $productDetail->id)->firstOrfail();
//            $imageMediaPath = Media::where("id", $productImages->image)->firstOrfail();
//            $app_url = env('APP_URL');
//            $image = "$app_url/storage/$imageMediaPath->path" . "$imageMediaPath->uniq_name";
//            $ext = "$imageMediaPath->ext";
//            $arrayProductDetail = ([
//                'product_detail_id' => $productDetail ->id,
//                'size' => $productDetail->size,
//                'color' => $productDetail->color,
//                'price' => $productDetail->price,
//                'image' => $image,
//                'ext'   => $ext
//            ]);
//
//            array_push($arrayProductDetails, $arrayProductDetail);
//        }
//        if(!$user) {
//            return response()->json([
//                'wishes' => ($arrayProductDetails
//
//                ),
//            ]);
//        }else{
//            return $arrayProductDetails;
//        }
//    }


    public function getUserWish(Request $request, $user = false)
    {

        if (!$user) {
            $validator = Validator::make($request->header(), [
                'token' => 'required ',
            ]);
            $wishes = Wish::where("user_id", $this->id)->orderBy('created_at', 'DESC')->with('productDetail.productImages.imageDetail')->with('product')->limit(5)->get()->toArray();
        } else {
            $wishes = Wish::where("user_id", $user)->orderBy('created_at', 'DESC')->with('productDetail.productImages.imageDetail')->with('product')->limit(5)->get()->toArray();
        }
        $arrayProductDetails = [];

        foreach ($wishes as $wish) {
            $images = [];
            foreach ($wish['product_detail']['product_images'] as $item) {
                array_push($images, array(
                    'path' => asset('/storage/' . $item['image_detail']['path'] . $item['image_detail']['uniq_name']),
                    'ext' => $item['image_detail']['ext']
                ));
            }
            array_push($arrayProductDetails, array(
                'wish_id' => ($wish['id']),
                'product_id' => $wish['product_detail']['product_id'],
                'product_detail_id' =>  $wish['product_detail']['id'],
                'name' => $wish['product']['name'],
                'size' => $wish['product_detail']['size'],
                'color' => $wish['product_detail']['color'],
                'price' => $wish['product_detail']['price'],
                'image' => $images
            ));
        }
        if (!$user) {
            return response()->json([
                'wishes' => $arrayProductDetails,
            ]);
        } else {
            return $arrayProductDetails;
        }
    }


/////////////////////////////////////////
//    //function for get menus//
/////////////////////////////////////////
//    public function getMenus(Request $request)
//    {
//
//        $auth_key = $request->header('Authorization');
//        $token = explode(" ", $auth_key);
//        $user = User::where("status", 30)->firstOrfail();
//        if ($token[1] == $user->auth_key) {
//            $k = 0;
//            if ($request->parent_id) {
//                $parentId = $request->parent_id;
//            } else {
//                $parentId = null;
//            }
//            if ($request->position) {
//                $position = $request->position;
//                $data = Menu::where('position', $position)->orderBy('order')->get();
//            }
////            else {
////                $data = Menu::orderBy('order')->get();
////            }
//            return $this->buildTree($data, $parentId);
//        } else {
//            return response()->json([
//                'status' => 'Error',
//                'message' => 'This is a wrong auth_key'
//            ]);
//        }
//    }
//
//    protected function buildTree($elements, $parentId)
//    {
//
//        $menus = array();
//
//        foreach ($elements as $element) {
//            if ($element['parent_id'] == $parentId) {
//
//                $abc = Menu::where("parent_id", $element["id"])->first();
//                if($abc){
//                    $children = $this->buildTree($elements, $element['id']);
//                    $element['children'] = $children;
//                }
//                $menus[] = $element;
//            }
//        }
//
//        return response()->json([
//            'status' => 'Success',
//            'message' => $menus
//        ]);
//
//    }


//////////////////////////////////////
    //function for create order//
//////////////////////////////////////
    public function createOrder(Request $request)
    {
        $this->validate($request, [
            'client_email' => 'required | email',
            'total_price'  => 'required | int',
            'name'         => 'required | string',
            'surname'      => 'required | string',
            'address'      => 'required | string',
            'phone'        => 'required | string',
            'products'     => 'required',
        ]);

        $order = new Orders();
        $order->client_id = $this->id;
        $order->client_email = $request->client_email;
        $order->total_price = $request->total_price;
        $order->approved = false;
        $order->status = 0;
        $order->read = 'no';

        $order->save();

        if ($order->client_id == null) {

            $unregisteredUserYes = UnregisteredUsers::where("email", $order->client_email)->first();

            if ($unregisteredUserYes) {
                $history = new History();
                $history->user_id = $unregisteredUserYes->id;
                $history->order_id = $order->id;
                $history->unregistered = true;
                $history->save();
            } else {

                $unregisteredUsers = new UnregisteredUsers();
                $unregisteredUsers->email = $order->client_email;
                $unregisteredUsers->order_id = $order->id;
                $unregisteredUsers->name = $request->name;
                $unregisteredUsers->surname = $request->surname;
                $unregisteredUsers->address = $request->address;
                $unregisteredUsers->phone = $request->phone;
                $unregisteredUsers->save();

                $history = new History();
                $history->user_id = $unregisteredUsers->id;
                $history->order_id = $order->id;
                $history->unregistered = true;
                $history->save();
            }

        } else {
            $history = new History();
            $history->user_id = $order->client_id;
            $history->order_id = $order->id;
            $history->save();
        }

        $productCount = count($request->products);
        for ($i = 0; $i < $productCount; $i++) {
            $orderProduct = new OrderProducts();
            $orderProduct->product_details_id = $request->products[$i]["product_detail_id"];
            $orderProduct->quantity = $request->products[$i]["quantity"];
            $orderProduct->order_id = $order->id;

            $product_detail_id = $request->products[$i]["product_detail_id"];

            $productDetails = ProductDetails::where("id", $product_detail_id)->firstOrfail();

//            $product = Products::where("id", $productDetails->product_id)->firstOrfail();
            if ($productDetails->discount_id != null) {
                $discountConditions = DiscountCondition::where("discount_id", $productDetails->discount_id)->get();
                $discountConditionsCount = count($discountConditions);
                $discountYesNo = 0;
                for ($j = 0; $j < $discountConditionsCount; $j++) {
                    $discountCondition = $discountConditions[$j];
                    switch ($discountCondition->condition_type) {
                        case '0':// when condition_type == '='
                            if ($discountCondition->condition_key == 0) {
                                if ($discountCondition->condition_value != $productDetails->price) {
                                    $discountYesNo++;
                                }
                            } else {
                                if ($discountCondition->condition_value != $productDetails->size) {
                                    $discountYesNo++;
                                }
                            }
                            break;
                        case '1':// when condition_type == '>'
                            if ($discountCondition->condition_key == 0) {
                                if ($productDetails->price < $discountCondition->condition_value) {
                                    $discountYesNo++;
                                }
                            } else {
                                if ($productDetails->size < $discountCondition->condition_value) {
                                    $discountYesNo++;
                                }
                            }
                            break;
                        case '2':// when condition_type == '>='
                            if ($discountCondition->condition_key == 0) {
                                if ($productDetails->price <= $discountCondition->condition_value) {
                                    $discountYesNo++;
                                }
                            } else {
                                if ($productDetails->size <= $discountCondition->condition_value) {
                                    $discountYesNo++;
                                }
                            }
                            break;
                        case '3':// when condition_type == '<'
                            if ($discountCondition->condition_key == 0) {
                                if ($productDetails->price > $discountCondition->condition_value) {
                                    $discountYesNo++;
                                }
                            } else {
                                if ($productDetails->size > $discountCondition->condition_value) {
                                    $discountYesNo++;
                                }
                            }
                            break;
                        case '4':// when condition_type == '<='
                            if ($discountCondition->condition_key == 0) {
                                if ($productDetails->price >= $discountCondition->condition_value) {
                                    $discountYesNo++;
                                }
                            } else {
                                if ($productDetails->size >= $discountCondition->condition_value) {
                                    $discountYesNo++;
                                }
                            }
                            break;
                    }

                }

                if ($discountYesNo == 0) {
                    $discount = Discount::where("id", $productDetails->discount_id)->firstOrfail();
                    $discountType = $discount->discount_type;
                    $discountSize = $discount->discount_size;
                    if ($discountType == '0') {
                        $orderProductPercent = $productDetails->price * $discountSize / 100;
                        $orderProduct->product_price = $productDetails->price - $orderProductPercent;
                    } else {
                        $orderProduct->product_price = $productDetails->price - $discountSize;
                    }
                } else {
                    $orderProduct->product_price = $productDetails->price;
                }
            } else {
                $orderProduct->product_price = $productDetails->price;
            }
            $saved = $orderProduct->save();
        }

        $mianiMail = env("MAIL_USERNAME");
        \Mail::to($mianiMail)->send(new SendMianiEmail($order));
        \Mail::to($order->client_email)->send(new SendClientEmail($order));
        if ($saved) {
            if ($order->client_id != null) {
                Cart::where('user_id', $order->client_id)->delete();
            }
            return response()->json([
                "success"
            ]);

        } else {
            return response()->json([
                "error"
            ]);
        }

    }

///////////////////////////////////////
    //function for register user//
///////////////////////////////////////
    public function userRegister(Request $request)
    {
        $this->validate($request, [
            'name' => 'required ',
            'surname' => 'required ',
            'email' => 'required | email | unique:users',
            'password' => 'required | min:8',
            'address' => 'required',
            'postal_code' => 'required',
            'phone' => 'required',
        ]);

        $user = new User();
        $user->name = $request->name;
        $user->surname = $request->surname;
        $user->email = $request->email;
        $user->address = $request->address;
        $user->postal_code = $request->postal_code;
        $user->phone = $request->phone;
        $user->password = Hash::make($request->password);
        $user->auth_key = Str::random(20);
        $user->confirmed_token = Str::random(20);
        $user->status = 0;
        $user->save();

        $userEmail = $user->email;
        $unregisteredUser = UnregisteredUsers::where("email", $userEmail)->first();

        if ($unregisteredUser) {
            History::where("user_id", $unregisteredUser->id)->update(['unregistered' => false]);

            Orders::where("client_email", $user->email)->update(['client_id' => $user->id]);

        }
        $email = $user->email;
        $confirmed = $user->confirmed_token;
        $name = $user->name;
        $surname = $user->surname;
        $subject = "Добро пожаловать";
        \Mail::to($user->email)->send(new SendUserEmail($confirmed, $email, $name, $surname, $subject));

        return response()->json([
            'status' => 'Success',
        ]);
    }

    public function userRegisterFacebook(Request $request)
    {
        $this->validate($request, [
            'token' => 'required',
            'firstName' => 'required',
            'lastName' => 'required',
            'email' => 'required',
            'provider' => 'required',
            'id' => 'required',
        ]);

        $isseted_user = User::where('email', $request->email)->where('provider', null)->where('provider_id', null)->first();
        if ($isseted_user) {
            $isseted_user->auth_key = $request->token;
            $isseted_user->provider = $request->provider;
            $isseted_user->provider_id = $request->id;
            $saved = $isseted_user->save();
            if ($saved) {
                return response()->json([
                    'status' => 'success',
                    'user' => ([
                        'token' => $isseted_user->auth_key,
                        'firstName' => $isseted_user->name,
                        'lastName' => $isseted_user->surname,
                        'email' => $isseted_user->email,
                    ])
                ]);
            } else {
                return response()->json([
                    'status' => 'error',
                ]);
            }
        } else {
            $this->validate($request, [
                'email' => 'unique:users',
            ]);
//            $array = explode(' ', $request->firstName);
//            $name = $array[0];
//            $surname = $array[1];
            $user = new User();
            $user->auth_key = $request->token;
            $user->name = $request->firstName;
            $user->surname = $request->lastName;
            $user->email = $request->email;
            $user->provider = $request->provider;
            $user->provider_id = $request->id;
            $saved = $user->save();
            if ($saved) {
                $created_user = User::where('id', $user->id)->first();
                return response()->json([
                    'status' => 'success',
                    'user' => ([
                        'token' => $created_user->auth_key,
                        'firstName' => $created_user->name,
                        'lastName' => $created_user->surname,
                        'email' => $created_user->email,
                    ]),
                ]);
            } else {
                return response()->json([
                    'status' => 'error',
                ]);
            }
        }


    }

    public function updateUserInfo(Request $request)
    {
        $user = User::where('id', $this->id)->first();
        if ($user) {
            if (!$request->email && !$request->phone && !$request->address) {
                return $user;
            } else {
                if ($request->email) {
                    $user->email = $request->email;
                }
                if ($request->phone) {
                    $user->phone = $request->phone;
                }
                if ($request->address) {
                    $user->address = $request->address;
                }
                $saved = $user->save();
                if ($saved) {
                    return response()->json([
                        'status' => 'success',
                    ]);
                }
            }
        } else {
            return response()->json('Пользователь не найден');
        }

    }

    public function deleteUser(Request $request)
    {
        $this->validate($request, [
            'password' => 'required',
        ]);

        $user = User::where("id", $this->id)->first();
        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                $history = History::where('user_id', $user->id)->first();
                if ($history) {
                    History::where('user_id', $user->id)->delete();
                }
                $orders = Orders::where('client_id', $user->id)->with('orderProducts')->get()->toArray();
                if (!empty($orders)) {
                    foreach ($orders as $order) {
                        foreach ($order['order_products'] as $order_product) {
                            if ($order_product) {
                                OrderProducts::where('id', $order_product['id'])->delete();
                            }
                        };
                    }
                    Orders::where('client_id', $user->id)->delete();
                }
                $wish = Wish::where('user_id', $user->id)->first();
                if ($wish) {
                    Wish::where('user_id', $user->id)->delete();
                }
//                $review = Review::where('user_id',$user->id)->first();
//                if ($review){
//                    Review::where('user_id',$user->id)->delete();
//                }

                $cart = Cart::where('user_id', $user->id)->first();
                if ($cart) {
                    Cart::where('user_id', $user->id)->delete();
                }

                $user->delete();
                return response()->json([
                    'status' => 'Sucess',
                ]);

            } else {
                return response()->json([
                    'status' => 'Error',
                ]);
            }
        }else{
            return response()->json([
                'status' => 'Error',
            ]);
        }
    }

///////////////////////////////////////
    //function for confirmed user//
///////////////////////////////////////
    public function confirmed(Request $request)
    {
        $this->validate($request, [
            'confirmed_token' => 'required ',
            'email' => 'required ',
        ]);
        $confirmed = $request->confirmed_token;
        $email = $request->email;
        $user = User::where("confirmed_token", $confirmed)->where("email", $email)->first();
        if (isset($user)) {
            $user->status = 1;
            $user->confirmed_token = null;
            $user->save();
            $getUser = $this->userAllData($request, false, $user->auth_key);
            return response()->json($getUser);
        } else {
            return response()->json([
                'status' => 'Error',
                'message' => 'You have already confirmed your page'
            ]);
        }
    }

///////////////////////////////////////
    //function for get user data//
///////////////////////////////////////
    public function userLogin(Request $request)
    {
        $api = false;
        $this->validate($request, [
            'email' => 'required ',
            'password' => 'required',
        ]);

        $user = User::where("email", $request->email)->first();
        if ($user) {
            $token = $user->auth_key;
            if (Hash::check($request->password, $user->password)) {
                return response()->json([
                    'status' => 'Success',
                    'user' => $this->userAllData($request, $api, $token)
                ]);

            } else {
                return response()->json([
                    'status' => 'Error',
                    'message' => 'The email or password incorrectly entered'
                ]);
            }
        } else {
            return response()->json([
                'status' => 'Error',
                'message' => 'The email or password incorrectly entered'
            ]);
        }
    }

/////////////////////////////////////////////
    //function for forgot password user//
/////////////////////////////////////////////
    public function forgotPassword(Request $request)
    {
        $this->validate($request, [
            'email' => 'required ',
        ]);
        $user = User::where("email", $request->email)->first();
        if($user){
            if ($user->confirmed_token == null) {

                $user->confirmed_token = Str::random(20);
                $user->save();
                $token = $user->confirmed_token;
                $name = $user->name;
                $surname = $user->surname;
                \Mail::to($user->email)->send(new SendForgotPasswordEmail($token, $name, $surname));

                return response()->json([
                    'status' => 'Success',
                    'message' => 'Пожалуйста, проверьте ваш адрес электронной почты.'
                ]);
            } else {
                return response()->json([
                    'status' => 'Error',
                    'message' => 'Ваш аддрес электронной почты не подтвержден.'
                ]);
            }
        }else{
            return response()->json([
                'status' => 'Error',
                'message' => 'Неверный адрес электронной почты.'
            ]);
        }

    }

/////////////////////////////////////////////
    //function for reset password user//
/////////////////////////////////////////////
    public function resetPassword(Request $request)
    {
        $this->validate($request, [
            'confirmed_token' => 'required ',
            'password' => 'required | min:8',
        ]);

        $user = User::where("confirmed_token", $request->confirmed_token)->first();
        if ($user) {
            $user->password = Hash::make($request->password);
            $user->confirmed_token = null;
            $user->save();
            return response()->json([
                'status' => 'Success',
                'message' => 'Your password has been restored successfully'
            ]);
        } else {
            return response()->json([
                'status' => 'Error',
                'message' => 'User does not exists'
            ]);
        }

    }

    public function getUserOrders(Request $request)
    {
        $arrayProductDetailImages = [];

        $arrayOrders = [];
        $user = User::where("id", $this->id)->first();
        if ($user) {
            $orders = Orders::where("client_id", $user->id)->get();
            if (count($orders) > 0) {
                for ($i = 0; $i < count($orders); $i++) {
                    $orderProducts = OrderProducts::where("order_id", $orders[$i]['id'])->get();
                    $arrayOrdersProductDetails = [];

                    for ($j = 0; $j < count($orderProducts); $j++) {
                        $productDetail = ProductDetails::where("id", $orderProducts[$j]->product_details_id)->with('product')->firstOrfail();

                        $productCategory = ProductCategory::where("product_id", $productDetail->product_id)->get();
                        $quantityCategory = count($productCategory);

//                        $arrayProductDetailCategories = [];
//                        for ($k = 0; $k < $quantityCategory; $k++) {
//                            $category = Category::where("id", $productCategory[$k]['category_id'])->firstOrfail();
//                            $arrayProductCategory = ([
//                                'name' => $category->title
//                            ]);
//                            array_push($arrayProductDetailCategories, $arrayProductCategory);
//                        }

                        $images = ProductImages::where("product_detail_id", $productDetail['id'])->get();
                        $arrayProductDetailImages = [];

                        for ($k = 0; $k < count($images); $k++) {
                            if ($images[$k]->imageDetail) {
                                $imageName = $images[$k]->imageDetail->uniq_name;
                                $iamgeExt = $images[$k]->imageDetail->ext;
                                $array4 = ([
                                    'path' => env('APP_URL') . '/storage/media/product' . "/" . $imageName,
                                    'ext' => $iamgeExt
                                ]);
                            } else {
                                $array4 = [];
                            }

                            array_push($arrayProductDetailImages, $array4);
                        }

//                        $discounted = '';
//                        if ($orderProducts[$j]->product_price < $productDetail['price']) {
//                            $discounted = 'true';
//                        } else {
//                            $discounted = 'false';
//                        }

                        $arrayProductDetail = ([
//                            'id' => $orderProducts[$j]['product_details_id'],
                            'product_id' => $productDetail->product_id,
                            'product_name' => $productDetail->product->name,
                            'product_detail_id' => $productDetail->id,
                            'quantity' => $orderProducts[$j]['quantity'],
                            'price' => $orderProducts[$j]['product_price'],
//                            'real_price' => $productDetail['price'],
                            'size' => $productDetail->size,
                            'color' => $productDetail->color,
//                            'discounted' => $discounted,
                            'images' => $arrayProductDetailImages,
//                            'categories' => $arrayProductDetailCategories

                        ]);
                        array_push($arrayOrdersProductDetails, $arrayProductDetail);
                    }
                    if ($orders[$i]['status'] == 0) {
                        $status = 'waiting';
                    } else if ($orders[$i]['status'] == 1) {
                        $status = 'inProcess';
                    } else {
                        $status = 'done';
                    }
                    $arrayOrder = ([
                        'id' => $orders[$i]['id'],
                        'status' => $status,
                        'total_price' => $orders[$i]['total_price'],
                        'created_at' => $orders[$i]['created_at'],
                        'product_details' => $arrayOrdersProductDetails,
                    ]);
                    array_push($arrayOrders, $arrayOrder);
                }
            } else {
                $arrayOrders = [];
            }
            return response()->json($arrayOrders);
        } else {
            return response()->json([
                'status' => 'Error',
                'message' => 'User does not exists'
            ]);

        }
    }

//////////////////////////////////////////
    //function for get all data user//
//////////////////////////////////////////
    public function userAllData(Request $request, $api = true, $sended_token = null)
    {
        if ($sended_token == null) {
            $validator = Validator::make($request->header(), [
                'token' => 'required ',
            ]);
            if ($validator->fails()) {
                return null;
            }
        }
        $arrayProductDetailImages = [];

        $arrayOrders = [];
        if ($sended_token == null) {
            $user = User::where("id", $this->id)->first();
        } else {
            $user = User::where('auth_key', $sended_token)->first();
        }
        if ($user) {
//                $orders = Orders::where("client_id", $user->id)->get();
//                if(count($orders) != 0)
//                {
//                    for($i = 0;$i < count($orders);$i++)
//                    {
//                        $orderProducts = OrderProducts::where("order_id", $orders[$i]['id'])->get();
//                        $arrayOrdersProductDetails = [];
//
//                        for($j = 0;$j < count($orderProducts);$j++)
//                        {
//                            $productDetail = ProductDetails::where("id", $orderProducts[$j]->product_details_id)->firstOrfail();
//
//                            $productCategory = ProductCategory::where("product_id", $productDetail->product_id)->get();
//                            $quantityCategory = count($productCategory);
//
//                            $arrayProductDetailCategories = [];
//                            for ($k = 0; $k < $quantityCategory; $k++) {
//                                $category = Category::where("id", $productCategory[$k]['category_id'])->firstOrfail();
//                                $arrayProductCategory = ([
//                                    'name' => $category->title
//                                ]);
//                                array_push($arrayProductDetailCategories, $arrayProductCategory);
//                            }
//
//                            $images = ProductImages::where("product_detail_id", $productDetail['id'])->get();
//                            $arrayProductDetailImages = [];
//
//                            for ($k = 0; $k < count($images); $k++) {
//
//                                $imageName = $images[$k]->imageDetail->uniq_name;
//                                $iamgeExt  = $images[$k]->imageDetail->ext;
//
//                                $array4 = ([
//                                    'path' => env('APP_URL').'/storage/media/product'."/".$imageName,
//                                    'ext'  =>$iamgeExt
//                                ]);
//                                array_push($arrayProductDetailImages, $array4);
//                            }
//
//                            $discounted = '';
//                            if ($orderProducts[$j]->product_price < $productDetail['price']) {
//                                $discounted = 'true';
//                            } else {
//                                $discounted = 'false';
//                            }
//
//                            $arrayProductDetail = ([
//                                'id' => $orderProducts[$j]['product_details_id'],
//                                'product_id' => $productDetail->product_id,
//                                'quantity' => $orderProducts[$j]['quantity'],
//                                'price' =>  $orderProducts[$j]['product_price'],
//                                'real_price' => $productDetail['price'],
//                                'size' => $productDetail->size,
//                                'color' => $productDetail->color,
//                                'discounted' => $discounted,
//                                'images' => $arrayProductDetailImages,
//                                'categories' => $arrayProductDetailCategories
//
//                            ]);
//                            array_push($arrayOrdersProductDetails,$arrayProductDetail);
//                        }
//                        if ($orders[$i]['status'] == 0) {
//                            $status = 'Ожидание';
//                        } else if ($orders[$i]['status'] == 1) {
//                            $status = 'В процессе';
//                        } else {
//                            $status = 'Доставлено';
//                        }
//                        $arrayOrder = ([
//                            'id'          => $orders[$i]['id'],
//                            'status'      => $status,
//                            'total_price' => $orders[$i]['total_price'],
//                            'created_at'  => $orders[$i]['created_at'],
//                            'product_details' => $arrayOrdersProductDetails,
//                        ]);
//                        array_push($arrayOrders,$arrayOrder);
//                    }
//
//                }else{
//                    $arrayOrders = null;
//                }
            $wishes = $this->getUserWish($request, $user->id);
            $cart = $this->getCart($request, $user->id);
            $arrayFinish = ([
                'name' => $user->name,
                'surname' => $user->surname,
                'address' => $user->address,
                'email' => $user->email,
                'postal_code' => $user->postal_code,
                'phone' => $user->phone,
                'token' => $user->auth_key,
                'created' => $user->created_at,
//                    'orders'      => $arrayOrders,
                'wishes' => $wishes,
                'cart' => $cart
            ]);
            if ($api == false) {
                return $arrayFinish;
            } else {
                return response()->json($arrayFinish);
            }
        } else {
            return null;

        }

    }

    /**
     * create subscribe functionality
     */

    public function createSubscriber(Request $request)
    {
        $this->validate($request, [
            'email' => 'required | email ',
        ]);
        $subscribers = Subscriber::where('email', $request->email)->first();
        if ($subscribers) {
            return response()->json([
                'status' => 'success',
                'message' => '0'
            ]);
        }
        $subscriber = Subscriber::insert([
            'email'  => $request->email,
            'active' => true
        ]);

        if ($subscriber) {
            return response()->json([
                'status' => 'success',
                'message' => '1'
            ]);
        } else {
            return response()->json([
                'status' => 'success',
                'message' => 'Подписчик не создан'
            ]);
        }
    }

    /**
     * create unsubscribe functionality
     */
    public function unsubscribe(Request $request)
    {
        $this->validate($request, [
            'email' => 'required | email',
        ]);
        $subscriber = Subscriber::where('email', $request->email)->first();
        if ($subscriber) {
            $subscriber->active = false;
            $subscriber->save();
            return response()->json([
                'status' => 'success'
            ]);
        } else {
            return response()->json([
                'status'  => 'error',
                'message' => 'Subscriber not found'
            ]);
        }
    }


    public function getPriceByDiscount()
    {
        $collections = Collections::where('discount', '!=', null)->with('discounts')->get()->toArray();
        $collection_products = CollectionProducts::with('collection', 'productDetail')->get()->toArray();
        $collection_products_array_by_discount = [];
        foreach ($collection_products as $collection_product) {
            if ($collection_product['collection']['discount'] != null) {
                $collection_products_array_by_discount[$collection_product['product_detail_id']] = $collection_product['collection']['discount'];
            }
        }
        $product_details = ProductDetails::with('product')->get()->toArray();
        $product_details_array = [];
        foreach ($product_details as $product_detail) {
            if (isset($collection_products_array_by_discount[$product_detail['id']])) {
                $discount = Discount::where('id', $collection_products_array_by_discount[$product_detail['id']])->with('discountConditions')->first()->toArray();
                $discountConditions = DiscountCondition::where("discount_id", $discount['id'])->get();
                $discountConditionsCount = count($discountConditions);
                $discountYesNo = 0;

                for ($j = 0; $j < $discountConditionsCount; $j++) {
                    $discountCondition = $discountConditions[$j];
                    switch ($discountCondition->condition_type) {
                        case '0':// when condition_type == '='
                            if ($discountCondition->condition_key == 0) {
                                if ($discountCondition->condition_value == $product_detail['price']) {
                                    $discountYesNo++;
                                }
                            } else {
                                if ($discountCondition->condition_value == $product_detail['size']) {
                                    $discountYesNo++;
                                }
                            }
                            break;
                        case '1':// when condition_type == '>'
                            if ($discountCondition->condition_key == 0) {

                                if ($product_detail['price'] > $discountCondition->condition_value) {
                                    $discountYesNo++;
                                }
                            } else {
                                if ($product_detail['size'] > $discountCondition->condition_value) {
                                    $discountYesNo++;
                                }
                            }
                            break;
                        case '2':// when condition_type == '>='
                            if ($discountCondition->condition_key == 0) {
                                if ($product_detail['price'] >= $discountCondition->condition_value) {
                                    $discountYesNo++;
                                }
                            } else {
                                if ($product_detail['size'] >= $discountCondition->condition_value) {
                                    $discountYesNo++;
                                }
                            }
                            break;
                        case '3':// when condition_type == '<'
                            if ($discountCondition->condition_key == 0) {
                                if ($product_detail['price'] < $discountCondition->condition_value) {
                                    $discountYesNo++;
                                }
                            } else {
                                if ($product_detail['size'] < $discountCondition->condition_value) {
                                    $discountYesNo++;
                                }
                            }
                            break;
                        case '4':// when condition_type == '<='
                            if ($discountCondition->condition_key == 0) {
                                if ($product_detail['price'] <= $discountCondition->condition_value) {
                                    $discountYesNo++;
                                }
                            } else {
                                if ($product_detail['size'] <= $discountCondition->condition_value) {
                                    $discountYesNo++;
                                }
                            }
                            break;
                    }

                }

                if ($discountYesNo == $discountConditionsCount) {
                    $discountType = $discount['discount_type'];
                    $discountSize = $discount['discount_size'];
                    if ($discountType == '0') {
                        $ProductPercent = $product_detail['price'] * $discountSize / 100;
                        $new_price = $product_detail['price'] - $ProductPercent;
                    } else {
                        $new_price = $product_detail['price'] - $discountSize;
                    }
                } else {
                    $new_price = $product_detail['price'];
                }


                $product_details_array[$product_detail['id']] = $new_price;
            } else {
                if ($product_detail['discount_id'] != null) {
                    $discount = Discount::where('id', $product_detail['discount_id'])->with('discountConditions')->first()->toArray();
                    $discountConditions = DiscountCondition::where("discount_id", $discount['id'])->get();
                    $discountConditionsCount = count($discountConditions);
                    $discountYesNo = 0;

                    for ($j = 0; $j < $discountConditionsCount; $j++) {
                        $discountCondition = $discountConditions[$j];
                        switch ($discountCondition->condition_type) {
                            case '0':// when condition_type == '='
                                if ($discountCondition->condition_key == 0) {
                                    if ($discountCondition->condition_value == $product_detail['price']) {
                                        $discountYesNo++;
                                    }
                                } else {
                                    if ($discountCondition->condition_value == $product_detail['size']) {
                                        $discountYesNo++;
                                    }
                                }
                                break;
                            case '1':// when condition_type == '>'
                                if ($discountCondition->condition_key == 0) {

                                    if ($product_detail['price'] > $discountCondition->condition_value) {
                                        $discountYesNo++;
                                    }
                                } else {
                                    if ($product_detail['size'] > $discountCondition->condition_value) {
                                        $discountYesNo++;
                                    }
                                }
                                break;
                            case '2':// when condition_type == '>='
                                if ($discountCondition->condition_key == 0) {
                                    if ($product_detail['price'] >= $discountCondition->condition_value) {
                                        $discountYesNo++;
                                    }
                                } else {
                                    if ($product_detail['size'] >= $discountCondition->condition_value) {
                                        $discountYesNo++;
                                    }
                                }
                                break;
                            case '3':// when condition_type == '<'
                                if ($discountCondition->condition_key == 0) {
                                    if ($product_detail['price'] < $discountCondition->condition_value) {
                                        $discountYesNo++;
                                    }
                                } else {
                                    if ($product_detail['size'] < $discountCondition->condition_value) {
                                        $discountYesNo++;
                                    }
                                }
                                break;
                            case '4':// when condition_type == '<='
                                if ($discountCondition->condition_key == 0) {
                                    if ($product_detail['price'] <= $discountCondition->condition_value) {
                                        $discountYesNo++;
                                    }
                                } else {
                                    if ($product_detail['size'] <= $discountCondition->condition_value) {
                                        $discountYesNo++;
                                    }
                                }
                                break;
                        }

                    }

                    if ($discountYesNo == $discountConditionsCount) {
                        $discountType = $discount['discount_type'];
                        $discountSize = $discount['discount_size'];
                        if ($discountType == '0') {
                            $ProductPercent = $product_detail['price'] * $discountSize / 100;
                            $new_price = $product_detail['price'] - $ProductPercent;
                        } else {
                            $new_price = $product_detail['price'] - $discountSize;
                        }
                    } else {
                        $new_price = $product_detail['price'];
                    }
                    $product_details_array[$product_detail['id']] = $new_price;
                } else {
                    $product_details_array[$product_detail['id']] = $product_detail['price'];
                }
            }
        }
        return $product_details_array;

    }

    public function getProducts(Request $request, $list = [])
    {
        if (!empty($list)) {
            $type = $list['type'];
            $id = $list['id'];
            $limit = 12;
        }
        if (empty($list)) {
            $validator = Validator::make($request->all(), [
                'type' => 'required',
                'id' => 'required',
            ]);
        }
        $price = $this->getPriceByDiscount();
        if (empty($list) && $validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }
        if ($request->type == 'category' || $request->type == 'collection' || !empty($list)) {
            if (((isset($type)) ? $type : $request->type) == 'category') {
                $category = Category::where('id', (isset($id)) ? $id : $request->id)->with('discount')->first();
                if (!$category) {
                    return response()->json('Категория по идентификатору ' . $request->id . ' не найдена');
                }
                if ($category->discount) {
                    $discount = $category->discount->name;
                } else {
                    $discount = $category->discount;
                }

                $type = "category_name";
                $slug = $category->slug;
                $type_id = $category->id;

                $name = $category->title;
                $products_categories = ProductCategory::where('category_id', (isset($id)) ? $id : $request->id)->orderBy('product_id', 'ASC')->select('product_id')->get()->toArray();
                $description = null;
            } else if (((isset($type)) ? $type : $request->type) == 'collection') {
                $collection = Collections::where('id', (isset($id)) ? $id : $request->id)->with('discounts')->first();
                if (!$collection) {
                    return response()->json('Коллекция по идентификатору ' . (isset($id)) ? $id : $request->id . ' не найдена');
                }

                if ($collection->discounts) {
                    $discount = $collection->discounts->name;
                } else {
                    $discount = $collection->discounts;
                }
                $type = "collection_name";
                $name = $collection->name;
                $slug = $collection->slug;
                $type_id = $collection->id;
                $description = $collection->description;
                if ($collection->deletable == "no") {
                    $lastProducts = Products::orderBy('id', 'DESC')->where('status', true)->with('productDetail')->take(12)->get()->toArray();
                    $collection_products = [];
                    foreach ($lastProducts as $lastProduct) {
                        array_push($collection_products, $lastProduct['id']);
                    }
                    $products_categories = array_unique($collection_products);

                } else {
                    $collection_products_details = CollectionProducts::where('collection_id', (isset($id)) ? $id : $request->id)->orderBy('id', 'DESC')->select('product_detail_id')->get()->toArray();
                    $collection_products = [];
                    foreach ($collection_products_details as $collection_products_detail) {
                        $product_collection = ProductDetails::where('id', $collection_products_detail['product_detail_id'])->where('available', true)->select('product_id')->first();
                        array_push($collection_products, $product_collection['product_id']);
                    }
                    $products_categories = array_unique($collection_products);

                }


            }

            $products_array = [];
            $lists = [];
            $collection = [];
            foreach ($products_categories as $k => $products_category) {
                $lists[] = $products_category['product_id'];
                $collection[] = $products_category;
            }
            if ($request->type == 'category') {
                $products =  Products::whereIn('id', $lists)->where('status', true)->with('productDetail.productImages.imageDetail')->get()->toArray();
            } else if ($request->type == 'collection' || !empty($list)) {
                $products =  Products::whereIn('id', $collection)->where('status', true)->with('productDetail.productImages.imageDetail')->get()->toArray();
            }
            if ($products) {
                foreach ($products as $item) {
                    $variants = [];
                    foreach ($item['product_detail'] as $product_detail) {
                        $images = [];
                        foreach ($product_detail['product_images'] as $product_image) {
                            array_push($images, array(
                                'path' => asset('/storage/' . $product_image['image_detail']['path'] . $product_image['image_detail']['uniq_name']),
                                'ext' => $product_image['image_detail']['ext']
                            ));
                        }
                        if ($product_detail['price'] == $price[$product_detail['id']]) {
                            $old_price = null;
                        } else {
                            $old_price = $product_detail['price'];
                        }
                        array_push($variants, array(
                            'id' => $product_detail['id'],
                            'size' => $product_detail['size'],
                            'color' => $product_detail['color'],
                            'price' => $price[$product_detail['id']],
                            'old-price' => $old_price,
                            'available' => $product_detail['available'],
                            'images' => $images
                        ));
                    }
                    array_push($products_array, array(
                        'product_id' => $item['id'],
                        'name' => $item['name'],
                        'variants' => $variants
                    ));
                }
            }

//            foreach ($products_categories as $k => $products_category) {
//                $variants = [];
//                if ($request->type == 'category') {
//                    $product = Products::where('id', $products_category['product_id'])->where('status', true)->with('productDetail')->first();
//                } else if ($request->type == 'collection' || !empty($list)) {
//                    $product = Products::where('id', $products_category)->where('status', true)->with('productDetail')->first();
//                }
//                if ($product) {
//                    foreach ($product->productDetail as $productDetail) {
//                        $images = [];
//                        $images_array = ProductImages::where('product_detail_id', $productDetail->id)->select('image')->get()->toArray();
//                        if ($images_array) {
//                            foreach ($images_array as $image) {
//                                $media = Media::where('id', $image['image'])->first();
//                                if ($media) {
//                                    array_push($images, array(
//                                        'path' => asset('/storage/' . $media->path . $media->uniq_name),
//                                        'ext' => $media->ext
//                                    ));
//                                } else {
//                                    array_push($images, []);
//                                }
//                            }
//                        } else {
//                            $images = null;
//                        }
//
//                        if ($productDetail->price == $price[$productDetail->id]) {
//                            $old_price = null;
//                        } else {
//                            $old_price = $productDetail->price;
//                        }
//                        array_push($variants, array(
//                            'id' => $productDetail->id,
//                            'size' => $productDetail->size,
//                            'color' => $productDetail->color,
//                            'price' => $price[$productDetail->id],
//                            'old_price' => $old_price,
//                            'available' => $productDetail->available,
//                            'images' => $images
//
//                        ));
//
//                    }
//                    array_push($products_array, array(
//                        'product_id' => $product->id,
//                        'name' => $product->name,
//                        'variants' => $variants
//                    ));
//                }
//            }

            if (!empty($list)) {
                $array = [
                    $type => $name,
                    'id' => $type_id,
                    'discount' => $discount,
                    'description' => $description,
                    'slug' => $slug,
                    'products' => $products_array
                ];
                return $array;
            } else {
                $array = [
                    'type' => $request->type,
                    $type => $name,
                    'id' => $type_id,
                    'discount' => $discount,
                    'slug' => $slug,
                    'products' => $products_array
                ];
                return response()->json($array);
            }

        } else {
            return response()->json('Тип должен бить category или collection');
        }
    }


    /**
     * Create get Category By Part
     */


    public function getCategoryByPart(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'count' => 'required'
        ]);
        $categories = Category::where('id', $request->id)->first();
        $products_array = [];
        if ($categories) {
            $childs = [];
            if ($categories->childs){
                $childs = $this->manageChild($categories->childs, $api = true);
                foreach ($childs as $child) {
                    $products_array[$child['product_id']] = $child;
                }
            } else {
                $childs = null;
            }
        } else {
            return response()->json('Error');
        }
        $products = $this->getProducts($request, array('type' => 'category', 'id' => $request->id))['products'];

        foreach ($products as $product) {
            $products_array[$product['product_id']] = $product;
        }
        $products_list = [];
        foreach ($products_array as $product_in_array) {
            array_push($products_list, $product_in_array);
        }

//        $page = ! empty($request->count ) ? $request->count : 1;
//        $total = count( $products_list );
//        $limit = 2;
//        $totalPages = ceil( $total/ $limit );
//        $page = max($page, 1);
//        $page = min($page, $totalPages);
//        $offset = ($page - 1) * $limit;
//        if( $offset < 0 ) {
//            $offset = 0;
//        }
//        $products_list = array_slice( $products_list, $offset, $limit );

        $page = !empty($request->count) ? $request->count : 1;
        $limit = 8;
        $list = array_chunk($products_list, $limit);
        if (isset($list[$page - 1])) {
            $products_list = $list[$page - 1];
        } else {
            $products_list = end($list);
        }
        if ((count($list)) > $page) {
            $canGet = true;
        } else {
            $canGet = false;
        }
        return response()->json([
                'id' => $categories->id,
                'name' => $categories->title,
                'slug' => $categories->slug,
                'canGet' => $canGet,
                'products' => $products_list,
            ]
        );
    }


    /**
     * Create get Collection By Part
     */

    public function getCollectionByPart(Request $request){
        $this->validate($request, [
            'id' => 'required',
            'count' => 'required'
        ]);
        $collections = Collections::where('id', $request->id)->first();
        $products_array = [];
        if ($collections) {
            $childs = [];
            if ($collections->childs){
                $childs = $this->manageChild($collections->childs, $api = true);
                foreach ($childs as $child) {
                    $products_array[$child['product_id']] = $child;
                }
            } else {
                $childs = null;
            }
        } else {
            return response()->json('Error');
        }
        $products = $this->getProducts($request, array('type' => 'collection', 'id' => $request->id))['products'];

        foreach ($products as $product) {
            $products_array[$product['product_id']] = $product;
        }
        $products_list = [];
        foreach ($products_array as $product_in_array) {
            array_push($products_list, $product_in_array);
        }
        $page = !empty($request->count) ? $request->count : 1;
        $limit = 8;
        $list = array_chunk($products_list, $limit);
        $count = count($list);
        if (isset($list[$page - 1])) {
            $products_list = $list[$page - 1];
        } else {
            $products_list = end($list);
        }
        if ($count > $page) {
            $canGet = true;
        } else {
            $canGet = false;
        }
        return response()->json([
                'id' => $collections->id,
                'name' => $collections->name,
                'slug' => $collections->slug,
                'canGet' => $canGet,
                'products' => $products_list,
            ]
        );
    }


    public function getCategoryByProducts(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);
        $categories = Category::where('id', $request->id)->first();
        $arrayCategories = [];
        $products_array = [];
        if ($categories) {
            $childs = [];
            if (count($categories->childs)) {
                $childs = $this->manageChild($categories->childs, $api = true);
                foreach ($childs as $child) {
                    $products_array[$child['product_id']] = $child;
                }
            } else {
                $childs = null;
            }
            $products = $this->getProducts($request, array('type' => 'category', 'id' => $request->id))['products'];
            $arrayCategory = ([
                'id' => $categories->id,
                'title' => $categories->title,
                'slug' => $categories->slug,
                'childs' => $childs,
                'products' => $products
            ]);
            array_push($arrayCategories, $arrayCategory);
            foreach ($products as $product) {
                $products_array[$product['product_id']] = $product;
            }

        } else {
            return response()->json('Error');
        }
//        if (!empty($arrayCategories)){
//            if (!empty($arrayCategories[0]['products'])){
//                array_push($products_array,$arrayCategories[0]['products']);
//            }
//            if (!empty($arrayCategories[0]['childs'])){
//                array_push($products_array,$this->childProduct($arrayCategories[0]['childs']));
//
//            }
//        }
        $products_list = [];
        foreach ($products_array as $product_in_array) {
            array_push($products_list, $product_in_array);
        }
        return response()->json([
                'name' => $categories->title,
                'slug' => $categories->slug,
                'products' => $products_list,
            ]
        );

    }


    public function childProduct($array = [], $products_array = null)
    {
        if ($products_array == null) {
            $product_arr = [];
        } else {
            $product_arr = $products_array;
        }

        if (!empty($array)) {
            if (!empty($array[0]['products'])) {
                array_push($product_arr, $array[0]['products']);
            }
            if (!empty($array[0]['childs'])) {
                $this->childProduct($array[0]['childs'], $product_arr);
            }
        }
        return $product_arr;

    }

//////////////////////////////////////////////////////
    //function for get product max and min price//
//////////////////////////////////////////////////////
    public function getProductsMaxMinPrice()
    {
        $price = DB::select('SELECT MAX(Price) AS MaxPrice, MIN(Price) AS MinPrice FROM product_details');
        return [
            'maxPrice' => $price[0]->MaxPrice,
            'minPrice' => $price[0]->MinPrice
        ];
    }

////////////////////////////////////
    //function for get product//
////////////////////////////////////
    public function getProduct(Request $request, $id = null)
    {

        if ($id != null) {
            $product = Products::where("id", $id)->firstOrfail();
        } else {
            $this->validate($request, [
                'id' => 'required | numeric',
            ]);
            $product = Products::where("id", $request->id)->firstOrfail();
        }

        $price = $this->getPriceByDiscount();
        if ($product->status == true) {
//            if ($product['discount_id'] != null) {
//                $discount = $product->discount->name;
//            } else {
//                $discount = null;
//            }

            $productCategoryQuantity = [];
            $productCategories = ProductCategory::where("product_id", $product->id)->get();
            $productCategoriesCount = count($productCategories);
            for ($j = 0; $j < $productCategoriesCount; $j++) {
                $cat = ([
                    'title' => $productCategories[$j]->category->title,
                ]);
                array_push($productCategoryQuantity, $cat);
            }

            $productDetailQuantity = [];
            $productDetails = ProductDetails::where("product_id", $product->id)->get();
            $productDetailsCount = count($productDetails);
            for ($k = 0; $k < $productDetailsCount; $k++) {
                if ($productDetails[$k]->quantity != 0) {
                    $productImageQuantity = [];
                    $productImages = ProductImages::where("product_detail_id", $productDetails[$k]->id)->get();
                    $productImagesCount = count($productImages);
                    for ($l = 0; $l < $productImagesCount; $l++) {
                        $app_url = env('APP_URL');
                        if ($productImages[$l]->imageDetail) {
                            $image = "$app_url/storage/" . $productImages[$l]->imageDetail->path . "" . $productImages[$l]->imageDetail->uniq_name;
                            $images = ([
                                'path' => $image,
                                'ext' => $productImages[$l]->imageDetail->ext
                            ]);
                        } else {
                            $images = [];
                        }
                        array_push($productImageQuantity, $images);
                    }
                    if ($productDetails[$k]->price == $price[$productDetails[$k]->id]) {
                        $old_price = null;
                    } else {
                        $old_price = $productDetails[$k]->price;
                    }

                    $details = ([
                        'id' => $productDetails[$k]->id,
                        'size' => $productDetails[$k]->size,
                        'color' => $productDetails[$k]->color,
                        'price' => $price[$productDetails[$k]->id],
                        'old_price' => $old_price,
                        'images' => $productImageQuantity,
                    ]);
                    array_push($productDetailQuantity, $details);
                }
            }
            $pruductArray = ([
                'name' => $product['name'],
                'product_id' => $product['id'],
                'description' => $product->description,
                'variants' => $productDetailQuantity,
            ]);
            if ($id != null) {
                return $pruductArray;
            }
            return response()->json([
                'product' => $pruductArray,
            ]);
        } else {
            if ($id != null) {
                return '';
            }
            return response()->json([
                'status' => 'Error',
                'message' => 'This item has expired'
            ]);
        }
    }

    public function getProductAndSmiliarProduct(Request $request)
    {
        $this->validate($request, [
            'id' => 'required | numeric',
        ]);
        $product = Products::where("id", $request->id)->with('productDetail.productImages.imageDetail')->with('category')->first()->toArray();
        $smiliarProducts = [];
        $productCat = [];
        foreach($product['category'] as $category) {
            $productCategories = ProductCategory::where('category_id', $category['category_id'])->with('products')->get();
            foreach ($productCategories as $productCategory) {
                if (isset($productCategory->products[0]->id)) {
                    array_push($productCat, $productCategory->products);
                }
            }
            foreach ($productCat as $value) {
                if($request->id != $value[0]->id){
                    array_push($smiliarProducts, $value[0]->id);
                }
            }
        }
        $productId = array_unique($smiliarProducts);
        $arrayFinish = [];
        if(count($productId) <= 4){
            foreach($productId as $value){
                array_push($arrayFinish,$this->getProduct($request,$value));
            }
        }else{
            $random_keys=array_rand($productId,4);
            foreach($random_keys as $value){
                array_push($arrayFinish,$this->getProduct($request,$productId[$value]));
            }
        }

        return response()->json([
            'product' => $this->getProduct($request,$request->id),
            'smiliar_products' => $arrayFinish
        ]);

    }

    public function getHeaderMenus($api = true)
    {
        $headerMenus = Menu::All();
        $arrayHeaderMenus = [];
        if ($headerMenus) {
            $countHeaderMenus = count($headerMenus);
            for ($k = 0; $k < $countHeaderMenus; $k++) {
                $arrayHeaderMenu = ([
                    'title' => $headerMenus[$k]->title,
                    'slug' => $headerMenus[$k]->slug
                ]);
                array_push($arrayHeaderMenus, $arrayHeaderMenu);
            }
        }
        if ($api == false) {
            return $arrayHeaderMenus;
        } else {
            return response()->json([
                'status' => 'Success',
                'menus' => $arrayHeaderMenus
            ]);
        }
    }

    public function manageChild($childs = null, $api = false, &$arr = [])
    {
        $k = 0;
        $request = new \Illuminate\Http\Request();
        $childs_array = [];
        if ($api == false) {
            if ($childs != null) {
                foreach ($childs as $key => $child) {
                    if (sizeof($child->childs) > 0) {
                        $childs = $this->manageChild($child->childs);
                    } else {
                        $childs = null;
                    }
//                    $products = $this->getProducts($request, array('type' => 'category', 'id' => $child->id));
                    $childs_in_array = [
                        'id' => $child->id,
                        'title' => $child->title,
                        'slug' => $child->slug,
                        'childs' => $childs,
//                        'products' => $products

                    ];

                    array_push($childs_array, $childs_in_array);
                    if($childs == null){
                        $products = ProductCategory::where("category_id",$child->id)->first();
                        if(!$products){
                            foreach($childs_array as $key => $child_array){
                                if($child->id == $child_array['id']){
                                    unset($childs_array[$key]);
                                }
                            }
                        }
                    }
//
                }
                if(sizeof($childs_array) > 0){
                    return $childs_array;

                }else{
                    return;
                }
//                dd($childs_array);
            }
        } else {
            if ($childs != null) {
                foreach ($childs as $child) {

                    $products = $this->getProducts($request, array('type' => 'category', 'id' => $child->id));
                    $childs_in_array = [
                        'id' => $child->id,
                        'title' => $child->title,
                        'slug' => $child->slug,
                        'childs' => $childs,
                        'products' => $products['products']

                    ];
                    array_push($childs_array, $childs_in_array);
                    if (!empty($products['products'])) {
                        array_push($arr, $products['products'][0]);
                    }

                    if ($child->childs != null) {
                        $childs = $this->manageChild($child->childs, true, $arr);
                    } else {
                        $childs = null;
                    }
                }
                return $arr;
            }
        }

    }

    public function getHomePageData(Request $request)
    {
        $api = false;
        $categories = Category::where('parent_id', '=', null)->get();
        $arrayCategories = [];

        if ($categories) {
            $childs = [];
            foreach ($categories as $category) {
//                if (count($category->childs)) {
                    $childs = $this->manageChild($category->childs);
//                } else {
//                    $childs = null;
//                }
                $arrayCategory = ([
                    'id' => $category->id,
                    'title' => $category->title,
                    'slug' => $category->slug,
                    'childs' => $childs,
                ]);

                array_push($arrayCategories, $arrayCategory);
            }
            foreach($arrayCategories as $key => $arrayCategory){
                if($arrayCategory['childs'] == null){
                    $products = ProductCategory::where("category_id",$arrayCategory['id'])->first();
                    if(!$products){
                        unset($arrayCategories[$key]);
                    }
                }
            }
            if(sizeof($arrayCategories) == 0){
                $arrayCategories = null;
            }
        }
        $arrayCategories = array_values($arrayCategories);

        $sliders = Slider::orderBy("sort")->get();
        $arraySliders = [];
        if ($sliders) {
            $countSlider = count($sliders);
            for ($j = 0; $j < $countSlider; $j++) {
                $arraySlider = ([
                    'image' => asset('storage/sliders/' . $sliders[$j]->image),
                    'ext' => $sliders[$j]->ext,
                    'product_id' => $sliders[$j]->product_id
                ]);
                array_push($arraySliders, $arraySlider);
            }
        }

        $settings = Settings::All();
        $arraysettings = [];
        if ($settings) {
            $countsettings = count($settings);
            for ($l = 0; $l < $countsettings; $l++) {
                $arraySetting = ([
                    'key' => $settings[$l]->key,
                    'value' => $settings[$l]->value
                ]);
                array_push($arraysettings, $arraySetting);
            }
        }
        $collections = Collections::orderBy('order', 'ASC')->get();
        $arrayCollections = [];
        $array_collections = [];
        if ($collections) {
            foreach ($collections as $collection) {
                $arrayCollection = $this->getProducts($request, array('type' => 'collection', 'id' => $collection->id));
                if(sizeof($arrayCollection['products']) > 0){
                    array_push($arrayCollections, $arrayCollection);
                }
            }
        }


        foreach($arrayCollections as $key => $arrayCollection){
            $count = count($arrayCollection['products']);
            if($count <= 8){
                $arraycollection = $arrayCollection;
            }
            else{
                $arrayCollections[$key]['products'] =  array_slice($arrayCollections[$key]['products'], -8,8);
            }
        }

        return response()->json([
            'categories' => $arrayCategories,
            'sliders' => $arraySliders,
            'collections' => $arrayCollections,
            'header_menus' => $this->getHeaderMenus($api),
            'settings' => $arraysettings,
            'user' => $this->userAllData($request, $api),
            'minMaxPrice' => $this->getProductsMaxMinPrice(),
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function sendEmailContact(Request $request)
    {
        $this->validate($request, [
            'email' => 'required | email',
            'subject' => 'required',
            'text' => 'required',
            'phone' => 'required',
            'name' => 'required'
        ]);

        $mianiMail = env("MAIL_USERNAME");

        $messagea = ([
            'email' => $request->email,
            'subject' => $request->subject,
            'text' => $request->text,
            'phone' => $request->phone,
            'name' => $request->name,
        ]);

        view('emails.sendContactEmail')->with('messagea', $messagea);

        \Mail::send('emails.sendContactEmail', ['messagea' => $messagea, 'mianiMail' => $mianiMail], function ($m) use ($messagea, $mianiMail) {
            $m->from($messagea['email'], $messagea['email']);

            $m->to($mianiMail, $messagea['name'])->subject($messagea['subject']);
        });

        return response()->json([
            'message' => 'Success'
        ]);
    }

    public function searchProducts(Request $request)
    {
        if ($request->get('minprice') || $request->get('maxprice') || $request->get('color') || $request->get('name'))
        {
            $products = ProductDetails::where('available', true)->with('product');
//            ->where('price','>=',$request->get('min-price'))
//            ->where('price','<=',$request->get('max-price'))
//            ->where('color',$request->get('color'))
//            ->with('product')->get();
//            dd($request->get('minprice'));
            if ($request->get('minprice')) {
                $products->where('price', '>=', $request->get('minprice'));
            }

            if ($request->get('maxprice')) {
                $products->where('price', '<=', $request->get('maxprice'));
            }
            $color =$request->get('color');
            $color_int = (int)$color;
            if ($color_int) {
                $products->where('color', $request->get('color'));
            }
            if ($request->get('name')) {
                $products->whereHas('product', function ($query) use ($request) {
                    $query->where('name', 'like', "%{$request->name}%");
                });
            }
            $getProducts = $products->orderBy('id', 'DESC')->get();
            $products_array = [];
            $products_filter = [];
            foreach ($getProducts as $k => $item) {
                array_push($products_array, $item->product->id);
            }
            $products_array = array_unique($products_array);
            foreach ($products_array as $product_in_array) {
                array_push($products_filter, $this->getProduct($request, $product_in_array));
            }

            return $products_filter;

//        $color = ProductDetails::getColor();
//        $price = $this->getPriceByDiscount();
//        if ($getProducts){
//            foreach ($getProducts as $k => $item){
//
//                if ($item->discount_id){
//                    $discount = Discount::where('id',$item->discount_id)->first();
//                    $discount_name = $discount->name;
//                }else{
//                    $discount_name = null;
//                }
//                if ($item->price == $price[$item->id]){
//                    $old_price = null;
//                }else{
//                    $old_price = $item->price;
//                }
//                array_push($product_array,[
//                   'id' => $item->id,
//                   'product_id' => $item->product_id,
//                   'rate' => $item->rate,
//                   'size' => $item->size,
//                   'color' => $color[$item->color],
//                   'quantity' => $item->id,
//                   'available' => $item->quantity,
//                   'other_details' => $item->other_details,
//                   'old_price' => $old_price,
//                   'price' => $price[$item->id],
//                   'discount_id' => $discount_name,
//                   'product' => [
//                       'id' => $item->product->id,
//                       'name' => $item->product->name,
//                       'caption' => $item->product->caption,
//                       'description' => $item->product->description,
//                       'status' => $item->product->status
//                   ],
//                ]);
//            }
//        }
            return response()->json([
                'products' => $products_filter
            ]);

        } else {
            return response()->json([
                'products' => [],
            ]);
        }

    }

    public function addCart(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_detail_id' => 'required | numeric',
            'quantity' => 'required | numeric',
        ]);
        if ($request->header('token') == null) {
            return 'token required';
        }
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }
        $issetet_product_in_cart = Cart::where('user_id', $this->id)->where('product_detail_id', $request->product_detail_id)->first();
        if ($issetet_product_in_cart) {
            return response()->json('пользователь уже добавил продукт на карту');
        }
        $user = User::where('id', $this->id)->first();
        if (!$user) {
            return response()->json('Пользователь не найден');
        }
        $product_detail = ProductDetails::where('id', $request->product_detail_id)->first();
        if (!$product_detail) {
            return response()->json('Деталь продукта по идентификатору ' . $request->product_detail_id . ' не найден');
        }

        $data = new Cart();
        $data->user_id = $this->id;
        $data->product_detail_id = $request->product_detail_id;
        $data->quantity = $request->quantity;
        $saved = $data->save();
        if ($saved) {
            return response()->json($data);
        }
    }

//    public function getCart(Request $request){
//        $validator = Validator::make($request->all(), [
//            'token' => 'required',
//        ]);
//
//        if ($validator->fails())
//        {
//            return response()->json(['errors'=>$validator->errors()->all()]);
//        }
//
//        $user = User::where('id',$this->id)->with('cart.productDetail.product')->first();
//        if (!$user){
//            return response()->json('Пользователь по идентификатору '.$request->user_id.' не найден');
//        }
//        $products_array = [];
//        foreach ($user->cart as $cart){
//            $products_array[$cart->productDetail->product_id] = [
//                'name' =>$cart->productDetail->product->name,
//                'status' =>$cart->productDetail->product->status,
//                'variants' => [],
//            ];
//            array_push($products_array[$cart->productDetail->product_id]['variants'],11);
//        }
//        $price = $this->getPriceByDiscount();
//        foreach ($user->cart as $cart){
//
//            if ($cart->productDetail->price == $price[$cart->productDetail->id]){
//                $old_price = null;
//            }else{
//                $old_price = $cart->productDetail->price;
//            }
//            array_push($products_array[$cart->productDetail->product_id]['variants'],[
//                'cart_quantity' => $cart->quantity,
//                'product_id' => $cart->productDetail->product_id,
//                'rate' => $cart->productDetail->rate,
//                'size' => $cart->productDetail->size,
//                'color' => $cart->productDetail->color,
//                'product_detail_quantity' => $cart->productDetail->quantity,
//                'available' => $cart->productDetail->available,
//                'other_details' =>$cart->productDetail->other_details,
//                'old_price' => $old_price,
//                'price' => $price[$cart->productDetail->id],
//            ]);
//        }
//        $array = [
//            'name' =>$user->name,
//            'surname' =>$user->surname,
//            'products' => $products_array
//
//        ];
//
//        return response()->json($array);
//    }

    public function getCart(Request $request, $cart = false)
    {
        if ($cart == false) {
            $user = User::where('id', $this->id)->with('cart.productDetail.product')->first();
        } else {
            $user = User::where('id', $cart)->with('cart.productDetail.product')->first();
        }


        if (!$user) {
            return response()->json('Пользователь по идентификатору ' . $request->user_id . ' не найден');
        }
        $products_array = [];
        foreach ($user->cart as $cart) {
            $products_array[$cart->productDetail->product_id] = [
                'name' => $cart->productDetail->product->name,
                'variants' => [],
            ];
        }
        $price = $this->getPriceByDiscount();
        foreach ($user->cart as $cart) {

            if ($cart->productDetail->price == $price[$cart->productDetail->id]) {
                $old_price = null;
            } else {
                $old_price = $cart->productDetail->price;
            }
            $images_array = [];
            $images = ProductImages::where('product_detail_id', $cart->productDetail->id)->with('imageDetail')->get();
            $app_url = env('APP_URL');
            foreach ($images as $item) {
                if ($item->imageDetail) {
                    $image = "$app_url/storage/" . $item->imageDetail->path . "" . $item->imageDetail->uniq_name;
                    $images = ([
                        'path' => $image,
                        'ext' => $item->imageDetail->ext
                    ]);
                } else {
                    $images = [];
                }
                array_push($images_array, $images);
            }
            array_push($products_array[$cart->productDetail->product_id]['variants'], [
//                'cart_id' => $cart->id,
                'product_id' => $cart->productDetail->product_id,
                'product_detail_id' => $cart->productDetail->id,
                'cart_quantity' => $cart->quantity,
                'rate' => $cart->productDetail->rate,
                'size' => $cart->productDetail->size,
                'color' => $cart->productDetail->color,
                'product_detail_quantity' => $cart->productDetail->quantity,
                'price' => $price[$cart->productDetail->id],
                'images' => $images_array,
            ]);
        }
        $products = [];
        foreach ($products_array as $product_in_array) {
            array_push($products, $product_in_array);
        }
        $array = [
            'products' => $products

        ];
        if ($cart == true) {
            return $array;
        }
        return response()->json($array);
    }


    /**
     * Create FAQ API functionality
     */

    public function getFaq(Request $request){
        $faq_array = [];
        $faq = Faq::get()->toArray();
        foreach ($faq as $item) {
            array_push($faq_array, array(
                'question' => $item['question'],
                'answer'   => $item['answer']
            ));
        }
        return response()->json([
            'Faqs' => $faq_array
        ]);
    }
}
