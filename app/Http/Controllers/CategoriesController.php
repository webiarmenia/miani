<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Discount;
use App\Media;
use App\ProductCategory;
use Illuminate\Support\Str;
//use Illuminate\Support\Facades\Validator;

class CategoriesController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(){
        $categories = Category::orderBy('id','DESC')->paginate(10);
        return view('admin.categories.index')->with([
            'categories' => $categories
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $media = Media::paginate(15);
        $discounts = Discount::get();
        $categories = Category::get();
        return view('admin.categories.create')->with([
            'discounts' => $discounts,
            'categories' => $categories,
            'media' => $media,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required | unique_with:categories,parent_id',
            'slug'  => 'required | alpha_dash | unique:categories',
//          'cover_id' => 'required',
//          'icon'     => 'required',
        ]);

        $category = new Category([
            'title'       => $request->get('title'),
//          'cover_id'    => $request->get('cover_id'),
            'parent_id'   => $request->get('parent_id'),
            'slug'   => $request->get('slug'),
//          'icon'        => $request->get('icon'),
//          'discount_id' => $request->get('discount_id'),
        ]);
        $category->save();
        return redirect()->route('admin.categories')->with('successCreate',  'Категория Успешно Создана');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $media_cover = Media::paginate(15);
        $item = Category::where('id',$id)->firstOrfail();
        $categories = Category::all();
//        $media = Media::where('id',$item->cover_id)->firstOrfail();
        $discounts = Discount::all();
        return view('admin.categories.edit')->with([
            'item' => $item,
            'categories' => $categories,
            'discounts' => $discounts,
            'media_cover' => $media_cover,
//            'media' => $media
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required | unique_with:categories,parent_id,'.$id,
            'slug'  => 'required | alpha_dash | unique:categories,slug,'.$id,
//            'icon'  => 'required',
        ]);

        $category = Category::where('id', $id)->firstOrfail();
        $category->title = $request->get('title');
//        $category->cover_id    = $request->get('cover_id');
        $category->parent_id = $request->get('parent_id');
        $category->slug = $request->get('slug');
//        $category->icon        = $request->get('icon');
//        $category->discount_id = $request->get('discount_id');
        $category->save();
        return redirect()->route('admin.categories')->with('successUpdate', 'Категория Успешно Обновлено');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $productCategories = ProductCategory::where("category_id", $id)->get();

        if(count($productCategories) == 0)
        {
            $category = Category::where('id',$id)->firstOrfail();
            $childs = Category::where("parent_id", $id)->get();
            $childs->parent_id = $category->parent_id;
            foreach($childs as $child)
            {
                $child->parent_id = $category->parent_id;
                $child->save();
            }

            $category->Delete();
            return back()->with('successDelete', 'Категория Успешно Удалено');
        }else{
            return back()->with('error', 'Категория невозможно удалить');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function sortableIndex()
    {
        $parentId = null;
        $permissionRecord = Category::orderBy('order')->get();
        $htmlStructure = "";
        $this->buildTree($permissionRecord, $parentId,$htmlStructure);
        return view('admin/categories/sortable')->with(['html' => $htmlStructure]);

    }

    public function buildTree($permissionRecord, $parent_id, &$html)
    {

        $a = Str::random(4);
        $html .= '<ol class="dd-list">';

        foreach ($permissionRecord as $row) {

            if ($row->parent_id == $parent_id) {
                $html .= '<li class="dd-item" id=' . $row->id . ' data-parent-id=' . 0 . ' data-order=' . 0 . '>' . '<div class="dd-handle">' . $row->title . '<span>' . ' ' . $row->order . '</span></div>';

                $m = Category::where('parent_id', $row->id)->get();

                if(!empty($m[0]->title)){
                    $html .= $this->buildTree($permissionRecord, $row->id, $html);
                }

                $html .= '</li>';
            }

        }

        $html .= '</ol>';

    }

    public function sortable()
    {
        $l = count($_POST["id"]);
        for($i = 0; $i<$l; $i++)
        {
            $item = Category::where('id', $_POST["id"][$i])->firstOrfail();

            if($_POST["data_parent_id"][$i] == 0)
            {
                $item->parent_id = null;
                $item->order     = $_POST["order"][$i];
            }else
            {
                $item->parent_id = $_POST["data_parent_id"][$i];
                $item->order     = $_POST["order"][$i];
            }
            $item->save();

        }
    }

}
