<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slider;
use App\Products;
use Illuminate\Support\Facades\Input;
use File;
use Illuminate\Support\Facades\Validator;
use Image;
use Carbon\Carbon;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = Slider::orderBy('sort')->paginate(10);
        return view('admin.sliders.index')->with(['sliders' => $sliders]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Products::All();
        return view('admin.sliders.create')->with(['products' => $products]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'image' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $quantity = Slider::All();
        if (count($quantity) == 5) {
            return redirect()->route('admin.sliders')->with('errorCreate', 'Вы не можете создать новый слайдер');
        } else {
            $sizes = array(array('360', '300'), array('745', '400'), array('1100', '500'));

            $originalImage = getimagesize($request->file('image'));
            $imgss = '';
            $cash = str_random(32);
            $ext = $request->file('image')->getClientOriginalExtension();

            $filename = $cash . "." . $ext;
            $request->file('image')->storeAs('public/sliders', $filename);
            $thumbnailPath = storage_path('app/public/sliders/' . $filename);
            if ($originalImage[0] < $sizes[2][0] && $originalImage[1] < $sizes[2][1]) {
                if ($sizes[2][0] / $originalImage[0] > $sizes[2][1] / $originalImage[1]) {
                    $newHeight = round($sizes[2][0] / $originalImage[0] * $originalImage[1]);
                    $imgss = Image::make(storage_path('app/public/sliders/' . $filename))->resize($sizes[2][0], $newHeight)->save($thumbnailPath);
                } else {
                    $newWidth = round($sizes[2][1] / $originalImage[1] * $originalImage[0]);
                    $imgss = Image::make(storage_path('app/public/sliders/' . $filename))->resize($newWidth, $sizes[2][1])->save($thumbnailPath);
                }
            } else if ($originalImage[0] < $sizes[2][0]) {
                $newHeight = round($sizes[2][0] / $originalImage[0] * $originalImage[1]);
                $imgss = Image::make(storage_path('app/public/sliders/' . $filename))->resize($sizes[2][0], $newHeight)->save($thumbnailPath);

            } else if ($originalImage[1] < $sizes[2][1]) {
                $newWidth = round($sizes[2][1] / $originalImage[1] * $originalImage[0]);
                $imgss = Image::make(storage_path('app/public/sliders/' . $filename))->resize($newWidth, $sizes[2][1])->save($thumbnailPath);
            }
            $sizes_name = array('small', 'medium', 'large');
            $originalImagea = getimagesize(storage_path('app/public/sliders/' . $filename));
            foreach ($sizes_name as $key => $type) {
                $filenamea = $cash . "_" . "$type" . "." . $ext;
                $thumb_size1 = $sizes[$key][0];
                $thumb_size2 = $sizes[$key][1];
                $thumbnailPath = storage_path('app/public/sliders/' . $filenamea);
                $x = round(($originalImagea[0] - $thumb_size1) / 2);
                $y = round(($originalImagea[1] - $thumb_size2) / 5);
                $img = Image::make(storage_path('app/public/sliders/' . $filename))->crop($thumb_size1, $thumb_size2, $x, $y)->save($thumbnailPath);
            }
            unlink(storage_path('app/public/sliders/' . $filename));
            $sort = null;
            if ($request->sort == null) {
                $sort = 1;
            } else {
                $sort = $request->sort;
            }

            $slider = Slider::insert([
                'product_id' => $request->product_id,
                'image' => $cash,
                'sort' => $sort,
                'ext' => $ext,
                'created_at' => Carbon::now()
            ]);

            if ($slider) {
                return redirect()->route('admin.sliders')->with('successCreate', 'Слайдер Успешно Создана');
            } else {
                return back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $products = Products::All();
        $item = Slider::where('id', $id)->firstOrfail();
        return view('admin.sliders.edit')->with(['item' => $item, 'products' => $products]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'sort' => 'required',
        ]);
        $slider = Slider::find($id);
        $oldImagename = $slider->image;
        if (Input::hasFile('image')) {
            $sizes = array(array('360', '300'), array('745', '400'), array('1100', '500'));
            $originalImage = getimagesize($request->file('image'));
            $imgss = '';
            $cash = str_random(32);
            $ext = $request->file('image')->getClientOriginalExtension();
            $filename = $cash . "." . $ext;
            $request->file('image')->storeAs('public/sliders', $filename);
            $thumbnailPath = storage_path('app/public/sliders/' . $filename);
            if ($originalImage[0] < $sizes[2][0] && $originalImage[1] < $sizes[2][1]) {
                if ($sizes[2][0] / $originalImage[0] > $sizes[2][1] / $originalImage[1]) {
                    $newHeight = round($sizes[2][0] / $originalImage[0] * $originalImage[1]);
                    $imgss = Image::make(storage_path('app/public/sliders/' . $filename))->resize($sizes[2][0], $newHeight)->save($thumbnailPath);
                } else {
                    $newWidth = round($sizes[2][1] / $originalImage[1] * $originalImage[0]);
                    $imgss = Image::make(storage_path('app/public/sliders/' . $filename))->resize($newWidth, $sizes[2][1])->save($thumbnailPath);
                }
            } else if ($originalImage[0] < $sizes[2][0]) {
                $newHeight = round($sizes[2][0] / $originalImage[0] * $originalImage[1]);
                $imgss = Image::make(storage_path('app/public/sliders/' . $filename))->resize($sizes[2][0], $newHeight)->save($thumbnailPath);

            } else if ($originalImage[1] < $sizes[2][1]) {
                $newWidth = round($sizes[2][1] / $originalImage[1] * $originalImage[0]);
                $imgss = Image::make(storage_path('app/public/sliders/' . $filename))->resize($newWidth, $sizes[2][1])->save($thumbnailPath);
            }
            $sizes_name = array('small', 'medium', 'large');
            $ext = $request->file('image')->getClientOriginalExtension();
            $originalImagea = getimagesize(storage_path('app/public/sliders/' . $filename));
            foreach ($sizes_name as $key => $type) {
                $sliderabc = $oldImagename . "_" . $sizes_name[$key] . "." . $slider->ext;
                $usersImage = storage_path("app/public/sliders/" . $sliderabc); // get previous image from folder
                if (File::exists($usersImage)) { // unlink or remove previous image from folder
                    unlink($usersImage);
                }
                $filenamea = $cash . "_" . "$type" . "." . $ext;
                $thumb_size1 = $sizes[$key][0];
                $thumb_size2 = $sizes[$key][1];
                $thumbnailPath = storage_path('app/public/sliders/' . $filenamea);
                $x = round(($originalImagea[0] - $thumb_size1) / 2);
                $y = round(($originalImagea[1] - $thumb_size2) / 5);
                $img = Image::make(storage_path('app/public/sliders/' . $filename))->crop($thumb_size1, $thumb_size2, $x, $y)->save($thumbnailPath);
            }
            unlink(storage_path('app/public/sliders/' . $filename));

            $slider->image = $cash;
            $slider->ext = $ext;
        }
        $slider->product_id = $request->get('product_id');
        $slider->sort = $request->get('sort');
        $slider->updated_at = Carbon::now();

        $slider->save();
        return redirect()->route('admin.sliders')->with('successUpdate', 'Слайдер Успешно Обновлено');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider = Slider::where("id", $id)->firstOrfail();
        Slider::destroy($id);
        $sizes_name = array('small', 'medium', 'large');

        foreach($sizes_name as $key => $value){
            $sliderabc = $slider->image . "_" . $sizes_name[$key] . "." . $slider->ext;
            $usersImage = storage_path("app/public/sliders/" . $sliderabc); // get previous image from folder
            if (File::exists($usersImage)) { // unlink or remove previous image from folder
                unlink($usersImage);
            }
        }

        return back()->with('successDeleteYes', 'Слайдер Успешно Удалено');
    }
}