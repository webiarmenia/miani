<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Faq;

class FaqController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $faqs = Faq::orderBy('id', 'desc')->paginate(10);
        return view('admin.faq.index')->with([
            'faqs' =>$faqs
        ]);
    }

    /**0
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        return view('admin.faq.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $request->validate([
            'question'=>'required'
        ]);
        $faqs = new Faq([
            'question' => $request->get('question'),
            'answer'=> $request->get('answer'),
        ]);
        $faqs->save();
        return redirect()->route('admin.faqs')->with('success', 'FAQ успешно добавлено');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $faqs = Faq::where('id',$id)->firstOrfail();
        return view('admin.faq.edit')->with([
            'faqs' => $faqs,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $request->validate([
            'question'=>'required',
        ]);
        $faqs = Faq::where('id',$id)->firstOrfail();
        $faqs->question = $request->get('question');
        $faqs->answer = $request->get('answer');
        $faqs->save();
        return redirect()->route('admin.faqs')->with('success', 'FAQ успешно обновлено');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $faqs = Faq::where('id',$id)->firstOrfail();
        $faqs->delete();
        return redirect()->route('admin.faqs')->with([
            'faqs' =>$faqs
        ])->with('success', 'FAQ успешно удалено');
    }
}
