<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use App\Page;

class MenusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = Menu::orderBy('id', 'DESC')->paginate(10);
        return view('admin.menus.index')->with(['menus' => $menus]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pages = Page::All();
        $menus = Menu::All();
        return view('admin.menus.create')->with(['menus' => $menus,'pages' => $pages]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'    => 'required | unique:menus',
//            'position' => 'required',
            'address'  => 'required | unique:menus',
            'slug'     => 'required | alpha_dash | max:25 | unique:menus',
        ]);

        $menu = new Menu([
            'title'     => $request->get('title'),
//            'parent_id' => $request->get('parent_id'),
            'position'  => $request->get('position'),
            'address'   => $request->get('address'),
            'slug'      => $request->get('slug'),
        ]);

        $saved = $menu->save();
        if ($saved){
            return redirect()->route('admin.menus')->with('successCreate',  'Меню Успешно Создана');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $pages = Page::All();
        $item = Menu::where('id',$id)->firstOrfail();
        $menus = Menu::all();
        return view('admin.menus.edit')->with(['item' => $item, 'menus' => $menus, 'pages' => $pages]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'   => 'required | unique:menus,title,' . $id,
//            'position' => 'required',
            'address' => 'required | unique:menus,address,' . $id,
            'slug'    => 'required | alpha_dash | max:25 | unique:menus,slug,' . $id,
        ]);

        $menu = Menu::where('id', $id)->firstOrfail();

        $menu->title = $request->get('title');
//        $menu->parent_id = $request->get('parent_id');
        $menu->position = $request->get('position');
        $menu->address = $request->get('address');
        $menu->slug = $request->get('slug');

        $menu->save();
        return redirect()->route('admin.menus')->with('successUpdate', 'Меню Успешно Обновлено');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $menu = Menu::find($id);

        $menu->delete();
        return back()->with('successDeleteYes', 'Меню Успешно Удалено');

    }

//
//    public function sortableIndex()
//    {
//        $parentId = null;
//        $permissionRecord = Menu::where('position', 1)->orderBy('order')->get();
//
//        $htmlStructure = "";
//        $this->buildTree($permissionRecord, $parentId,$htmlStructure);
//        return view('admin/menus/sortable')->with(['html' => $htmlStructure]);
//    }
//
//
//
//
//    public function buildTree($permissionRecord, $parent_id, &$html)
//    {
//
//        $html .= '<ol class="dd-list" id="sortable">';
//
//        foreach ($permissionRecord as $row) {
//
//            if ($row->parent_id == $parent_id) {
//
//                $html .= '<li class="dd-item" id=' . $row->id . ' data-parent-id=' . 0 . ' data-order=' . 0 . ' >' . '<div class="dd-handle">' . $row->title . '<span>' . ' order-' . $row->order . '</span></div>';
//
//                $m = Menu::where('parent_id', $row->id)->get();
//
//                if(!empty($m[0]->title)){
//                    $html .= $this->buildTree($permissionRecord, $row->id, $html);
//                }
//
//                $html .= '</li>';
//            }
//
//        }
//
//        $html .= '</ol>';
//
//    }
//
//    public function sortable()
//    {
//        $l = count($_POST["id"]);
//        for($i = 0; $i<$l; $i++)
//        {
//            $item = Menu::where('id', $_POST["id"][$i])->firstOrfail();
//
//            if($_POST["data_parent_id"][$i] == 0)
//            {
//                $item->parent_id = null;
//                $item->order     = $_POST["order"][$i];
//            }else
//            {
//                $item->parent_id = $_POST["data_parent_id"][$i];
//                $item->order     = $_POST["order"][$i];
//            }
//            $item->save();
//
//        }
//    }

}
