<?php

namespace App\Http\Controllers;

use App\Category;
use App\Discount;
use App\DiscountCondition;
use App\ProductDetails;
use App\Products;
use Carbon\Carbon;
use foo\bar;
use Illuminate\Http\Request;

class DiscountsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $discounts = Discount::orderBy('id','DESC')->orderBy('id','DESC')->paginate(10);
        return view('admin.discounts.index')->with([
            'discounts' =>$discounts
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $lists = DiscountCondition::getList();
        $types = DiscountCondition::getType();
        return view('admin.discounts.create')->with([
            'lists' => $lists,
            'types' => $types,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $this->validate($request, [
            'name' => 'required | unique:discounts',
            'discount_size' => 'required | numeric',
            'discount_type' => 'required | boolean',
            'condition_type.*' => 'required',
            'condition_key.*' => 'required',
            'condition_value.*' => 'required',
            'date_to' => 'required | date_format:m/d/Y',
            'date_from' => 'required | date_format:m/d/Y',
        ]);

        $date_from = Carbon::parse($request->date_from);
        $date_to = Carbon::parse($request->date_to);
        if ($date_from > $date_to){
            return back()->with('error','Дата от не может быть больше чем Дата до');
        }
            $length = count($request->condition_type);
            $discountConditons = [];
            $data = new Discount();
            $data->name = $request->name;
            $data->discount_type =$request->discount_type;
            $data->discount_size =$request->discount_size;
            $data->discount_description = $request->discount_description;
            $data->date_from = $date_from;
            $data->date_to = $date_to;
            $saved = $data->save();
            if($saved){
                for ($i=0;$i<$length;$i++){
                    array_push($discountConditons,
                        array('discount_id' =>$data->id,
                            'condition_type' => $request->condition_type[$i],
                            'condition_key' =>$request->condition_key[$i],
                            'condition_value' =>$request->condition_value[$i])
                    );
                }

                $createDiscountContidions = DiscountCondition::insert($discountConditons);
                if ($createDiscountContidions){
                    return redirect()->route('admin.discounts')->with('success', 'Скидка '.'<strong>'.$request->get('name').'</strong>'.' успешно добавлено');
                }
            }else{
                return back();
            }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

        $item = Discount::where('id',$id)
            ->with('discountConditions')
            ->firstOrfail();

        $type = DiscountCondition::getType()[$item->discount_type];

        return view('admin.discounts.view')->with([
            'item' => $item,
            'type' => $type
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $item = Discount::where('id',$id)->with('discountConditions')->firstOrfail();
        $lists = DiscountCondition::getList();
        $types = DiscountCondition::getType();
        return view('admin.discounts.edit')->with([
            'item' => $item,
            'lists' => $lists,
            'types' => $types,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $discount = Discount::where('id',$id)->firstOrfail();
        $this->validate($request, [
            'name' => 'required | unique:discounts,name,'.$id,
            'discount_size' => 'required | numeric',
            'discount_type' => 'required | boolean',
            'condition_type.*' => 'required',
            'condition_key.*' => 'required',
            'condition_value.*' => 'required',
            'date_to' => 'required | date_format:m/d/Y',
            'date_from' => 'required | date_format:m/d/Y',
        ]);
        $date_from = Carbon::parse($request->date_from);
        $date_to = Carbon::parse($request->date_to);
        if ($date_from > $date_to){
            return back()->with('error','Дата от не может быть больше чем Дата до');
        }
        if (!isset($request->condition_type) || !isset($request->condition_key) || !isset($request->condition_value)){
            return back()->with('error','Условия скидки обязательно');
        }else{
            if ($discount){
                $updateDiscount = Discount::where('id',$id)->update([
                'name' => $request->name,
                'discount_type' => $request->discount_type,
                'discount_size' => $request->discount_size,
                'discount_description' => $request->discount_description,
                'date_from' => $date_from,
                'date_to' => $date_to,
                ]);
                if ($updateDiscount){
                    $DiscountCondition = DiscountCondition::where('discount_id',$discount->id)->get();
                        if ($DiscountCondition){
                            DiscountCondition::where('discount_id',$discount->id)->delete();
                        }
                        $discountConditons = [];
                        $length = count($request->condition_type);
                        for ($i = 0; $i < $length; $i++) {
                            array_push($discountConditons,
                                array('discount_id' => $discount->id,
                                    'condition_type' => $request->condition_type[$i],
                                    'condition_key' => $request->condition_key[$i],
                                    'condition_value' => $request->condition_value[$i])
                            );
                        }
                        $createDiscountContidions = DiscountCondition::insert($discountConditons);
                        if ($createDiscountContidions) {
                            return redirect()->route('admin.discounts')->with('success', 'Скидка '.'<strong>'.$request->get('name').'</strong>'.' успешно обновлено');
                        } else {
                            return back();
                        }


                }else{
                    return back();
                }
            }else{
                return back();
            }

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function destroy($id)
    {
        //

        $discount = Discount::where('id',$id)->firstOrfail();
        $name = $discount->name;
        if($discount){
                $categories = Category::where('discount_id',$id)->get();
                $products = ProductDetails::where('discount_id',$id)->get();

            if ($categories->count()){
                    $items = '';
                    foreach ($categories as $k => $category){
                        $items .= ($k != 0)? ' , '.$category->title:$category->title;
                    }
                    $msg = 'Скидку невозможно удалить.Категория '.'<strong>'.$items.'</strong>'.' использует эту скидку';
                    return redirect()->route('admin.discounts')->with('error', $msg);
                }
                if ($products->count()){
                    $items = '';
                    foreach ($products as $k => $product){
                        $items .= ($k != 0)? ' , '.$product->id:$product->id;
                    }
                    $msg = 'Скидку '.'<strong>'.$name.'</strong>'.' невозможно удалить.Детали продукта по идентификатору '.'<strong>'.$items.'</strong>'.' использует эту скидку';
                    return redirect()->route('admin.discounts')->with('error', $msg);
                }
            $discountCanditions = DiscountCondition::where('discount_id',$discount->id)->get();
            if ($discountCanditions){
            DiscountCondition::where('discount_id',$discount->id)->delete();
            }
            $discount->delete();
            return redirect()->route('admin.discounts')->with('success', 'Скидка '.'<strong>'.$name.'</strong>'.' успешно удалено');
        }
        return back();
    }
}
