<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Media;
use Illuminate\Support\Facades\Input;
use File;
use Illuminate\Support\Facades\Validator;
use Image;
use Spatie\ImageOptimizer\OptimizerChainFactory;

class MediaController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $media = Media::orderBy('created_at', 'desc')->paginate(10);
        return view('admin.media.index')->with([
            'media' =>$media
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create(){
        return view('admin.media.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request){
        if ($request->ajax()) {
            $validator = Validator::make($request->all(), [
                'type' => 'required',
                'cover' => 'required |mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]);
            if ($validator->fails()) {
                return response()->json(['validation' => $validator->errors()->all()]);
            }
        } else {
            $request->validate([
                'type' => 'required',
                'cover' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]);
        }
        $sizes = array(array('250', '250'), array('250', '250'), array('850', '850'));
        $sizes_name = array('small','medium','large');
        $targetPath = 'media/' . $request->get('type').'/';
        $file = Input::file('cover');
        if ($request->hasfile('cover')){
            $fname = $request->file('cover')->getClientOriginalName();
            $ext = $request->file('cover')->getClientOriginalExtension();
            $nameWithOutExt = str_replace('.' . $ext, '', $fname);
            $cash = str_random(32);
            $filename = $cash . "." . $ext;
            foreach ($sizes_name as $key => $type) {
                $nameWithExt = $cash . '_' . $type . '.' . $ext;
                if ($type == "medium") {
                    $file_hash = $request->file('cover')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->fit($sizes[1][0], $sizes[1][1], null, "top-left")->save();
                } else if ($type == "small") {
                    $file_hash = $request->file('cover')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->fit($sizes[0][0], $sizes[0][1], null, "top-left")->save();
                }else if ($type == "large") {
                    $file_hash = $request->file('cover')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->fit($sizes[2][0], $sizes[2][1], null, "top-left")->save();
                }
            }
        }
        $media = new Media([
            'path' => $targetPath,
            'uniq_name' => $cash,
            'ext' => $ext,
            'type' => $request->get('type'),
            'original_name' => $nameWithOutExt,
        ]);
        $media->save();
        if ($request->ajax() && $media->save()){
            $media = Media::orderBy('id','DESC')->paginate(15);
            $images = explode(',',$request->images);
            if (count($images) == 0 || $request->images == "undefined"){
                $images = false;
            }
            return view('admin.products.pagination_data', compact('media','images'))->render();
        }else {
            return redirect()->route('admin.media')->with('success', 'Картинка успешно добавлено');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id){
        $media = Media::where('id',$id)->firstOrfail();
        return view('admin.media.edit')->with([
            'media' => $media
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $media = Media::where('id', $id)->firstOrfail();
        $request->validate([
            'cover' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        $sizes = array(array('250', '250'), array('250', '250'), array('850', '850'));
        $sizes_name = array('small', 'medium', 'large');
        $targetPath = 'media/' . $request->get('type') . '/';
        $file = Input::file('cover');
        if ($request->get('type') != $media->type) {
            if (!file_exists('storage' . '/media/' . $request->get('type'))) {
                File::makeDirectory('storage' . '/media/' . $request->get('type'));
            }
            foreach ($sizes_name as $key => $type) {
                $file_name_old = $media->type . '/' . $media->uniq_name . '_' . $type . '.' . $media->ext;
                $file_name_new = $request->get('type') . '/' . $media->uniq_name . '_' . $type . '.' . $media->ext;
                $usersImage = storage_path("app/public/media/" . $file_name_old);
                if (is_file($usersImage)) {
                    $oldPath = storage_path("app/public/media/" . $file_name_old);
                    $newPath = storage_path("app/public/media/" . $file_name_new);
                    \File::copy($oldPath, $newPath);
                }
                unlink(storage_path('app/public/media/' . $file_name_old));
            }
            $media->path = 'media/' . $request->get('type') . '/';
            $media->type = $request->get('type');
            $media->save();
        }
        if ($request->hasFile('cover')) {
            $fname = $request->file('cover')->getClientOriginalName();
            $ext = $request->file('cover')->getClientOriginalExtension();
            $nameWithOutExt = str_replace('.' . $ext, '', $fname);
            $cash = str_random(32);
            $filename = $cash . "." . $ext;
            $targetPath = $media->path;
            foreach ($sizes_name as $key => $type) {
                $filename = $cash . '_' . $type . '.' . $ext;
                if ($type == "medium") {
                    $file_hash = $request->file('cover')->storeAs('public/' . $targetPath, $filename);
                    $source = storage_path() . '/app/public/' . $targetPath . $filename;
                    $image = Image::make($source)->fit($sizes[1][0], $sizes[1][1], null, "top-left")->save();
                } else if ($type == "small") {
                    $file_hash = $request->file('cover')->storeAs('public/' . $targetPath, $filename);
                    $source = storage_path() . '/app/public/' . $targetPath . $filename;
                    $image = Image::make($source)->fit($sizes[0][0], $sizes[0][1], null, "top-left")->save();
                } else if ($type == "large") {
                    $file_hash = $request->file('cover')->storeAs('public/' . $targetPath, $filename);
                    $source = storage_path() . '/app/public/' . $targetPath . $filename;
                    $image = Image::make($source)->fit($sizes[2][0], $sizes[2][1], null, "top-left")->save();
                }
                $file_name_old = $media->path . $media->uniq_name . '_' . $type . '.' . $media->ext;
                if (is_file(storage_path() . '/app/public' . '/' . $file_name_old)) {
                    unlink(storage_path() . '/app/public' . '/' . $file_name_old);
                }
                $media->original_name = $nameWithOutExt;
                $media->ext = $ext;
                $media->uniq_name = $cash;
                $media->save();
            }
        }
        return redirect()->route('admin.media')->with('success', 'Картинка успешно обновлено');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id){
        $media = Media::where('id',$id)->firstOrfail();
        $sizes_name = array('large', 'medium', 'small');
        foreach ($sizes_name as $key => $type) {
            $file_name = $media->path . $media->uniq_name . "_" . $type . "." . $media->ext;
            $usersImage = storage_path("app/public/" . $file_name);

            if (File::exists($usersImage)) { // unlink or remove previous image from folder
                unlink($usersImage);
            }
        }
        $media->delete();
        return redirect()->route('admin.media')->with([
            'media' =>$media
        ])->with('success', 'Картинка успешно удалено');
    }
}
