<?php

namespace App\Http\Controllers;

use App\CollectionProducts;
use App\Collections;
use App\Discount;
use App\ProductDetails;
use App\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CollectionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $collections = Collections::orderBy('id', 'ASC')->with('discounts')->paginate(10);
        return view('admin.collections.index')->with([
            'collections' => $collections
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $discounts = Discount::all();
        $products = Products::where('status',true)->whereHas('productDetail', function ($query) {
            $query->where('available',true );
        })->orderBy('id', 'DESC')->with('productDetail')->get();
        $lists = ProductDetails::getColor();
        $selected = [];
        $Detail = ProductDetails::getColor();
        $collections = Collections::all()->toArray();


        return view('admin.collections.create')->with([
            'discounts' => $discounts,
            'products' => $products,
            'lists' => $lists,
            'selected' => $selected,
            'Detail' => $Detail,
            'collections' => $collections
        ]);

    }

//    function fetch_data(Request $request)
//    {
//        if ($request->ajax()) {
//            $products = Products::orderBy('id', 'DESC')->paginate(5);
//            $selected = explode(',', $request->selected);
//            $lists = ProductDetails::getColor();
//            if (count($selected) == 0 || $request->selected == "undefined") {
//                $selected = [];
//
//            }
//            return view('admin.collections.product_data', compact('products', 'selected','lists'))->render();
//        }
//    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $collections = Collections::get()->toArray();

        if (!$collections){
                if ($request->discount != null){
                    $msg = ['Коллекция Last Products не может иметь скидки'];
                    return response()->json(['errors'=>$msg]);
                }
                if ($request->details_selected != null){
                    $msg = ['Коллекция Last Products не может иметь выбранные продукты , в нем автоматически добавляются последние 15 продуктов'];
                    return response()->json(['errors'=>$msg]);
                }

                $validator = Validator::make($request->all(), [
                    'slug'  => 'required'
                ]);
                if ($validator->fails()) {
                    return response()->json(['errors' => $validator->errors()->all()]);
                }
                $data = new Collections();
                $data->name = $request->name;
                $data->discount = $request->discount;
                $data->description = $request->description;
                $data->order = 1;
                $data->slug = $request->slug;
                $data->deletable = 'no';
                $saved = $data->save();
                if ($saved){
                    return response()->json(['created'=>true]);
                }
        }else {
            if (count($collections) < 5) {
//                if ($request->name == "Last Products") {
//                    $msg = ['коллекция Last Products уже имеется'];
//                    return response()->json(['errors' => $msg]);
//                }

                $selected = $request->details_selected;
                if ($request->discount != null && $selected != null) {
                    $isseted_collection_products_array = [];
                    $isseted_collection_products = CollectionProducts::with('collection')->get();
                    foreach ($isseted_collection_products as $isseted_collection_product) {
                        if ($isseted_collection_product->collection->discount != null) {
                            array_push($isseted_collection_products_array, $isseted_collection_product->product_detail_id);
                        }
                    }
                    $issetet_discounts = [];
                    foreach ($selected as $k => $selected_product) {
                        if (in_array($selected_product, $isseted_collection_products_array)) {
                            array_push($issetet_discounts, $selected_product);
                        }
                    }
                    if (!empty($issetet_discounts)) {
                        $msg = [];
                        foreach ($issetet_discounts as $k => $issetet_discount) {
                            array_push($msg, 'Продукт по Идентификатору <strong>' . $issetet_discount . '</strong> уже уже использует скидочную коллекцию');
                        }
                        return response()->json(['errors' => $msg]);
                    }

                }
                $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'description' => 'required | max:18',
                    'details_selected' => 'required',
                    'order' => 'required',
                    'slug' => 'required | alpha_dash | unique:collections',
                ]);
                if ($validator->fails()) {
                    return response()->json(['errors' => $validator->errors()->all()]);
                }

                $data = new Collections();
                $data->name = $request->name;
                $data->discount = $request->discount;
                $data->description = $request->description;
                $data->order = $request->order;
                $data->slug = $request->slug;
                $data->deletable = 'yes';
                $saved = $data->save();
                $details_selected = [];
                if ($saved) {
                    foreach ($selected as $k => $item) {
                        array_push($details_selected, array(
                            'collection_id' => $data->id,
                            'product_detail_id' => $item,
                            'top' => false
                        ));
                    }
                    $collectionProducts = CollectionProducts::insert($details_selected);
                    if ($collectionProducts) {
                        return response()->json(['created' => true]);
                    } else {
                        return back();
                    }

                } else {
                    return back();
                }
            }else{
                $msg = ['максимум число коллекции не должен превышать 5'];
                return response()->json(['errors'=>$msg]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $item = Collections::where('id',$id)->with('collectionProducts')->firstOrfail();
        $discounts = Discount::all();
        if ($item->name == "Last Products"){
            $products = Products::orderBy('id','DESC')->where('status',true)->whereHas('productDetail', function ($query) {
                $query->where('available',true );
            })->with('productDetail')->take(15)->get();
        }else{
            $products = Products::orderBy('id', 'DESC')->where('status',true)->whereHas('productDetail', function ($query) {
                $query->where('available',true );
            })->with('productDetail')->get();
        }
        $lists = ProductDetails::getColor();
        $selected = [];
        $Detail = ProductDetails::getColor();
        $collectionProducts = CollectionProducts::where('collection_id',$id)->select('product_detail_id')->get()->toArray();
        foreach ($collectionProducts as $collectionProduct){
            array_push($selected,$collectionProduct['product_detail_id']);
        }

        return view('admin.collections.edit')->with([
            'item' => $item,
            'discounts' => $discounts,
            'products' => $products,
            'lists' => $lists,
            'selected' => $selected,
            'Detail' => $Detail,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $collection = Collections::where('id',$id)->firstOrfail();

        $selected = $request->details_selected;
        if ($request->discount != null && $selected!= null){
            $isseted_collection_products_array = [];
            $isseted_collection_products = CollectionProducts::with('collection')->get();
            foreach ($isseted_collection_products as $isseted_collection_product){
                if ($isseted_collection_product->collection->discount != null && $isseted_collection_product->collection->id != $collection->id ){
                    array_push($isseted_collection_products_array,$isseted_collection_product->product_detail_id);
                }
            }
            $issetet_discounts =[];
            foreach ($selected as $k => $selected_product){
                if (in_array($selected_product,$isseted_collection_products_array)){
                    array_push($issetet_discounts,$selected_product);
                }
            }
            if (!empty($issetet_discounts)){
                $msg = [];
                foreach ($issetet_discounts as $k => $issetet_discount){
                    array_push($msg,'Продукт по Идентификатору <strong>'.$issetet_discount.'</strong> уже уже использует скидочную коллекцию');
                }
                return response()->json(['errors'=>$msg]);
            }

        }
        if ($collection->deletable == 'no'){
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'description' => 'required | max:18',
                'slug' => 'required | alpha_dash | unique:collections,slug,'.$id,
            ]);
        }else{
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'description' => 'required | max:18',
                'details_selected' => 'required',
                'order' => 'required',
                'slug' => 'required | alpha_dash | unique:collections,slug,'.$id,
            ]);
        }



        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        $collection->name = $request->name;
        $collection->discount = $request->discount;
        $collection->description = $request->description;
        if ($collection->deletable == 'no'){
            $collection->order = 1;
        }else{
            $collection->order = $request->order;

        }
        $collection->slug = $request->slug;
        $collection->save();
        if ($collection->deletable == 'no'){
            return response()->json(['created' => true]);
        }
        $collection_products = CollectionProducts::where('collection_id',$id)->select('product_detail_id')->get()->toArray();
        $isseted_collections = [];
        foreach ($collection_products as $collection_product){
            array_push($isseted_collections,$collection_product['product_detail_id']);
        }
        $new_products_details =[];
        foreach ($selected as $k => $item){
            if (in_array($item,$isseted_collections)){
                if (($key = array_search($item, $isseted_collections)) !== false) {
                    unset($isseted_collections[$key]);
                }
            }else{
                array_push($new_products_details,array(
                    'collection_id' => $id,
                    'product_detail_id' => $item,
                ));
            }
        }

        if (!empty($isseted_collections)){
            foreach ($isseted_collections as $isseted_collection){
                CollectionProducts::where('product_detail_id',$isseted_collection)->where('collection_id',$collection->id)->delete();
            }
        }
        if (!empty($new_products_details)){
            CollectionProducts::insert($new_products_details);
        }
        return response()->json(['created' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $collection = Collections::where('id',$id)->firstOrfail();
        if ($collection->deletable == "no"){
            abort(404);
        }
        $collection_products = CollectionProducts::where('collection_id',$id)->get()->toArray();
        if (!empty($collection_products)){
            CollectionProducts::where('collection_id',$id)->delete();
        }
        $collection->delete();
        $msg = 'Колекциая '.'<strong>'.$collection->name.'</strong>'.' успешно удалено';
        return redirect()->route('admin.collections')->with('success', $msg);
    }
}
