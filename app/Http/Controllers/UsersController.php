<?php

namespace App\Http\Controllers;

use App\History;
use App\Subscriber;
use Illuminate\Http\Request;
use App\User;
use App\ProductDetails;

class UsersController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $users = User::orderBy('id', 'desc')->paginate(10);
        return view('admin.user.index')->with([
            'users' => $users
        ]);
    }

    public function getSubscribers(){
        $subsribers = Subscriber::orderBy('id','DESC')->paginate(10);
        return view('admin.subscribers.index')->with([
            'subscribers' => $subsribers
        ]);
    }

    public function userChange(Request $request,$id){
        $users = User::where('id',$id)->firstOrfail();
        if ($users->status == 30){
            return redirect()->route('admin.users')->with('error',  'Статус Админа не может быть изменен ');
        }
        if($users->status == true){
            $statusValue = false;
        }else{
            $statusValue = true;
        }
        User::where('id',$id)->update([
                'status' => $statusValue
            ]);
        return back();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create(){

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $color = ProductDetails::getColor();
        $users = User::where('id',$id)->with('wishes.productDetail.product','cart.productDetail.product')->with('orders')->firstOrfail();
        return view('admin.user.show')->with([
            'users' => $users,
            'color' => $color,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){

    }

}
