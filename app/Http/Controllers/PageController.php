<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\Media;

class PageController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $pages = Page::orderBy('id', 'desc')->with('media')->paginate(10);
        return view('admin.page.index')->with([
            'pages' =>$pages
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $media = Media::where('type','other')->orderBy('id','DESC')->paginate(15);
        $pages = Page::all();
        return view('admin.page.create')->with([
            'media'  =>$media,
            'pages' =>$pages
        ]);
    }
    function fetch_data(Request $request){
        if($request->ajax()){
            $media = Media::where('type','other')->orderBy('id','DESC')->paginate(15);
            $images = explode(',',$request->images);
            if (count($images) ==0 || $request->images == "undefined"){
                $images = false;
            }
            return view('admin.page.page_data', compact('media','images'))->render();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $request->validate([
            'title' => 'required|unique:pages',
            'content'=> 'required'
        ]);
        $pages = new Page([
            'title' => $request->get('title'),
            'content'=> $request->get('content'),
            'cover' => $request->get('cover')
        ]);
        $pages->save();
        return redirect()->route('admin.pages')->with('success', 'Страница '.'<strong>'.$request->get('title').'</strong>'.' успешно добавлено');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $pages = Page::where('id',$id)->firstOrfail();
        $media = Media::where('type','other')->orderBy('id','DESC')->paginate(15);
        $media_images = Media::orderBy('id','DESC')->get();
        return view('admin.page.edit')->with([
            'pages' => $pages,
            'media' => $media,
            'media_images' => $media_images
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $request->validate([
            'title' => 'required | unique:pages,title,'.$id,
            'content'=> 'required',
        ]);
        $pages = Page::where('id',$id)->firstOrfail();
        $pages->title = $request->get('title');
        $pages->content = $request->get('content');
        $pages->cover =$request->get('cover');
        $pages->save();
        return redirect()->route('admin.pages')->with('success', 'Страница '.'<strong>'.$request->get('title').'</strong>'.' успешно обновлено');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $pages = Page::where('id',$id)->firstOrfail();
        $title = $pages->title;
        if(is_file(storage_path().'/app/public/'.$pages->cover))
            unlink(storage_path().'/app/public/'.$pages->cover);
        $pages->delete();
        return redirect()->route('admin.pages')->with([
            'pages' =>$pages
        ])->with('success', 'Страница '.'<strong>'.$title.'</strong>'.' успешно удалено');
    }
}
