<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Category;
use App\CollectionProducts;
use App\Discount;
use App\Media;
use App\OrderProducts;
use App\PopularProducts;
use App\ProductCategory;
use App\ProductDetails;
use App\ProductImages;
use App\Products;
use App\Review;
use App\Wish;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;


class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $products = Products::orderBy('id','DESC')->with('discount','category','productDetail')->paginate(10);
        $categories = Category::all();
        return view('admin.products.index')->with([
            'products' => $products,
            'categories' => $categories,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $parentId = null;
        $categories = Category::orderBy('order')->get();
        $htmlStructure = "";
        $productParents = [];
        $this->category($categories, $parentId,$htmlStructure,$productParents);

        $discounts = Discount::all();
        $media = Media::where('type','product')->orderBy('id','DESC')->paginate(15);
        $lists = ProductDetails::getColor();

        return view('admin.products.create')->with([
            'html' => $htmlStructure,
            'discounts' => $discounts,
            'media'  => $media,
            'lists'  => $lists,
        ]);

    }

    public function category($categories, $parent_id, &$html,$productParents)
    {
        $a = Str::random(4);
        $html .= '<ol class="dd-list">';
//
        foreach ($categories as $key => $row) {
            if ($row->parent_id == $parent_id) {
                $html .= '<li class="dd-item">' . '<div class="dd-handle">' . $row->title . '</div>';

                $thisCategory = Category::where('parent_id', $row->id)->get();

                if(!empty($thisCategory[0]->title)){
                    $html .= $this->category($categories, $row->id, $html,$productParents);
                }
                else{
                    if (in_array($row->id, $productParents)){
                        $html .= '<input class="myCheck checkbox" type="checkbox" name="category[]" data-title="' . $row->title . '" value="' . $row->id . '"> Choose<br>';
                    }else {
                        $html .= '<input class="checkbox" type="checkbox" name="category[]" data-title="' . $row->title . '" value="' . $row->id . '"> Choose<br>';
                    }
                }

                $html .= '</li>';
            }
        }

        $html .= '</ol>';
    }



    function fetch_data(Request $request)
    {
        if($request->ajax())
        {
            $media = Media::where('type','product')->orderBy('id','DESC')->paginate(15);
            $images = explode(',',$request->images);
            if (count($images) ==0 || $request->images == "undefined"){
                $images = false;

            }
            return view('admin.products.pagination_data', compact('media','images'))->render();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $avilable = $request->sList;
        $avialable_array = explode(',',$avilable);
        if (empty($avialable_array)){
            $avialable_array = array();
            array_push($avialable_array,$request->sList);
        }
        if (isset($request->status)){
            $status = true;
        }else{
            $status = false;
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'category' => 'required',
//            'caption' => 'required',
            'size.*' => 'required',
            'quantity.*' => 'required | numeric | min:0 | max:999999',
            'price.*' => 'required | numeric | between:0,999999.99',
            'color.*' => 'required',
            'cover_image.*' => 'required | string',
        ]);
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        $categories = [];
        $length = count($request->price);
        $data = new Products();
        $data->name = $request->name;
//        $data->caption =$request->caption;
        $data->description = $request->description;
        $data->status =$status;
        $saved = $data->save();
        if($saved){
            foreach ($request->category as $category){
                array_push($categories,array('product_id' => $data->id,'category_id' => $category));
            }
            $createProductCategories = ProductCategory::insert($categories);
            if ($createProductCategories) {
                for ($i = 0; $i < $length; $i++) {

                    $data_detail = new ProductDetails();
                    $data_detail->product_id = $data->id;
                    $data_detail->size = $request->size[$i];
                    $data_detail->discount_id = $request->discount[$i];
                    $data_detail->color = $request->color[$i];
                    $data_detail->quantity = $request->quantity[$i];
                    $data_detail->available = $avialable_array[$i];
                    $data_detail->other_details = $request->other_details[$i];
                    $data_detail->price = $request->price[$i];
                    $save_detail = $data_detail->save();
                    if($save_detail){
                        $images_array = [];
                        $product_images = [];
                        $images_array = explode(',',$request->cover_image[$i]);
                        foreach ($images_array as $item) {
                            array_push($product_images, array(
                                'product_detail_id' => $data_detail->id,
                                'image' => $item
                            ));
                        }
                        $createProductImages = ProductImages::insert($product_images);
                    }
                }

                if ($createProductImages){
                    return response()->json(['created' => true]);
                }else{
                    ProductCategory::where('product_id',$data->id)->delete();
                    Products::where('id',$data->id)->delete();
                    return back();
                }
            }else{
                return back();
            }
        }else{
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $productParents = [];
        $productParentsTitle = [];
        $item = Products::where('id',$id)->with('category','discount','productDetail')->firstOrfail();
        foreach($item->category as $value){
            array_push($productParents,$value->category_id);
            array_push($productParentsTitle,$value->category->title);
        }
        $parentId = null;
        $categories = Category::orderBy('order')->get();
        $htmlStructure = "";
        $this->category($categories, $parentId,$htmlStructure,$productParents);
        $productDetails = ProductDetails::where('product_id',$id)->with('productImages')->get();
        $discounts = Discount::all();
        $media = Media::where('type','product')->orderBy('id','DESC')->paginate(15);
        $media_images = Media::orderBy('id','DESC')->get();
        $lists = ProductDetails::getColor();
        return view('admin.products.edit')->with([
            'item' => $item,
            'discounts' => $discounts,
            'media' => $media,
            'productDetails' => $productDetails,
            'media_images' => $media_images,
            'lists'  => $lists,
            'html' => $htmlStructure,
            'productParentsTitle' => $productParentsTitle,
            'productParents' => $productParents
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $product = Products::where('id',$id)->firstOrfail();

        $avilable = $request->sList;
        $avialable_array = explode(',',$avilable);
        if (empty($avialable_array)){
            $avialable_array = array();
            array_push($avialable_array,$request->sList);
        }
        if (isset($request->status)){
            $status = true;
        }else{
            $status = false;
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'category' => 'required',
//            'caption' => 'required',
            'size.*' => 'required',
            'quantity.*' => 'required | numeric | min:0 | max:999999',
            'price.*' => 'required | numeric | between:0,999999.99',
            'color.*' => 'required',
            'cover_image.*' => 'required | string',
            'new_size.*' => 'required',
            'new_quantity.*' => 'required | numeric | min:0 | max:999999',
            'new_price.*' => 'required | numeric | between:0,999999.99',
            'new_color.*' => 'required',
            'new_cover_image.*' => 'required | string',
        ]);
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        if(isset($request->size) || isset($request->new_size)) {
            $categories = [];
            $productDetails = [];
            if (!empty($request->new_price)){
                $length = count($request->new_price);
            }
            if ($product) {
                $updateProduct = Products::where('id', $id)->update([
                    'name' => $request->name,
//                    'caption' => $request->caption,
                    'description' => $request->description,
                    'status' => $status
                ]);
                if ($updateProduct) {
                    $productCategory = ProductCategory::where('product_id', $id)->get();
                    if ($productCategory) {
                        ProductCategory::where('product_id', $id)->delete();
                        foreach ($request->category as $category) {
                            array_push($categories, array('product_id' => $id, 'category_id' => $category));
                        }
                        $createProductCategories = ProductCategory::insert($categories);
                    }else{
                        return back();
                    }
                    if ($createProductCategories) {
                        $issetedProductDetails = ProductDetails::where('product_id',$id)->select('id')->get()->toArray();
                        $issetedProductDetails_array = [];
                        foreach ($issetedProductDetails as $issetedProductDetail){
                            array_push( $issetedProductDetails_array,$issetedProductDetail['id']);
                        }
                        $product_detail_id = $request->product_detail_id;
                        foreach ($product_detail_id as $detail_id){
                            if (in_array($detail_id,$issetedProductDetails_array)){
                                $updatedProductDetail = ProductDetails::find($detail_id);
                                $updatedProductDetail->size = $request->size[$detail_id];
                                $updatedProductDetail->color = $request->color[$detail_id];
                                $updatedProductDetail->quantity = $request->quantity[$detail_id];
                                $updatedProductDetail->available = $request->hidden_available[$detail_id];
                                $updatedProductDetail->other_details = $request->other_details[$detail_id];
                                $updatedProductDetail->price = $request->price[$detail_id];
                                $updatedProductDetail->discount_id = $request->discount[$detail_id];
                                $updatedProductDetail->save();
                                if (($key = array_search($detail_id, $issetedProductDetails_array)) !== false) {
                                    unset($issetedProductDetails_array[$key]);
                                }
                                $deleteProductImages = ProductImages::where('product_detail_id',$detail_id)->get()->toArray();
                                if (!empty($deleteProductImages)){
                                    ProductImages::where('product_detail_id',$detail_id)->delete();
                                }
                                $images_array = [];
                                $product_images = [];
                                $images_array = explode(',',$request->cover_image[$detail_id]);
                                foreach ($images_array as $item) {
                                    array_push($product_images, array(
                                        'product_detail_id' => $detail_id,
                                        'image' => $item
                                    ));
                                }
                                $createProductImages = ProductImages::insert($product_images);
                            }else{
                                for ($i = 0; $i < $length; $i++) {
                                    $data_detail = new ProductDetails();
                                    $data_detail->product_id = $id;
                                    $data_detail->size = $request->new_size[$i];
                                    $data_detail->color = $request->new_color[$i];
                                    $data_detail->quantity = $request->new_quantity[$i];
                                    $data_detail->available = $request->new_hidden_available[$i];
                                    $data_detail->other_details = $request->new_other_details[$i];
                                    $data_detail->price = $request->new_price[$i];
                                    $data_detail->discount_id = $request->new_discount[$i];
                                    $save_detail = $data_detail->save();
                                    if($save_detail){
                                        $images_array = [];
                                        $product_images = [];
                                        $images_array = explode(',',$request->new_cover_image[$i]);
                                        foreach ($images_array as $item) {
                                            array_push($product_images, array(
                                                'product_detail_id' => $data_detail->id,
                                                'image' => $item
                                            ));
                                        }
                                        $createProductImages = ProductImages::insert($product_images);
                                    }
                                }
                            }
                        }
                        foreach($issetedProductDetails_array as $issetedProductDetail){
                            $deleteProductImages_in_array = ProductImages::where('product_detail_id',$issetedProductDetail)->get()->toArray();
                            $deleteCollectionProducts = CollectionProducts::where('product_detail_id',$issetedProductDetail)->get()->toArray();
                            $deletePopularProducts = PopularProducts::where('product_detail_id',$issetedProductDetail)->get()->toArray();
                            $deleteOrderProducts = OrderProducts::where('product_details_id',$issetedProductDetail)->get()->toArray();
                            $deleteWhishesProducts = Wish::where('product_id',$issetedProductDetail)->get()->toArray();
                            //
                            $deleteProductDetails = ProductDetails::where('id', $issetedProductDetail)->get()->toArray();
                            $deleteProductDetail_in_cart = Cart::where('product_detail_id',$issetedProductDetail)->get()->toArray();



                            if (!empty($deleteProductImages_in_array)) {
                                ProductImages::where('product_detail_id', $issetedProductDetail)->delete();
                            }
                            if (!empty($deleteCollectionProducts)){
                                CollectionProducts::where('product_detail_id',$issetedProductDetail)->delete();
                            }
                            if (!empty($deletePopularProducts)) {
                                PopularProducts::where('product_detail_id',$issetedProductDetail)->delete();
                            }
                            if (!empty($deleteOrderProducts)) {
                                OrderProducts::where('product_details_id',$issetedProductDetail)->delete();
                            }
                            if (!empty($deleteWhishesProducts)) {
                                Wish::where('product_id',$issetedProductDetail)->delete();
                            }

                            if (!empty($deleteProductDetail_in_cart)) {
                                Cart::where('product_detail_id',$issetedProductDetail)->delete();
                            }
                            if (!empty($deleteProductDetails)) {
                                ProductDetails::where('id', $issetedProductDetail)->delete();
                            }
                        }
                        return response()->json(['created' => true]);

                    }else{
                        return back();
                    }
                } else {
                    return back();
                }
            } else {
                return back();
            }
        }else{
            $msg = ['Свойства продукта обязательно'];
            return response()->json(['errors'=>$msg]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $product = Products::where('id',$id)->firstOrfail();
        if ($product){
            $categoryProduct = ProductCategory::where('product_id',$product->id)->get()->toArray();
            $productDetails = ProductDetails::where('product_id',$product->id)->get();
            if (!empty($categoryProduct)){
                ProductCategory::where('product_id',$product->id)->delete();
            }
            if ($productDetails->count()){
                foreach ($productDetails as $productDetail){
                    $deleteProductImages_in_array = ProductImages::where('product_detail_id',$productDetail->id)->get()->toArray();
                    $deleteCollectionProducts = CollectionProducts::where('product_detail_id',$productDetail->id)->get()->toArray();
                    $deletePopularProducts = PopularProducts::where('product_detail_id',$productDetail->id)->get()->toArray();
                    $deleteOrderProducts = OrderProducts::where('product_details_id',$productDetail->id)->get()->toArray();
                    $deleteWhishesProducts = Wish::where('product_id',$productDetail->id)->get()->toArray();
                    //
                    $deleteProductDetails = ProductDetails::where('id', $productDetail->id)->get()->toArray();
                    $deleteProductDetail_in_cart = Cart::where('product_detail_id',$productDetail->id)->get()->toArray();



                    if (!empty($deleteProductImages_in_array)) {
                        ProductImages::where('product_detail_id', $productDetail->id)->delete();
                    }
                    if (!empty($deleteCollectionProducts)){
                        CollectionProducts::where('product_detail_id',$productDetail->id)->delete();
                    }
                    if (!empty($deletePopularProducts)) {
                        PopularProducts::where('product_detail_id',$productDetail->id)->delete();
                    }
                    if (!empty($deleteOrderProducts)) {
                        OrderProducts::where('product_details_id',$productDetail->id)->delete();
                    }
                    if (!empty($deleteWhishesProducts)) {
                        Wish::where('product_id',$productDetail->id)->delete();
                    }
                    if (!empty($deleteProductDetail_in_cart)) {
                        Cart::where('product_detail_id',$productDetail->id)->delete();
                    }
                    if (!empty($deleteProductDetails)) {
                        ProductDetails::where('id', $productDetail->id)->delete();
                    }

                }
            }
            $deleteReviewsProducts = Review::where('product_id',$product->id)->get()->toArray();
            if (!empty($deleteReviewsProducts)) {
                Review::where('product_id',$product->id)->delete();
            }
            $msg = 'Продукт '.'<strong>'.$product->name.'</strong>'.' успешно удалено';
            $product->delete();

        }
        return redirect()->route('admin.products')->with('success', $msg);
    }
}
