<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class
ProductCategory extends Model
{
    public function category(){
        return $this->hasOne(Category::class,'id','category_id');
    }

    public function products(){
        return $this->hasMany(Products::class,'id','product_id')->where('status',true);
    }
}
