<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiscountCondition extends Model
{
    //


    public static function getList(){
        return $list = [
            0 => 'цена',
            1 => 'размер',
        ];
    }

    public static function getType(){
        return $types = [
            0 => '=',
            1 => '>',
            2 => '>=',
            3 => '<',
            4 => '<=',
        ];
    }

    public function discount(){
        return $this->hasOne(Discount::class,'id','discount_id');
    }
}
