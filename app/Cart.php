<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    //

    public function user(){

        return $this->hasOne(User::class,'id','user_id');

    }

    public function productDetail(){

        return $this->hasOne(ProductDetails::class,'id','product_detail_id');

    }

}
