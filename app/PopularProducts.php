<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PopularProducts extends Model
{
    //

    public function productDetail(){
        return $this->hasOne(ProductDetails::class,'id','product_detail_id');
    }
}
