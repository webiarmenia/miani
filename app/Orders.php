<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    //

    public function user(){
        return $this->hasOne(User::class,'id','client_id');
    }

    public function orderProducts(){
        return $this->hasMany(OrderProducts::class,'order_id','id');
    }

    public function productDetail()
    {
        return $this->hasOne(OrderProducts::class,'product_details_id','id');
    }

}
