<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    //


    public function product(){
        return $this->hasMany(Products::class);
    }

    public function discountConditions(){
        return $this->hasMany(DiscountCondition::class);
    }

    public function category(){
        return $this->hasMany(Category::class);
    }
}
