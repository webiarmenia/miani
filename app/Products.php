<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    //

    public function productDetail(){
        return $this->hasMany(ProductDetails::class,'product_id','id');
    }

    public function discount(){
        return $this->hasOne(Discount::class,'id','discount_id');
    }

    public function reviews(){
        return $this->hasMany(Review::class,'product_id','id');
    }

    public function category(){
        return $this->hasMany(ProductCategory::class,'product_id','id');
    }

}
