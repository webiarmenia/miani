<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model{
    protected $fillable = [
        'title',
        'content',
        'cover'
    ];

    public function media(){
        return $this->hasOne(Media::class,'id','cover');
    }
}