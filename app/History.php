<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    //
    protected $table = 'history';

    protected $fillable = [
        'user_id','order_id'
    ];

    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }
    public function order(){
        return $this->hasOne(Orders::class,'id','order_id');
    }

}
