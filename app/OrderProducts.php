<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderProducts extends Model
{
    //

    public function order(){
        return $this->hasOne(Orders::class,'id','order_id');
    }
    public function productDetail(){
        return $this->hasOne(ProductDetails::class,'id','product_details_id');
    }
}
