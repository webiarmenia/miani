<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Collections extends Model
{
    //

    public function discounts()
    {
        return $this->hasOne(Discount::class,'id','discount');
    }

    public function collectionProducts(){
        return $this->hasMany(CollectionProducts::class,'id','collection_id');
    }
}
