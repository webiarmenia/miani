<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model{
    protected $fillable = [
        'path',
        'uniq_name',
        'ext',
        'type',
        'original_name'
    ];
}
