<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wish extends Model{
    protected $fillable = [
        'product_id',
        'user_id'
    ];

    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }
    public function productDetail(){
        return $this->hasOne(ProductDetails::class,'id','product_id');
    }
    public function product(){
        return $this->hasOne(Products::class,'id','product_id');
    }
}
