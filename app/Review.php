<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model{
    protected $fillable = [
        'user_id'
    ];

    public function products(){
        return $this->hasOne(Products::class,'id','product_id');
    }
    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }
}
