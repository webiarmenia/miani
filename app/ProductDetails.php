<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductDetails extends Model
{
    //

    public static function getColor(){
        return $list = [
            0 => 'Белый',
            1 => 'Красный',
            2 => 'Оранжевый',
            3 => 'Желтый',
            4 => 'Зеленый',
            5 => 'Циан',
            6 => 'Синий',
            7 => 'Пурпурный',
            8 => 'Розовый',
            9 => 'Черный',
        ];
    }

    public function product(){
        return $this->hasOne(Products::class,'id','product_id');
    }

    public function products(){
        return $this->hasOne(Products::class,'id','product_id')->where('status',true);
    }

    public function productImages(){
        return $this->hasMany(ProductImages::class,'product_detail_id','id');
    }
    public function orderProducts(){
        return $this->hasOne(OrderProducts::class,'product_details_id','id');
    }


}
