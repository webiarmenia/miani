@extends('layouts.app')
@section('content')
<div class="container" id="login-block">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4">
            <div class="login-box clearfix animated flipInY">
                <div class="page-icon animated bounceInDown">
                    <img src="{{ asset('img/user-icon.png') }}" alt="Key icon">
                </div>
                <div class="login-logo">
                  <h1 style="color:#fff;">MIANI</h1>
                </div>
                <hr>
                <div class="login-form">
                    <div class="alert alert-danger hide">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <h4>Error!</h4>
                        Your Error Message goes here
                    </div>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <input type="email"  name="email" placeholder="Email" class="input-field form-control user"  value="{{ old('email') }}"/>

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong style="color:#fff; font-size: 13px; margin-left: 12%; font-weight: 400; ">{{ $message }}</strong>
                            </span>
                        @enderror

                        <input type="password" placeholder="Password"  name="password" class="input-field form-control password" />

                        @error('password')
                            <span class="invalid-feedback " role="alert">
                                <strong style="color:#fff; font-size:13px;margin-left: 12%; font-weight: 400;">{{ $message }}</strong>
                            </span>
                        @enderror

                        <div class="form-group row mb-0">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-login ladda-button text-center" data-style="expand-left"   id="submit-form"> Login </button>
                            </div>
                            <div class="col-md-12 text-center">
                                <div class="login-links">
                                    @if (Route::has('password.request'))
                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                            Forgot password?
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection