<div>Добро пожаловать уважаемый <strong>{{$name}} {{$surname}}</strong> ,
    <br>
    для окончательной регистрации нажмите <strong>Подтвердить</strong></div>


<div>
    <a href="{{env('APP_URL')}}/confirm-password/{{ $email }}/{{ $confirmed }}"
                   style="background-color:#06cd04;border:1px solid #bfbfbb;
                   border-radius:4px;color:#ffffff;display:inline-block;
                   font-family:sans-serif;font-size:13px;font-weight:bold;
                   line-height:30px;text-align:center;text-decoration:none;
                   width:100px;-webkit-text-size-adjust:none;mso-hide:all;">
        Подтвердить </a>
</div>