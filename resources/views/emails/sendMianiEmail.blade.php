<p>Индентификатор - {{ $order->id }}</p>
<p>Почта клиента   - {{ $order->client_email }}</p>
<p>Итоговая цена  - {{ $order->total_price }}</p>
<p>Одобренно     -
    @if($order->approved == false)
        нет
    @else
        да
    @endif
</p>
<p>Статус         -
    @if($order->status == 0)
        Ожидание
    @elseif($order->status == 1)
        В процессе
    @else
        Готово
    @endif
</p>
