<!DOCTYPE html>
<html class="no-js sidebar-large lt-ie9 lt-ie8 lt-ie7">
<html class="no-js sidebar-large lt-ie9 lt-ie8">
<html class="no-js sidebar-large lt-ie9">
<html class="no-js sidebar-large">
<head>
    <meta charset="utf-8">
    @yield('title')
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="" name="description"/>
    <meta content="themes-lab" name="author"/>

    <link href="{{ asset('css/icons/icons.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('css/plugins.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('css/color-dark.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('css/component.css') }}" rel="stylesheet" media="all">
    @yield('css')

</head>

<body data-page="blank_page">
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#sidebar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a id="menu-medium" class="sidebar-toggle tooltips">
                <i class="fa fa-outdent"></i>
            </a>

        </div>
        <div class="navbar-collapse collapse">
            <!-- BEGIN TOP NAVIGATION MENU -->
            <ul class="nav navbar-nav pull-right header-menu">
                <!-- BEGIN NOTIFICATION DROPDOWN -->
                <li class="dropdown" id="notifications-header">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                       data-close-others="true">
                        <i class="glyph-icon flaticon-notifications"></i>
                        <span class="badge badge-danger badge-header">{{ $quantity }}</span>
                    </a>
                    <ul class="dropdown-menu" style="margin-top: 7px !important;">
                        <li class="dropdown-header clearfix">
                            <p class="pull-left">Уведомления</p>
                        </li>
                        <li>
                            @if($orders_no_read->count()>5)
                                <ul class="dropdown-menu-list withScroll" data-height="220">
                            @else
                                <ul class="dropdown-menu-list withScroll" data-height="auto">
                            @endif
                                @if($orders_no_read->count())
                                    @foreach($orders_no_read as $item_no_read)
                                        <a href="{{url('admin_v128_11/orders/view',$item_no_read->id)}}" style="text-decoration: none; font-size: 13px">
                                            <li>
                                                {{$item_no_read->client_email}} - {{$item_no_read->total_price.'₽'}}
                                            </li>
                                        </a>
                                    @endforeach
                                @endif
                            </ul>
                        </li>
                    </ul>
                </li>
                        <li class="nav-item dropdown nav-user">
                    <form action="{{ route('logout') }}" method="POST" style="width: 100%;">
                        @csrf
                        <div class="collapse navbar-collapse " id="navbarSupportedContent">
                            <ul class="navbar-nav ml-auto navbar-right-top">
                                <li class="nav-item dropdown nav-user" style="margin-top: 7px;">
                                    <a class="nav-link nav-user-img" href="" id="navbarDropdownMenuLink2"
                                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="glyph-icon flaticon-account"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right nav-user-dropdown"
                                         aria-labelledby="navbarDropdownMenuLink2">
                                        <button type="submit" class="dropdown-item">
                                            <i class="fa fa-power-off mr-2" style="margin-right: 7px;"></i> Logout
                                        </button>
                                    </div>

                                </li>
                            </ul>
                        </div>
                    </form>
                </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div id="wrapper">
    <nav id="sidebar">
        <div id="main-menu">
            <ul class="sidebar-nav">
                <li class="active-orders-page">
                    <a href="{{route('admin.orders')}}"><i class="fa fa-credit-card"></i><span
                                class="sidebar-text">Заказы</span></a>
                </li>
                <li class="active-users-page">
                    <a href="{{ route('admin.users') }}"><i class="glyph-icon flaticon-account"></i><span
                                class="sidebar-text">Пользователи</span></a>
                </li>
                <li class="active-products-page">
                    <a href="{{route('admin.products')}}"><i class="glyph-icon flaticon-shopping80"></i><span
                                class="sidebar-text">Продукты</span></a>
                </li>
                <li class="active-collections-page">
                    <a href="{{route('admin.collections')}}"><i class="glyph-icon flaticon-panels"></i><span class="sidebar-text">Коллекции</span></a>
                </li>
                <li class="active-discounts-page">
                    <a href="{{route('admin.discounts')}}"><i class="fa fa-money"></i><span
                                class="sidebar-text">Скидки</span></a>
                </li>
                <li class="active-categories-page">
                    <a href="{{route('admin.categories')}}"><i class="fa fa-list-ul"></i><span class="sidebar-text">Категории</span></a>
                </li>
                <li class="active-sliders-page">
                    <a href="{{route('admin.sliders')}}"><i class="fa fa-list-alt"></i><span class="sidebar-text">Слайдеры</span></a>
                </li>
                <li class="active-menus-page">
                    <a href="{{route('admin.menus')}}"><i class="fa fa-th-list"></i><span
                                class="sidebar-text">Меню</span></a>
                </li>
                <li class="active-pages-page">
                    <a href="{{ route('admin.pages') }}"><i class="glyph-icon flaticon-pages"></i><span
                                class="sidebar-text">Страницы</span></a>
                </li>
                <li class="active-faqs-page">
                    <a href="{{route('admin.faqs')}}"><i class="fa fa-info"></i><span
                                class="sidebar-text">FAQ</span></a>
                </li>
                <li class="active-media-page">
                    <a href="{{ route('admin.media') }}"><i class="fa fa-picture-o"></i><span class="sidebar-text">Картинки</span></a>
                </li>
                <li class="active-subscribers-page">
                    <a href="{{ route('admin.subscribers') }}"><i class="glyph-icon flaticon-account"></i><span
                                class="sidebar-text">Подписчики</span></a>
                </li>
                <li class="active-settings-page">
                    <a href="{{route('admin.settings')}}"><i class="fa fa-gears"></i><span class="sidebar-text">Настройки</span></a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="md-modal md-effect-1" id="modal-1">
        <div class="md-content">
            <h3>Your title</h3>
            <div>
                <p>Content</p>
                <button class="btn btn-modal btn-default">Close me!</button>
            </div>
        </div>
    </div>
    <div id="main-content">
        @yield('content')
    </div>
</div>
<script src="{{asset('js/jquery-1.11.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jquery-migrate-1.2.1.js')}}" type="text/javascript"></script>
{{--<script src="{{asset('js/jquery-ui/jquery-ui-1.10.4.min.js')}}" type="text/javascript"></script>--}}
{{--<script src="{{asset('js/plugins/jquery-mobile/jquery.mobile-1.4.2.js')}}"></script>--}}
<script src="{{asset('js/modernizr-2.6.2-respond-1.1.0.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/bootstrap-hover-dropdown.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/bootstrap-select.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jquery.mCustomScrollbar.concat.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jquery.mmenu.min.all.js')}}" type="text/javascript"></script>
<script src="{{asset('js/nprogress.js')}}" type="text/javascript"></script>
<script src="{{asset('js/sparkline.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/breakpoints.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jquery-numerator.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jquery.cookie.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/bootstrap-datepicker.js')}}" type="text/javascript"></script>
<script src="{{asset('js/classie.js')}}" type="text/javascript"></script>
<script src="{{asset('js/modalEffects.js')}}" type="text/javascript"></script>
<script src="{{asset('js/application.js')}}" type="text/javascript"></script>
<script>
    $(document).ready(function(){
        $active_page = $(".wi-page-title").attr("id");
        $("."+$active_page).css('background','#eaeaea');
    });
</script>
@yield('javascript')

</body>
</html>
