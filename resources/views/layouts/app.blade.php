<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <title>Miani</title>
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('css/icons/icons.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('css/plugins.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('css/animate-custom.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('css/lada.min.css') }}" rel="stylesheet" media="all">
</head>
<body>
    <main class="py-4">
        @yield('content')
    </main>
    <script src="{{asset('js/modernizr-2.6.2-respond-1.1.0.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/jquery-1.11.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/jquery-migrate-1.2.1.js')}}" type="text/javascript"></script>
{{--    <script src="{{asset('js/plugins/jquery-mobile/jquery.mobile-1.4.2.js')}}"></script>--}}
    <script src="{{asset('js/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/jquery.cookie.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/jquery-ui-1.11.2.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/backstretch.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/lada.min.js')}}" type="text/javascript"></script>
</body>
</html>