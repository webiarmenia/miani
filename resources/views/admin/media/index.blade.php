@extends('layouts.appmiani')
@section('title')
<title>Картинки</title>
@endsection
@section('content')
    <div class="m-b-20 clearfix">
        <div class="page-title pull-left">
            <h3 class="pull-left"><strong class="wi-page-title" id="active-media-page">Картинки</strong></h3>
        </div>
        <div class="pull-right">
            <a href="{{route('admin.media.create')}}" class="btn btn-success m-t-10"><i class="fa fa-plus p-r-10"></i> Дабавить Картинку </a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if (\Session::has('success'))
                <div class="alert alert-success fade in" style="width: 100%">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! \Session::get('success') !!}
                </div>
            @endif
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
                            <table id="products-table" class="table table-tools table-hover">
                                <thead class="no-bd">
                                <tr>
                                    <th class="text-center"><strong>№</strong></th>
                                    <th class="text-center"><strong>Картинка</strong></th>
                                    <th class="text-center"><strong>Тип</strong></th>
                                    <th class="text-center"><strong>оригинальное имя</strong></th>
                                    <th class="text-center"><strong>расширение</strong></th>
                                    <th class="text-center"><strong>Действия</strong></th>
                                </tr>
                                </thead>
                                <tbody class="no-bd-y">
                                @foreach($media as $key =>$item)
                                    <tr>
                                        <td  class="text-center">{{$key+1}}</td>
                                        <td class="text-center">
                                            <img src="{{ asset( '/storage/'.$item->path. $item->uniq_name.'_'.'small'.'.' .$item->ext) }}" width="50" height="50">
                                        </td>
                                        <td class="text-center">{{$item->type}}</td>
                                        <td class="text-center">{{$item->original_name}}</td>
                                        <td class="text-center">{{$item->ext}}</td>
                                        <td class="text-center">
                                            <a href="{{ route('admin.media.edit',$item->id)}}" class="edit btn btn-sm btn-default"><i class="fa fa-pencil"></i>Редактировать</a>
                                            <form action="{{route('admin.media.destroy',$item->id)}}" method="POST" style="display: inline-grid;">
                                                @csrf
                                                <button type="submit" class="delete btn btn-sm btn-default"><i class="fa fa-times-circle"></i> Удалить </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            {{ $media->links() }}
        </div>
    </div>
@endsection
