@extends('layouts.appmiani')
@section('title')
    <title>Редактирование Картинки</title>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong class="wi-page-title" id="active-media-page">Редактирование картинка</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <form method="post" action="{{ route('admin.media.update', $media->id) }}" id="form1" class="form-horizontal" data-parsley-validate enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Тип<span class="asterisk">*</span></label>
                                    <div class="col-sm-9">
                                        <select class="form-control" data-style="input-sm btn-default" name="type">
                                            <option value="category" {{ $media->type == 'category' ? 'selected': '' }}>category</option>
                                            <option value="product"  {{ $media->type == 'product' ?  'selected': '' }}>product</option>
                                            <option value="other"    {{ $media->type == 'other' ?    'selected': '' }}>other</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Картинка <span class="asterisk">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="file"  name="cover" id="cover" value="{{ $media->cover}}"><br/>
                                        <img src="{{ asset( '/storage/'.$media->path. $media->uniq_name.'_'.'small'.'.' .$media->ext) }}" width="200" height="200">
                                    </div>
                                </div>
                                <div class="col-sm-9 col-sm-offset-3">
                                    <div class="pull-right">
                                        <a href="{{route('admin.media')}}" class="btn btn-default m-r-10 m-t-10">
                                            <i class="fa fa-reply"></i> Назад
                                        </a>
                                        <button id="submit" type="submit" class="btn btn-success m-t-10"><i class="fa fa-check"></i> Сохранить
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
