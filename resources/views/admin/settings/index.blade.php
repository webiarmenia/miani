@extends('layouts.appmiani')
@section('title')
    <title>Настройки</title>
@endsection
@section('content')
    <div class="m-b-20 clearfix">
        <div class="page-title pull-left">
            <h3 class="pull-left"><strong class="wi-page-title" id="active-settings-page">Настройки</strong></h3>
        </div>
        <div class="pull-right">
            <a href="{{route('admin.settings.create')}}" class="btn btn-success m-t-10"><i class="fa fa-plus p-r-10"></i> Добавить настройку</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if (\Session::has('successCreate'))
                <div class="alert alert-success fade in" style="width: 100%">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! \Session::get('successCreate') !!}
                </div>
            @endif
            @if (\Session::has('successUpdate'))
                <div class="alert alert-success fade in" style="width: 100%">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! \Session::get('successUpdate') !!}
                </div>
            @endif
            @if (\Session::has('successDelete'))
                <div class="alert alert-success fade in" style="width: 100%">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! \Session::get('successDelete') !!}
                </div>
            @endif
            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="no-bd">
                                <tr>
                                    <th class="text-center"><strong>Индентификатор</strong></th>
                                    <th class="text-center"><strong>Ключ значение</strong></th>
                                    <th class="text-center"><strong>Значение</strong></th>
                                    <th class="text-center"><strong>Действия</strong></th>
                                </tr>
                                </thead>
                                <tbody class="no-bd-y">
                                @if($settings->count())
                                    @foreach($settings as $setting)
                                        <tr>
                                            <td class="text-center">{{$setting->id}}</td>
                                            <td class="text-center">{{$setting->key}}</td>
                                            <td class="text-center">{{$setting->value}}</td>
                                            <td class="text-center">
                                                <a href="{{route('admin.settings.edit',$setting->id)}}" class="edit btn btn-sm btn-default" style="margin-top: 5px"><i class="fa fa-pencil"></i> Редактировать</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            {{ $settings->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection