@extends('layouts.appmiani')
@section('title')
    <title>Новая настройкa</title>
@endsection
@section('css')
    <link href="{{asset('plugins/datetimepicker/jquery.datetimepicker.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/pickadate/themes/default.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/pickadate/themes/default.date.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/pickadate/themes/default.time.css')}}" rel="stylesheet">
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong class="wi-page-title" id="active-settings-page">Новое настройкa</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <form id="form1" class="form-horizontal" action="{{route('admin.settings.store')}}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Ключ значение <span class="asterisk">*</span>
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control @error('key') parsley-error @enderror" name="key" value="{{ old('key') }}">
                                        <ul class="parsley-errors-list" id="parsley-id-1143">
                                            @error('key')
                                            <li>{{ $message }}</li>
                                            @enderror
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Значение
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="value" value="{{ old('value') }}">
                                    </div>
                                </div>
                                <div class="col-sm-9 col-sm-offset-3">
                                    <div class="pull-right">
                                        <button id="submit" type="submit" class="btn btn-success m-t-10" value="add" name="button">
                                            <i class="fa fa-check"></i> Дабавить
                                        </button>
                                        <a href="{{route('admin.settings')}}" class="btn btn-default m-r-10 m-t-10">
                                            <i class="fa fa-reply"></i> Назад
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


