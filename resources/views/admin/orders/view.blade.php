@extends('layouts.appmiani')
@section('title')
    <title> Заказ Клиента - {{$item->client_email}}</title>
@endsection
@section('content')
    <input type="hidden" id="refreshed" value="no">
    <div class="page-title">
        <div  class="form-group">
            <strong style="font-size: 20px;" class="wi-page-title" id="active-orders-page">Индентификатор Клиента</strong>
            <small style="font-size: 20px;padding-left: 20px;">
                @if($item->client_id == null)
                    null
                @else
                    {{$item->client_id}}
                @endif
            </small>
        </div>
        <div class="form-group">
            <strong style="font-size: 20px;">Почта Клиента</strong>  <small style="font-size: 20px; padding-left: 20px">{{$item->client_email}}</small>
        </div>
    </div>
    <div class="m-b-20 clearfix">
        <div class="page-title pull-left">
            <h1 class="pull-left"><strong>Продукты</strong></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
                            <table id="products-table" class="table table-tools table-hover">
                                <thead class="no-bd">
                                <tr>
                                    <th class="text-left"><strong>Идентификатор</strong></th>
                                    <th class="text-left"><strong>Название</strong></th>
                                    <th class="text-left"><strong>Размер</strong></th>
                                    <th class="text-left"><strong>Цвет</strong></th>
                                    <th class="text-left"><strong>Количество</strong></th>
                                    <th class="text-left"><strong>Цена</strong></th>
                                    <th class="text-left"><strong>Действия</strong></th>
                                </tr>
                                </thead>
                                <tbody class="no-bd-y">
                                @foreach($order_products->orderProducts as $key =>$order_product)
                                    <tr>
                                        <td class="text-left">{{'#'.$order_product['product_details_id']}}</td>
                                        <td class="text-left">{{$order_product['productDetail']['product']['name']}}</td>
                                        <td class="text-left">{{$order_product['productDetail']['size']}}</td>
                                        <td class="text-left">{{$color[$order_product['productDetail']['color']]}}</td>
                                        <td class="text-left">{{$order_product['quantity']}}</td>
                                        <td class="text-left">{{$order_product['productDetail']['price']}}</td>
                                        <td class="text-left">
                                            <a href="{{route('admin.products.edit',$order_product['productDetail']['product']['id'])}}" class="edit btn btn-sm btn-default"><i class="fa fa-eye"></i> Посмотреть</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="well">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <i class="fa fa-user f-80"></i>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="row align-right m-b-10">
                                                <div class="col-md-6">
                                                    Имя:
                                                </div>
                                                <div class="col-md-5 w-600">
                                                    {{ $user->name }}
                                                </div>
                                            </div>
                                            <div class="row align-right m-b-10">
                                                <div class="col-md-6">
                                                    Эл. адрес:
                                                </div>
                                                <div class="col-md-5 w-600">
                                                    {{ $user->email }}
                                                </div>
                                            </div>
                                            <div class="row align-right m-b-10">
                                                <div class="col-md-6">
                                                    Фамилия:
                                                </div>
                                                <div class="col-md-5 w-600">
                                                    {{ $user->surname }}
                                                </div>
                                            </div>
                                            <div class="row align-right m-b-10">
                                                <div class="col-md-6">
                                                    Адрес:
                                                </div>
                                                <div class="col-md-5 w-600">
                                                    {{ $user->address }}
                                                </div>
                                            </div>
                                            <div class="row align-right m-b-10">
                                                <div class="col-md-6">
                                                    Телефон:
                                                </div>
                                                <div class="col-md-5 w-600">
                                                    {{ $user->phone }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="well">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <i class="glyph-icon flaticon-shopping80 f-80"></i>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row align-right m-b-10">
                                                <div class="col-md-8">
                                                    Итоговая цена:
                                                </div>
                                                <div class="col-md-3 w-600">
                                                    {{$item->total_price}}
                                                </div>
                                            </div>
                                            <div class="row align-right m-b-10">
                                                <div class="col-md-8">
                                                    Oдобрено:
                                                </div>
                                                <div class="col-md-3 w-600">
                                                    @if($item->approved == false)
                                                        Нет
                                                    @else
                                                        Да
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="row align-right m-b-10">
                                                <div class="col-md-8">
                                                    Статус:
                                                </div>
                                                <div class="col-md-3 w-600">
                                                    @if($item->status == 0)
                                                        Ожидание
                                                    @elseif($item->status == 1)
                                                        В процессе
                                                    @else
                                                        Готово
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-9 col-sm-offset-3">
                        <div class="pull-right">
                            <a href="{{route('admin.orders')}}" class="btn btn-default m-r-10 m-t-10">
                                <i class="fa fa-reply"></i> Назад
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

