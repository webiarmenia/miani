@extends('layouts.appmiani')
@section('title')
    <title>Редактирование заказа</title>
@endsection
@section('css')
    <link href="{{asset('plugins/datetimepicker/jquery.datetimepicker.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/pickadate/themes/default.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/pickadate/themes/default.date.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/pickadate/themes/default.time.css')}}" rel="stylesheet">
@endsection
@section('content')
    <input type="hidden" id="refreshed" value="no">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong class="wi-page-title" id="active-orders-page">Редактирование заказа</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <form id="form1" class="form-horizontal"  action="{{route('admin.orders.update',$item->id)}}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Одобренно <span class="asterisk">*</span>
                                    </label>
                                    <div class="col-sm-9">
                                        <select name="approved" class="form-control">
                                            <option value="1" {{  $item->approved == 1 ? 'selected': '' }}>Да</option>
                                            <option value="0" {{  $item->approved == 0 ? 'selected': '' }}>Нет</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Статус <span class="asterisk">*</span>
                                    </label>
                                    <div class="col-sm-9">
                                        <select name="status" class="form-control">
                                            <option value="0" {{  $item->status == 0 ? 'selected': '' }}>Ожидание</option>
                                            <option value="1" {{  $item->status == 1 ? 'selected': '' }}>В процессе</option>
                                            <option value="2" {{  $item->status == 2 ? 'selected': '' }}>Готово</option>
                                        </select>
                                    </div>
                                </div>
{{--                                <div class="col-sm-9 col-sm-offset-3">--}}
{{--                                    <div class="pull-right">--}}
{{--                                        <button id="submit" type="submit" class="btn btn-success m-t-10">--}}
{{--                                            <i class="fa fa-check"></i>Сахранить--}}
{{--                                        </button>--}}
{{--                                        <a href="{{ redirect()->getUrlGenerator()->previous() }}" >--}}
{{--                                            <button class="btn btn-default m-r-10 m-t-10">--}}
{{--                                                <i class="fa fa-reply"></i> Назад</button>--}}
{{--                                        </a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                                <div class="col-sm-9 col-sm-offset-3">
                                    <div class="pull-right">
                                        <button id="submit" type="submit" class="btn btn-success m-t-10" name="button" value="update">
                                            <i class="fa fa-check"></i>Сахранить
                                        </button>
                                        <a href="{{route('admin.orders')}}" class="btn btn-default m-r-10 m-t-10">
                                            <i class="fa fa-reply"></i> Назад
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
