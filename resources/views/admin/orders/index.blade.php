@extends('layouts.appmiani')
@section('title')
    <title>Заказы</title>
@endsection
@section('css')
    <style>
        #yes{
            background-color: red !important;
            color: white;
        }
    </style>
@endsection
@section('content')
    <div class="m-b-20 clearfix">
        <div class="page-title pull-left">
            <h3 class="pull-left"><strong class="wi-page-title" id="active-orders-page">Заказы</strong></h3>
        </div>
        <form id="form1" class="form-horizontal" action="{{route('admin.orders.index')}}" method="POST">
            @csrf
            <div class="pull-right" style="margin-right: 15px;">
                <label class="col-sm-3 control-label">Статус</label>
                <select name="status" class="form-control">
                    <option value="" {{ $selected == 10 ? 'selected': '' }}>Все</option>
                    <option value="0" {{ $selected == 0 ? 'selected': '' }}>Ожидание</option>
                    <option value="1" {{ $selected == 1 ? 'selected': '' }}>В процессе</option>
                    <option value="2" {{ $selected == 2 ? 'selected': '' }}>Готово</option>
                </select>
            </div>
            <div class="col-sm-9 col-sm-offset-3">
                <div class="pull-right">
                    <button id="submit" type="submit" class="btn btn-success m-t-10" value="filter" name="button"><i class="fa fa-check"></i> Посмотреть</button>
                </div>
            </div>
        </form>
    </div>
    <div class="row" id="res">
        <input type="hidden" id="refreshed" value="no">
        <div class="col-md-12">
            @if (\Session::has('successUpdate'))
                <div class="alert alert-success fade in" style="width: 100%">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! \Session::get('successUpdate') !!}
                </div>
            @endif
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="no-bd">
                                <tr>
                                    <th class="text-center"><strong>№</strong></th>
                                    <th class="text-center"><strong>Клиент</strong></th>
                                    <th class="text-center"><strong>Почта Клиента</strong></th>
                                    <th class="text-center"><strong>Итоговая_цена</strong></th>
                                    <th class="text-center"><strong>Одобренно</strong></th>
                                    <th class="text-center"><strong>Статус</strong></th>
                                    <th class="text-center"><strong>Действия</strong></th>
                                </tr>
                                </thead>
                                <tbody class="no-bd-y">
                                @if($orders->count())
                                    @foreach($orders as $key =>$order)
                                            <tr id="{{ $order->id }}">
                                                <td class="text-center">
                                                    {{ $key + 1 }}
                                                </td>
                                                <td class="text-center">
                                                    @if($order->client_id == null)
                                                        Null
                                                    @else
                                                        {{$order->client_id}}
                                                    @endif
                                                </td>
                                                <td class="text-center">{{$order->client_email}}</td>
                                                <td class="text-center">{{$order->total_price}}</td>
                                                <td class="text-center">
                                                    @if($order->approved == false)
                                                        Нет
                                                    @else
                                                        Да
                                                    @endif
                                                </td>
                                                @if($order->read == 'no')
                                                <td class="text-center" id="yes">
                                                    @if($order->status == 0)
                                                        Ожидание
                                                    @elseif($order->status == 1)
                                                        В процессе
                                                    @else
                                                        Готово
                                                    @endif
                                                </td>
                                                @elseif($order->read == 'yes')
                                                    <td class="text-center" class="yes">
                                                        @if($order->status == 0)
                                                            Ожидание
                                                        @elseif($order->status == 1)
                                                            В процессе
                                                        @else
                                                            Готово
                                                        @endif
                                                    </td>
                                                @endif
                                                <td class="text-center">
                                                    <a href="{{url('admin_v128_11/orders/view',$order->id)}}" class="edit btn btn-sm btn-default" data-id="{{ $order->id }}" style="margin-top: 5px"><i class="fa fa-eye"></i> Посмотреть</a>
                                                    <a href="{{url('admin_v128_11/orders/edit',$order->id)}}" class="edit btn btn-sm btn-default" data-id="{{ $order->id }}" style="margin-top: 5px"><i class="fa fa-pencil"></i> Редактировать</a>
                                                </td>
                                            </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            {{ $orders->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

{{--@section('javascript')--}}
{{--    <script type = "text/javascript">--}}

{{--        function preventBack() {--}}
{{--            alert('a');--}}
{{--            window.history.forward();--}}
{{--            alert('10');--}}
{{--            // window.hisory.forward();--}}
{{--        }--}}
{{--        setTimeout("preventBack()", 0);--}}
{{--        // window.onunload = function ()--}}
{{--        // {--}}
{{--        //     null--}}
{{--        // }--}}
{{--    </script>--}}
{{--@endsection--}}