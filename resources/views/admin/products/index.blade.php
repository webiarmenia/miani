@extends('layouts.appmiani')
@section('title')
    <title>Продукты</title>
@endsection
@section('content')
    <div class="m-b-20 clearfix">
        <div class="page-title pull-left">
            <h3 class="pull-left"><strong class="wi-page-title" id="active-products-page">Продукты</strong></h3>
        </div>
        <div class="pull-right">
            <a href="{{route('admin.products.create')}}" class="btn btn-success m-t-10"><i class="fa fa-plus p-r-10"></i> Дабавить продукт</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if (\Session::has('success'))
                <div class="alert alert-success fade in" style="width: 100%">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! \Session::get('success') !!}
                </div>
            @endif
            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="no-bd">
                                <tr>
                                    <th class="text-center"><strong>№</strong></th>
                                    <th class="text-center"><strong>Название</strong></th>
                                    <th class="text-center"><strong>Категория </strong></th>
                                    <th class="text-center"><strong>Детали(#Идентификатор) </strong></th>
{{--                                    <th class="text-center"><strong>Заголовок</strong></th>--}}
                                    <th class="text-center"><strong>Действия</strong></th>
                                </tr>
                                </thead>
                                <tbody class="no-bd-y">
                                @if($products->count())
                                    @php
                                        if (!request()->page || request()->page == 1){
                                            $page = false;
                                        }else{
                                            $page = request()->page -1;
                                        }
                                    @endphp
                                    @foreach($products as $k => $product)
                                        @php $number = $k+1 @endphp
                                        <tr>
                                            <td class="text-center">{{($page == false)?$number:$page.$number}}</td>
                                            <td class="text-center">{{$product->name}}</td>
                                            <td class="text-center">
                                                @foreach($categories as $category)
                                                    @foreach($product->category as $k => $item)
                                                    @if($category->id ==$item->category_id)
                                                         {!! ($k != 0)? ' , '.$category->title:' '.$category->title!!}
                                                    @endif
                                                    @endforeach
                                                @endforeach
                                            </td>
{{--                                            <td class="text-center">{{$product->caption}}</td>--}}
                                            <td class="text-center">
                                                @foreach($product->productDetail as $k => $productDetail)
                                                    @if($k == 0)
                                                        {{'#'.$productDetail->id.' '}}
                                                    @else
                                                        {{', '.'#'.$productDetail->id}}
                                                    @endif
                                                @endforeach
                                            </td>
                                            <td class="text-center">
                                                <a href="{{route('admin.products.edit',$product->id)}}" class="edit btn btn-sm btn-default" style="margin-top: 5px"><i class="fa fa-pencil"></i> Редактировать</a>
                                                <form action="{{route('admin.products.destroy',$product->id)}}" method="POST" style="display: inline-block;margin-bottom: 5px">
                                                    @csrf
                                                    <button type="submit" class="edit btn btn-sm btn-default" data-toggle="tooltip" data-placement="top">
                                                        <i class="fa fa-times-circle"></i> Удалить
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                            <div class="row">
                                <div class="col-md-12">
                                    {{ $products->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection