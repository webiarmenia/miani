@extends('layouts.appmiani')
@section('title')
    <title>Редактирование продукта - {{$item->name}}</title>
@endsection
@section('css')
    <link href="{{asset('plugins/datetimepicker/jquery.datetimepicker.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/pickadate/themes/default.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/pickadate/themes/default.date.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/pickadate/themes/default.time.css')}}" rel="stylesheet">
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" media="all">
    <link href="{{asset('plugins\bootstrap-switch\bootstrap-switch.css')}}" rel="stylesheet">
    <meta name="_token" content="{{csrf_token()}}" />
    <style>
        .modal .active-image{
            border:2px solid red;
            opacity: 0.5;
        }

        .selected-image img{
            width: 100px !important;
            height: 100px !important;
        }
    </style>

@endsection
@section('content')
<div id="main-content">
    <div class="page-title">
        <i class="icon-custom-left"></i>
        <h3><strong class="wi-page-title" id="active-products-page">Редактирование продукта - {{$item->name}} </strong></h3>
        <br>
    </div>
    <form method="POST" action="{{route('admin.products.update',$item->id)}}">
        @csrf
    <div class="row">
        <div class="col-md-12">
            <div class="tabcordion">
                <ul id="myTab" class="nav nav-tabs">
                    <li class="active"><a href="#product_general" data-toggle="tab">Главная</a></li>
                    <li><a href="#product_properties" data-toggle="tab">Свойства</a></li>
                </ul>
                <div id="myTabContent" class="tab-content">
                    <div class="tab-pane fade active in" id="product_general">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Название <span class="asterisk">*</span>
                                        </label>
                                        <div class="col-sm-7">
                                            <input type="text" id="name" class="form-control form-control @error('name') parsley-error @enderror"
                                                   value="{{old('name')? old('name'): $item->name}}"
                                                   name="name">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Категория <span
                                                    class="asterisk">*</span></label>
                                        <div class="col-sm-7">

                                            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Choose</button>
                                            <div class="category-titles">
                                                @foreach($productParents as $key => $value)
                                                    <h3 id="{{ $value }}">{{ $productParentsTitle[$key] }}</h3>
                                                @endforeach
                                            </div>
                                            <input type="hidden" name="category" id="category-input" >
                                            <!-- Modal -->
                                            <div class="modal fade" id="myModal" role="dialog">
                                                <div class="modal-dialog">

                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-body sortable-panel">
                                                                            <div class="row">
                                                                                <div class="col-md-8">
                                                                                    <div class="dd nestable">
                                                                                        {!! $html !!}
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

{{--                                    <div class="form-group">--}}
{{--                                        <label class="col-sm-2 control-label">Категория <span class="asterisk">*</span></label>--}}
{{--                                        <div class="col-sm-7">--}}
{{--                                            <select class="js-example-basic-multiple " id="category" multiple title="Choose one or more" name="category[]">--}}
{{--                                                @if($categories->count())--}}
{{--                                                    @php $e_s_length = count($item->category); @endphp--}}
{{--                                                    @foreach($categories as $category)--}}
{{--                                                            @php--}}
{{--                                                                for($i=0;$i<$e_s_length;$i++){--}}
{{--                                                                    if($category->id == $item->category[$i]->category_id){--}}
{{--                                                                        break;--}}
{{--                                                                    }--}}
{{--                                                                }--}}
{{--                                                                if($i==$e_s_length){--}}
{{--                                                                    echo '<option value="'.$category->id.'" >'.$category->title.'</option>';--}}
{{--                                                                }--}}
{{--                                                                else{--}}
{{--                                                                    echo '<option value="'.$category->id.'" selected>'.$category->title.'</option>';--}}
{{--                                                                }--}}
{{--                                                            @endphp--}}
{{--                                                    @endforeach--}}
{{--                                                @endif--}}

{{--                                            </select>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <div class="form-group">--}}
{{--                                        <label class="col-sm-2 control-label">Заголовок <span class="asterisk">*</span>--}}
{{--                                        </label>--}}
{{--                                        <div class="col-sm-7">--}}
{{--                                            <input type="text" id="caption" class="form-control form-control @error('caption') parsley-error @enderror"--}}
{{--                                                   value="{{old('caption')? old('caption'): $item->caption}}" name="caption">--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Описание
                                        </label>
                                        <div class="col-sm-7">
                                            <textarea rows="5" class="form-control valid" placeholder="Описание..." name="description" id="description">{{old('description')? old('description'): $item->description}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Статус
                                        </label>
                                        <div class="col-sm-7">
                                            <input type="checkbox" id="status" name="status" class="available" {{($item->status == true) ? 'checked': ''}} data-on-color="success" data-off-color="danger" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="product_properties">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-horizontal" id="properties">
                                    @foreach($item->productDetail as $k => $productDetail)
                                    <div class="properties">

                                        @if($k != 0)
                                            <hr>
                                        @endif
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">ИДЕНТИФИКАТОР <span class="asterisk">*</span></label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" value="{{$productDetail->id}}" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Размер <span class="asterisk">*</span></label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" name="size[{{$productDetail->id}}]" value="{{$productDetail->size}}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Скидка</label>
                                                <div class="col-sm-7">
                                                    <select class="form-control" name="discount[{{$productDetail->id}}]"
                                                            id="discount">
                                                        @if(!empty($discounts))

                                                            <option value="{{null}}" selected>Нет Скидки</option>
                                                            @foreach($discounts as $discount)
                                                                @if(isset($productDetail->discount_id) && $productDetail->discount_id == $discount->id)
                                                                    <option value="{{$discount->id}}" selected>{{$discount->name}}</option>
                                                                @else
                                                                    <option value="{{$discount->id}}" >{{$discount->name}}</option>
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Количество <span class="asterisk">*</span>
                                        </label>
                                        <div class="col-sm-7">
                                            <input type="number" class="form-control" name="quantity[{{$productDetail->id}}]" value="{{$productDetail->quantity}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Цена <span class="asterisk">*</span>
                                        </label>
                                        <div class="col-sm-7">
                                            <input type="number" class="form-control" name="price[{{$productDetail->id}}]" value="{{$productDetail->price}}">
                                        </div>
                                    </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Цвет <span
                                                            class="asterisk">*</span></label>
                                                <div class="col-sm-7">
                                                    <select class="form-control" name="color[{{$productDetail->id}}]">
                                                            @foreach($lists as $k => $list)
                                                                <option value="{{$k}}" {{($k == $productDetail->color)? 'selected':''}}>{{$list}}</option>
                                                            @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                    <div class="form-group">
                                    <label class="col-sm-2 control-label">Другие детали
                                    </label>
                                    <div class="col-sm-7">
                                        <textarea rows="5" class="form-control valid" placeholder="Product description..." name="other_details[{{$productDetail->id}}]">{{$productDetail->other_details}}</textarea>
                                    </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Имеется в наличи
                                        </label>
                                        <div class="col-sm-7">
                                            <input type="checkbox" data-id="{{$productDetail->id}}" name="available[{{$productDetail->id}}]" {{($productDetail->available == true) ? 'checked': ''}} data-on-color="success" data-off-color="danger" data-on-text="Да" data-off-text="Нет" class="available">
                                            <input type="hidden" class="hidden_available" name="hidden_available[{{$productDetail->id}}]" value="{{($productDetail->available == true) ? 1: 0}}">
                                        </div>
                                    </div>
                                            <div class="img-selectable">
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Картинки
                                                    </label>
                                                    <div class="col-sm-7">
                                                        <button  class="btn btn-primary imageresource" style="margin-bottom: 20px;" type="button">+</button>
                                                        <div class="selected-image" style="clear: both;">
                                                            @php $array = []; @endphp
                                                            @foreach($productDetails as $detail)
                                                                @if($productDetail->id == $detail->id)
                                                                    @foreach($detail->productImages as $image)
                                                                            @foreach($media_images as $key =>$value)
                                                                                @if($value->id ==$image->image)
                                                                                <img src="{{ asset('/storage/'.$value->path. $value->uniq_name.'_'.'small'.'.' .$value->ext) }}" data_id="{{$value->id}}" class = 'image' width="200" height="200">
                                                                                @php array_push($array,$value->id); @endphp
                                                                                @endif
                                                                            @endforeach
                                                                    @endforeach
                                                                @endif
                                                            @endforeach
                                                            <input type="hidden" name="cover_image[{{$productDetail->id}}]" value="{{(!empty($array))? implode(",",$array):''}}">

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2"></div>
                                                <div class="col-sm-7" >
                                                    <input type="button" class="ibtnDel btn btn-md btn-danger "  value="Удалить" style="float: right">
                                                </div>
                                            </div>
                                            <input type="hidden" name="product_detail_id[]" value="{{$productDetail->id}}">

                                </div>
                                    @endforeach
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-2">

                                    </div>
                                    <div class="add-row col-sm-7" >
                                        <input type="button" class="btn btn-success" id="addrow" value="+" style="color: white!important;float: left" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog" style="width: 60%; height: 50%;">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Картинки</h4>
                                </div>
                                <div id="table_data">
                                    @include('admin.products.pagination_data')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <ul class="alert alert-danger alert-danger-form" style="display:none">

                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 m-t-20 m-b-40 align-center">
                <a href="{{route('admin.products')}}" class="btn btn-default m-r-10 m-t-10">
                        <i class="fa fa-reply"></i> Назад
                </a>
                <button id="submit" type="submit" class="btn btn-success m-t-10"><i class="fa fa-check"></i> Обновить
                </button>
            </div>
        </div>
    </div>
    </form>
</div>
@endsection

@section('javascript')
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.config.autoParagraph = false;
        CKEDITOR.config.filebrowserBrowseUrl = '/laravel-filemanager';
        CKEDITOR.replace('description');

    </script>
    <script src="{{asset('js/select2.min.js')}}" type="text/javascript"></script>
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{asset('plugins/datetimepicker/jquery.datetimepicker.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('plugins/pickadate/picker.js')}}"></script>
    <script src="{{asset('plugins/pickadate/picker.date.js')}}"></script>
    <script src="{{asset('plugins/pickadate/picker.time.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-progressbar/bootstrap-progressbar.js')}}"></script>
    <script src="{{asset('plugins\bootstrap-switch\bootstrap-switch.js')}}"></script>

    <!-- END  PAGE LEVEL SCRIPTS -->
    <script src="{{asset('js/application.js')}}"></script>

    <script>
        $(document).ready(function () {
            if($(".checkbox").hasClass("myCheck")){
                $(".myCheck").prop('checked',true);
            }
        });

    </script>

    <script>
        $(".checkbox").click(function(){
            $title=$(this).attr("data-title");
            $id=$(this).val();
            if($(this).prop('checked') == true)
            {
                $(this).prop('checked',true);
                $(".category-titles").append("<h3 id="+$id+">"+$title+"</h3>");
            }else{
                document.getElementById($id).remove();
                $(this).prop('checked',false);
            }
        });
    </script>

    <script>
        $(document).ready(function () {
            var counter = 0;

            $("#addrow").on("click", function () {
                var newRow = $('<div class="properties" >');
                var cols = "";
                cols += '<hr>';
                cols += '<div class="form-group">\n' +
                    '<label class="col-sm-2 control-label">Размер <span class="asterisk">*</span></label>\n' +
                    '<div class="col-sm-7">\n' +
                    '<input type="text" class="form-control" name="new_size[]">\n' +
                    '</div>\n' +
                    '</div>';

                cols += '<div class="form-group">\n' +
                    '<label class="col-sm-2 control-label">Скидка</label>\n' +
                    '<div class="col-sm-7">\n' +
                    '<select class="form-control" name="new_discount[]"\n' +
                    'id="discount">\n' +
                    '@if($discounts->count())\n' +
                    '<option value="{{null}}" selected>Нет Скидки</option>\n' +
                    '@foreach($discounts as $discount)\n' +
                    '<option value="{{$discount->id}}">{{$discount->name}}</option>\n' +
                    '@endforeach\n' +
                    '@endif\n' +
                    '</select>\n' +
                    '</div>\n' +
                    '</div>';

                cols += '<div class="form-group">\n' +
                    '<label class="col-sm-2 control-label">Количество <span class="asterisk">*</span>\n' +
                    '</label>\n' +
                    '<div class="col-sm-7">\n' +
                    '<input type="number" class="form-control" name="new_quantity[]" min="0">\n' +
                    '</div>\n' +
                    '</div>';

                cols += '<div class="form-group">\n' +
                    '<label class="col-sm-2 control-label">Цена  <span class="asterisk">*</span>\n' +
                    '</label>\n' +
                    '<div class="col-sm-7">\n' +
                    '<input type="number" class="form-control" name="new_price[]" min="0">\n' +
                    '</div>\n' +
                    '</div>';
                cols += '<div class="form-group">\n' +
                    '<label class="col-sm-2 control-label">Цвет <span\n' +
                    'class="asterisk">*</span></label>\n' +
                    '<div class="col-sm-7">\n' +
                    '<select class="form-control" name="new_color[]">\n' +
                    '@foreach($lists as $k => $list)\n' +
                    '<option value="{{$k}}">{{$list}}</option>\n' +
                    '@endforeach' +
                    '</select>\n' +
                    '</div>\n' +
                    '</div>';

                cols += '<div class="form-group">\n' +
                    '<label class="col-sm-2 control-label">Другие детали\n' +
                    '</label>\n' +
                    '<div class="col-sm-7">\n' +
                    '<textarea rows="5" class="form-control valid" placeholder="Product description..." name="new_other_details[]"></textarea>\n' +
                    '</div>\n' +
                    '</div>';


                cols += '<div class="form-group">\n' +
                    '<label class="col-sm-2 control-label">Имеется в наличи\n' +
                    '</label>\n' +
                    '<div class="col-sm-7" >\n' +
                    '<input type="checkbox" name="new_available[]" class="available" data-on-color="success" data-off-color="danger" data-on-text="Да" data-off-text="Нет" value="{{1}}">\n' +
                    '<input type="hidden" class="hidden_available" name="new_hidden_available[]" value="{{0}}">'+
                    '</div>\n' +
                    '</div>\n';

                cols += '<div class="img-selectable">\n' +
                    '<div class="form-group">\n' +
                    '<label class="col-sm-2 control-label">Картинки\n' +
                    '</label>\n' +
                    '<div class="col-sm-7">\n' +
                    '<button  class="btn btn-primary imageresource" style="margin-bottom: 20px;" type="button">+</button>\n' +
                    '<div class="selected-image" style="clear: both;">\n' +
                    '<input type="hidden" name="new_cover_image[]">\n' +
                    '</div>\n' +
                    '</div>\n' +
                    '</div>\n' +
                    '</div>';
                cols += '<div class="form-group">' +
                    '<div class="col-sm-2"></div>' +
                    '<div class="col-sm-7" >' +
                    '<input type="button" class="ibtnDel btn btn-md btn-danger "  value="Удалить" style="float: right">' +
                    '</div>' +
                    '</div>';
                cols += '<input type="hidden" name="product_detail_id[]" value="0">';
                newRow.append(cols);
                $("#properties").append(newRow);
                counter++;
                $('.js-example-basic-multiple').select2();
                $(".available").bootstrapSwitch();
            });


            $("#properties").on("click", ".ibtnDel", function (event) {
                $(this).closest('div.properties').remove();
                counter -= 1
            });


        });
    </script>
    <script>
        $(document).ready(function () {
            $('.js-example-basic-multiple').select2();
        });
    </script>
    <script type="text/javascript">

        jQuery(document).ready(function () {

            jQuery('form').submit(function (e) {

                e.preventDefault();
                jQuery.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                $("#description").val(CKEDITOR.instances.description.getData());
                jQuery.ajax({
                    url: "{{ route('admin.products.update',$item->id) }}",
                    method: 'post',
                    data: $('form').serialize(),
                    success: function (data) {
                        $(".alert-danger-form").empty();
                        if (data.errors) {
                            console.log(data);
                            jQuery.each(data.errors, function (key, value) {
                                jQuery('.alert-danger-form').show();
                                jQuery('.errors').show();
                                jQuery('.alert-danger-form').append('<li class="error-li">' + value + '</li>');

                            });
                        } else if (data.created) {
                            window.location.href = '{{route('admin.products')}}';

                        }
                    },

                });
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $('.js-example-basic-multiple').select2();
            $("[name='status']").bootstrapSwitch();
            $(".available").bootstrapSwitch();
        });
    </script>

    <script>
        $(document).ready(function(){
            var div;
            var images;
            $(document).on("click",".imageresource", function(e) {
                // e.preventDefault();
                div =  $(this).parent('.col-sm-7').children('.selected-image');
                images =  div.find('input[type="hidden"]');
                var page = 1;
                var img = $(images).val();
                fetch_data(page,img);
                $('#imagemodal').modal('show');

            });
            $(document).on("click",".modal-body .image", function(elem) {
                elem.preventDefault();
                var array = [];
                if ($(this).hasClass('active-image')) {
                    $(this).removeClass('active-image');
                    $(div).find('img[data_id='+$(this).attr('data_id')+']').remove();

                }else {
                    $(this).clone().appendTo(div);
                    $(this).addClass('active-image');
                }
                var selected_image = $(div).find('img');
                selected_image.each(function (k,elem) {
                    array.push($(elem).attr("data_id"));
                });
                $(images).attr('value',array);
            });

            $(document).on('click', '.pagination a', function(event){
                event.preventDefault();
                var page = $(this).attr('href').split('page=')[1];

                var img = $(images).val();
                fetch_data(page,img);
            });
            $(document).on("click","#add-image-btn", function(e) {
                e.preventDefault();
                jQuery.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                var image = $('#cover')[0].files[0];
                var form = new FormData();
                var page = 1;
                var img = $(images).val();
                form.append('type', 'product');
                form.append('cover', image);
                form.append('page', page);
                form.append('images', img);
                jQuery.ajax({
                    url: "{{ route('admin.media.store') }}",
                    data: form,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function (data) {
                        $(".alert-danger").empty();
                        if (data.validation) {
                            jQuery.each(data.validation, function (key, value) {
                                jQuery('.alert-danger-image').show();
                                jQuery('.errors').show();
                                jQuery('.alert-danger-image').append('<li class="error-li">' + value + '</li>');

                            });
                        } else {
                            $('#table_data').html(data);
                        }
                    },

                });
            });
            function fetch_data(page,img)
            {
                $.ajax({
                    url:"/admin_v128_11/products/fetch_data?page="+page+"&images="+img,
                    success:function(data)
                    {
                        $('#table_data').html(data);
                    }
                });
            }
            $(document).on('switchChange.bootstrapSwitch','.available', function (event, state){
                if(this.checked){
                    $(this).closest('.col-sm-7').children('.hidden_available').attr('value',1);
                }else{
                    $(this).closest('.col-sm-7').children('.hidden_available').attr('value',0);
                }
            });

            $(document).on("click",".show-image", function(e) {
                $('.hide-add-image').toggleClass('hide');
            });

        });
    </script>
@endsection
