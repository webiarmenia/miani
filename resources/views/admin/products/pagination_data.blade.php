<div class="modal-body text-center">
    @foreach($media as $key =>$value)
        @if(isset($images) && $images != false)
            @php $length = count($images); @endphp
            @for($i= 0 ;$i<$length;$i++)
                @if($value->id == $images[$i])
                    @break
                @endif
            @endfor
            @if($i == $length)
                <img src="{{ asset('/storage/'.$value->path. $value->uniq_name.'_'.'small'.'.' .$value->ext) }}" data_id="{{$value->id}}" class = 'image' width="200" height="200">
            @else
                <img src="{{ asset('/storage/'.$value->path. $value->uniq_name.'_'.'small'.'.' .$value->ext) }}" data_id="{{$value->id}}"  class = 'image active-image' width="200" height="200">
            @endif
        @else
            <img src="{{ asset('/storage/'.$value->path. $value->uniq_name.'_'.'small'.'.' .$value->ext) }}" data_id="{{$value->id}}" class = 'image' width="200" height="200">
        @endif
    @endforeach
    <div class="row">
        <div class="col-md-12">
            {{ $media->links() }}
        </div>
    </div>
        <hr>
        <div class="form-group">
            <div class="add-image" id="add-image">
                <label style="float: left"><strong>Дабавить новую картинку</strong></label>
                <div style="clear: both"></div>
                <a class="show-image btn btn-success m-b-10" style="float: left;padding: 0px 10px;margin: 0px;border-radius: 0px" > + </a>
                <button type="button" class=" btn btn-primary" data-dismiss="modal" style="float: right;;margin: 0px;border-radius: 0px">ОК</button>
                <div style="clear: both;" class="hide-add-image hide">
                <input type="file" name="cover" id="cover" style="float: left;" >
                <button type="submit" class="add-image btn btn-success m-b-10" style="float: left;padding: 0px 10px;margin: 0px;border-radius: 0px" id="add-image-btn">Загрузить</button>
                <input type="hidden" name="type" value="product">
                </div>
            </div>
        </div>
        <div style="clear: both"></div>
        <div class="panel-body">
            <ul class="alert alert-danger alert-danger-image" style="display:none">

            </ul>
        </div>
</div>

