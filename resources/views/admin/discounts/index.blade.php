@extends('layouts.appmiani')
@section('title')
    <title>Скидки</title>
@endsection
@section('content')
    <div class="m-b-20 clearfix">
        <div class="page-title pull-left">
            <h3 class="pull-left"><strong class="wi-page-title" id="active-discounts-page">Скидки</strong></h3>
        </div>
        <div class="pull-right">
            <a href="{{route('admin.discounts.create')}}" class="btn btn-success m-t-10"><i class="fa fa-plus p-r-10"></i>Дабавить Скидку</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if (\Session::has('success'))
            <div class="alert alert-success fade in" style="width: 100%">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {!! \Session::get('success') !!}
            </div>
            @endif
                @if (\Session::has('error'))
                    <div class="alert alert-danger fade in" style="width: 100%">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {!! \Session::get('error') !!}
                    </div>
                @endif
            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="no-bd">
                                <tr>
                                    <th class="text-center"><strong>№</strong></th>
                                    <th class="text-center"><strong>Имя</strong></th>
                                    <th class="text-center"><strong>Размер</strong></th>
                                    <th class="text-center"><strong>Действия</strong></th>
                                </tr>
                                </thead>
                                <tbody class="no-bd-y">
                                @if($discounts->count())
                                    @php
                                        if (!request()->page || request()->page == 1){
                                            $page = false;
                                        }else{
                                            $page = request()->page -1;
                                        }
                                    @endphp
                                @foreach($discounts as $k => $discount)
                                    @php $number = $k+1 @endphp
                                    <tr>
                                        <td class="text-center">{{($page == false)?$number:$page.$number}}</td>
                                        <td class="text-center">{{$discount->name}}</td>
                                        <td class="text-center">{{$discount->discount_size}}</td>
                                        <td class="text-center">
                                            <a href="{{route('admin.discounts.view',$discount->id)}}" class="edit btn btn-sm btn-default" style="margin-top: 5px"><i class="fa fa-eye"></i> Посмотреть</a>
                                            <a href="{{route('admin.discounts.edit',$discount->id)}}" class="edit btn btn-sm btn-default" style="margin-top: 5px"><i class="fa fa-pencil"></i> Редактировать</a>
                                            <form action="{{route('admin.discounts.destroy',$discount->id)}}" method="POST" style="display: inline-block;margin-bottom: 5px">
                                                @csrf
                                                <button type="submit" class="edit btn btn-sm btn-default" data-toggle="tooltip" data-placement="top">
                                                    <i class="fa fa-times-circle"></i> Удалить
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                @endif
                                </tbody>
                            </table>
                            <div class="row">
                                <div class="col-md-12">
                                    {{ $discounts->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection