@extends('layouts.appmiani')
@section('title')
    <title>Discount - {{$item->name}}</title>
@endsection

@section('content')
    <div class="m-b-20 clearfix">
        <div class="page-title pull-left">
            <h3 class="pull-left"><strong class="wi-page-title" id="active-discounts-page">Скидка - {{$item->name}}</strong></h3>
        </div>
    </div>
<div class="panel-default">
    <div class="panel-body">
        <div class="row">
            <div>
                <div class="col-sm-3">
                    <span>Имя </span>
                </div>
                <div class="col-sm-9">
                    <span>{{$item->name}}</span>
                </div>
            </div>
            <div>
                <div class="col-sm-3">
                    <span>Размер</span>
                </div>
                <div class="col-sm-9">
                    <span>{{$item->discount_size}}</span>
                </div>
            </div>
            <div>
                <div class="col-sm-3">
                    <span>Тип скидки</span>
                </div>
                <div class="col-sm-9">
                    <span>
                    @if($item->discount_type == true)
                            Процент
                        @else
                            Сумма
                        @endif
                    </span>
                </div>
            </div>
            <div>
                <div class="col-sm-3">
                    <span>Дата от.</span>
                </div>
                <div class="col-sm-9">
                    <span>{{$item->date_from}}</span>
                </div>
            </div>
            <div>
                <div class="col-sm-3">
                    <span>Дата до.</span>
                </div>
                <div class="col-sm-9">
                    <span>{{$item->date_to}}</span>
                </div>
            </div>

            <div>
                <div class="col-sm-3">
                    <span>Описание скидки</span>
                </div>
                <div class="col-sm-9">
                    <span>{{$item->discount_description}}</span>
                </div>
            </div>
            <hr>
            <div class="clearfix"></div>
            <br>
            <span style="display: block;height: 1px;background: #ccc;"></span>
            <br>
            <div>
            <div class="col-sm-3">
                <span>Условия скидки</span>
            </div>
            <div class="col-sm-9" style="float: right">
                <table id="myTable" class="table order-list">
                    <thead>
                    <tr>
                        <td>Тип условия *</td>
                        <td>Ключ условия *</td>
                        <td>Значение условия *</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($item->discountConditions as $discountConditions)
                        <tr>
                        <td>
                            {{\App\DiscountCondition::getType()[$discountConditions->condition_type]}}
                        </td>
                        <td>
                            {{\App\DiscountCondition::getList()[$discountConditions->condition_key]}}
                        </td>
                        <td>
                            {{$discountConditions->condition_value}}
                        </td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>

            </div>
            </div>
        </div>
    </div>
</div>
    <div class="row" style="padding-bottom: 20px">
        <div class="col-md-12">
            <div class="pull-right">
                <a href="{{route('admin.discounts')}}" class="btn btn-default m-r-10 m-t-10">
                    <i class="fa fa-reply"></i> Назад
                </a>
            </div>
        </div>
    </div>
@endsection
