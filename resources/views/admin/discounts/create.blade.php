@extends('layouts.appmiani')
@section('title')
    <title>Новая Скидка</title>
@endsection
@section('css')
    <link href="{{asset('plugins/datetimepicker/jquery.datetimepicker.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/pickadate/themes/default.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/pickadate/themes/default.date.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/pickadate/themes/default.time.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><strong class="wi-page-title" id="active-discounts-page">Новая скидка</strong></h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <form id="form1" class="form-horizontal"  action="{{route('admin.discounts.store')}}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Название <span class="asterisk">*</span>
                                </label>
                                <div class="col-sm-9">
                                    <input type="text" data-parsley-minlength="3" class="form-control @error('name') parsley-error @enderror"
                                           data-parsley-id="1143" name="name" value="{{old('name')}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Размер <span class="asterisk">*</span>
                                </label>
                                <div class="col-sm-9">
                                    <input type="text" data-parsley-minlength="3" class="form-control @error('discount_size') parsley-error @enderror"
                                           data-parsley-id="1143" name="discount_size" value="{{old('discount_size')}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Тип скидки <span class="asterisk">*</span>
                                </label>
                                <div class="col-sm-9">
                                    <select class="form-control" data-style="input-sm btn-default" name="discount_type">
                                        <option value="0">Процент</option>
                                        <option value="1">Сумма</option>
                                    </select>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Дата от. <span class="asterisk">*</span>
                                </label>
                                <div class="md-form col-sm-9">
                                    <input placeholder="Дата" name="date_from" type="text" id="date-picker-example"
                                           class="form-control datepicker @error('date_from') parsley-error @enderror"
                                           style="padding: 10px" value="{{old('date_from')}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Дата до. <span class="asterisk">*</span>
                                </label>
                                <div class="md-form col-sm-9">
                                    <input placeholder="Дата" name="date_to" type="text" id="date-picker-example"
                                           class="form-control datepicker @error('date_to') parsley-error @enderror"
                                           style="padding: 10px" value="{{old('date_to')}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Описание скидки
                                </label>
                                <div class="col-sm-9">
                                    <textarea name="discount_description" rows="5" class="form-control valid" data-parsley-minlength="30">{{old('discount_description')}}</textarea>
                                </div>
                            </div>
                            <hr>
                            <div class="col-sm-9" style="float: right">
                            <table id="myTable" class="table order-list">
                                <thead>
                                <tr>
                                    <td>Тип условия *</td>
                                    <td>Ключ условия *</td>
                                    <td>Значение условия *</td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="col-sm-4">
                                        <select class="form-control" data-style="input-sm btn-default" name="condition_type[]" style="padding: 10px">
                                            @foreach($types as $k =>$type)
                                            <option value="{{$k}}">{{$type}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td class="col-sm-4">
                                        <select class="form-control" data-style="input-sm btn-default" name="condition_key[]">
                                            @foreach($lists as $k =>$list)
                                                <option value="{{$k}}">{{$list}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td class="col-sm-3">
                                        <input type="text" data-parsley-minlength="3" class="form-control" name="condition_value[]"  >
                                    </td>
                                    <td class="col-sm-2"><a class="deleteRow"></a>

                                    </td>
                                </tr>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="5" style="text-align: left;">
                                        <input type="button" class="btn btn-success" id="addrow" value="+" style="color: white!important;" />
                                    </td>
                                </tr>
                                <tr>
                                </tr>
                                </tfoot>
                            </table>
                                @if ($errors->any())
                                <ul class="alert alert-danger" style="width: 100%">
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                </ul>
                                @endif
                                @if (\Session::has('error'))
                                    <ul class="alert alert-danger" style="width: 100%">
                                        {!! \Session::get('error') !!}
                                    </ul>
                                @endif
                            </div>


                            <div class="col-sm-9 col-sm-offset-3">
                                <div class="pull-right">
                                    <a href="{{route('admin.discounts')}}" class="btn btn-default m-r-10 m-t-10">
                                        <i class="fa fa-reply"></i> Назад
                                    </a>
                                    <button id="submit" type="submit" class="btn btn-success m-t-10"><i class="fa fa-check"></i> Дабавить
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('javascript')
    <script src="{{asset('js/bootstrap-datepicker.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/classie.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/modalEffects.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/application.js')}}" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            var counter = 0;

            $("#addrow").on("click", function () {
                var newRow = $("<tr>");
                var cols = "";

                cols += '<td><select class="form-control" data-style="input-sm btn-default" name="condition_type[]">\n' +
                    '                                            @foreach($types as $k =>$type)\n' +
                     '   <option value="{{$k}}">{{$type}}</option>\n' +
                '@endforeach\n' +
                    '                                        </select></td>';
                cols += '<td><select class="form-control" data-style="input-sm btn-default" name="condition_key[]">\n' +
                    '                                            @foreach($lists as $k =>$list)\n' +
                    '                                                <option value="{{$k}}">{{$list}}</option>\n' +
                    '                                            @endforeach\n' +
                    '                                        </select></td>';
                cols += '<td><input type="text" data-parsley-minlength="3" class="form-control" name="condition_value[]" >\n</td>';

                cols += '<td><input type="button" class="ibtnDel btn btn-md btn-danger "  value="Удалить"></td>';
                newRow.append(cols);
                $("table.order-list").append(newRow);
                counter++;
            });



            $("table.order-list").on("click", ".ibtnDel", function (event) {
                $(this).closest("tr").remove();
                counter -= 1
            });


        });

    </script>
@endsection
