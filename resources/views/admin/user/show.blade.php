@extends('layouts.appmiani')
@section('title')
    <title>Пользователь - {{$users->email}}</title>
@endsection
@section('content')
    <div class="page-title">
        <div  class="form-group">
            <strong style="font-size: 20px;" class="wi-page-title" id="active-users-page">Имя</strong>  <small style="font-size: 20px;padding-left: 20px;">{{$users->name}}</small>
        </div>
        <div class="form-group">
            <strong style="font-size: 20px;">Эл.адрес</strong>  <small style="font-size: 20px; padding-left: 20px">{{$users->email}}</small>
        </div>
    </div>
    <div class="m-b-20 clearfix">
        <div class="page-title pull-left">
            <h1 class="pull-left"><strong>Пожелания</strong></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
                            <table id="products-table" class="table table-tools table-hover">
                                <thead class="no-bd">
                                <tr>
                                    <th class="text-left"><strong>Идентификатор</strong></th>
                                    <th class="text-left"><strong>Название</strong></th>
                                    <th class="text-left"><strong>Размер</strong></th>
                                    <th class="text-left"><strong>Цвет</strong></th>
                                    <th class="text-left"><strong>Действия</strong></th>
                                </tr>
                                </thead>
                                <tbody class="no-bd-y">
                                @foreach($users->wishes as $key =>$user)
                                    <tr>
                                        <td class="text-left">{{'#'.$user->productDetail->id}}</td>
                                        <td class="text-left">{{$user->productDetail->product->name}}</td>
                                        <td class="text-left">{{$user->productDetail->size}}</td>
                                        <td class="text-left">{{$color[$user->productDetail->color]}}</td>
                                        <td class="text-left">
                                            <a href="{{route('admin.products.edit',$user->productDetail->product->id)}}" class="edit btn btn-sm btn-default"><i class="fa fa-eye"></i> Посмотреть</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-b-20 clearfix">
        <div class="page-title pull-left">
            <h1 class="pull-left"><strong>Корзина</strong></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
                            <table id="products-table" class="table table-tools table-hover">
                                <thead class="no-bd">
                                <tr>
                                    <th class="text-left"><strong>Идентификатор</strong></th>
                                    <th class="text-left"><strong>Название</strong></th>
                                    <th class="text-left"><strong>Размер</strong></th>
                                    <th class="text-left"><strong>Цвет</strong></th>
                                    <th class="text-left"><strong>Количество</strong></th>
                                    <th class="text-left"><strong>Действия</strong></th>
                                </tr>
                                </thead>
                                <tbody class="no-bd-y">
                                @if($users->cart != null)
                                    @foreach($users->cart as $k =>$cart)
                                        <tr>
                                            <td class="text-left">{{'#'.$cart->productDetail->id}}</td>
                                            <td class="text-left">{{$cart->productDetail->product->name}}</td>
                                            <td class="text-left">{{$cart->productDetail->size}}</td>
                                            <td class="text-left">{{$color[$cart->productDetail->color]}}</td>
                                            <td class="text-left">{{$cart->quantity}}</td>
                                            <td class="text-left">
                                                <a href="{{route('admin.products.edit',$cart->productDetail->product->id)}}" class="edit btn btn-sm btn-default"><i class="fa fa-eye"></i> Посмотреть</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="m-b-20 clearfix">
        <div class="page-title pull-left">
            <h1 class="pull-left"><strong>История</strong></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
                            <table id="products-table" class="table table-tools table-hover">
                                <thead class="no-bd">
                                <tr>
                                    <th class="text-left"><strong>индентификатор заказа</strong></th>
                                    <th class="text-left"><strong>Итоговая цена</strong></th>
                                    <th class="text-left"><strong>Действия</strong></th>
                                </tr>
                                </thead>
                                <tbody class="no-bd-y">
                                @foreach($users->orders as $key =>$history)
                                    <tr>
                                        <td class="text-left">{{'#'.$history->id}}</td>
                                        <td class="text-left">{{$history->total_price}}</td>
                                        <td class="text-left">
                                            <a href="{{route('admin.orders.view',$history->id)}}" class="edit btn btn-sm btn-default"><i class="fa fa-eye"></i> Посмотреть</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="padding-bottom: 20px">
        <div class="col-md-12">
            <div class="pull-right">
                <a href="{{route('admin.users')}}" class="btn btn-default m-r-10 m-t-10">
                    <i class="fa fa-reply"></i> Назад
                </a>
            </div>
        </div>
    </div>
@endsection
