@extends('layouts.appmiani')
@section('title')
<title>Пользователи</title>
@endsection
@section('content')
    <div class="m-b-20 clearfix">
        <div class="page-title pull-left">
            <h3 class="pull-left"><strong class="wi-page-title" id="active-users-page">Пользователи</strong></h3>
        </div>
    </div>
    @if (\Session::has('error'))
        <div class="alert alert-danger fade in" style="width: 100%">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
                            <table id="products-table" class="table table-tools table-hover">
                                <thead class="no-bd">
                                <tr>
                                    <th class="text-center"><strong>№</strong></th>
                                    <th class="text-center"><strong>Имя</strong></th>
                                    <th class="text-center"><strong>Фамилия</strong></th>
                                    <th class="text-center"><strong>Эл.адрес</strong></th>
                                    <th class="text-center"><strong>Статус</strong></th>
                                    <th class="text-center"><strong>Посмотреть</strong></th>
                                </tr>
                                </thead>
                                <tbody class="no-bd-y">
                                @foreach($users as $key => $user)
                                    <tr>
                                        <td  class="text-center">{{$key +1}}</td>
                                        <td class="text-center">{{$user->name}}</td>
                                        <td class="text-center">{{$user->surname}}</td>
                                        <td class="text-center">{{$user->email}}</td>
                                        <td class="text-center">
                                            <form action="{{route('admin.users.change',$user->id)}}" method="POST">
                                                @csrf
                                                @if($user->status == 1)
                                                    <span class="table-active"><button type="submit" class="btn btn-success  btn-sm my-0 ">Активный</button></span>
                                                @elseif($user->status == 30)
                                                    <span class="table-active"><button type="button" class="btn btn-warning  btn-sm my-0 ">Админ</button></span>
                                                @else
                                                    <span class="table-active"><button type="submit" class="btn btn-danger  btn-sm my-0">Неактивный</button></span>
                                                @endif
                                            </form>
                                        </td>
                                        <td class="text-center">
                                            <a href="{{route('admin.users.show',$user->id)}}" class="edit btn btn-sm btn-default" ><i class="fa fa-eye"></i> Посмотреть</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            {{ $users->links() }}
        </div>
    </div>
@endsection