@extends('layouts.appmiani')
@section('title')
    <title>Страницы</title>
@endsection
@section('content')
    <div class="m-b-20 clearfix">
        <div class="page-title pull-left">
            <h3 class="pull-left"><strong class="wi-page-title" id="active-pages-page">Страницы</strong></h3>
        </div>
        <div class="pull-right">
            <a href="{{route('admin.pages.create')}}" class="btn btn-success m-t-10"><i class="fa fa-plus p-r-10"></i> Дабавить Страницу</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if (\Session::has('success'))
                <div class="alert alert-success fade in" style="width: 100%">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! \Session::get('success') !!}
                </div>
            @endif
                @if (\Session::has('error'))
                    <div class="alert alert-danger fade in" style="width: 100%">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {!! \Session::get('error') !!}
                    </div>
                @endif
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
                            <table id="products-table" class="table table-tools table-hover">
                                <thead class="no-bd">
                                <tr>
                                    <th class="text-center"><strong>№</strong></th>
                                    <th class="text-center"><strong>Заголовок</strong></th>
                                    <th class="text-center"><strong>Картинка</strong></th>
                                    <th class="text-center"><strong>Действия</strong></th>
                                </tr>
                                </thead>
                                <tbody class="no-bd-y">
                                @foreach($pages as $key =>$page)
                                    <tr>
                                        <td  class="text-center">{{$key+1}}</td>
                                        <td class="text-center">{{$page->title}}</td>
                                            <td class="text-center">
                                                @if($page->media != null)
                                                <img src="{{asset('/storage/'.$page->media->path. $page->media->uniq_name.'_'.'small'.'.' .$page->media->ext) }}" class = 'image' width="70px">
                                                @else
                                                нет картинки
                                                @endif
                                            </td>
                                        <td class="text-center">
                                            <a href="{{ route('admin.pages.edit',$page->id)}}" class="edit btn btn-sm btn-default"><i class="fa fa-pencil"></i>Редактировать</a>
                                            <form action="{{route('admin.pages.destroy',$page->id)}}" method="POST" style="display: inline-grid;">
                                                @csrf
                                                <button type="submit" class="delete btn btn-sm btn-default"><i class="fa fa-times-circle"></i> Удалить </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            {{ $pages->links() }}
        </div>
    </div>
@endsection
