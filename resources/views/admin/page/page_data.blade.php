<div class="modal-body text-center">
    @foreach($media as $key =>$value)
        @if(isset($images) && $images != false)
            @php $length = count($images); @endphp
            @for($i= 0 ;$i<$length;$i++)
                @if($value->id == $images[$i])
                    @break
                @endif
            @endfor
            @if($i == $length)
                <img src="{{ asset('/storage/'.$value->path. $value->uniq_name.'_'.'small'.'.' .$value->ext) }}" data_id="{{$value->id}}" class = 'image' width="200" height="200">
            @else
                <img src="{{ asset('/storage/'.$value->path. $value->uniq_name.'_'.'small'.'.' .$value->ext) }}" data_id="{{$value->id}}"  class = 'image active-image' width="200" height="200">
            @endif
        @else
            <img src="{{ asset('/storage/'.$value->path. $value->uniq_name.'_'.'small'.'.' .$value->ext) }}" data_id="{{$value->id}}" class = 'image' width="200" height="200">
        @endif
    @endforeach
    <div class="row">
        <div class="col-md-12">
            {{ $media->links() }}
        </div>
    </div>
</div>
