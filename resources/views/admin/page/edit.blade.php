@extends('layouts.appmiani')
@section('title')
    <title>Редактирование Страница</title>
@endsection
@section('css')
    <meta name="_token" content="{{csrf_token()}}" />
    <style>
        .modal .active-image{
            border:5px solid #00e7ff85;
        }
        .selected-image img{
            width: 100px !important;
            height: 100px !important;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong class="wi-page-title" id="active-pages-page">Редактирование</strong> Страницы - {{$pages->title}}</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <form method="post" action="{{ route('admin.pages.update', $pages->id) }}" id="form1" class="form-horizontal"  enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Заголовок<span class="asterisk">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" data-parsley-minlength="3" class="form-control @error('title') parsley-error @enderror" name="title" data-parsley-id="1143"
                                               value=" {{old('title')? old('title'):$pages->title}}">
                                        <ul class="parsley-errors-list" id="parsley-id-1143">
                                            @error('title')
                                            <li>{{ $message }}</li>
                                            @enderror
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Содержание<span class="asterisk">*</span></label>
                                    <div class="col-sm-9">
                                        <textarea type="text"  data-parsley-minlength="3"  name="content" id="content" class="form-control @error('content') parsley-error @enderror" data-parsley-id="1143">
                                            {{old('content')? old('content'):$pages->content}}
                                        </textarea>
                                        <ul class="parsley-errors-list" id="parsley-id-1143">
                                            @error('content')
                                            <li>{{ $message }}</li>
                                            @enderror
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Картинка</label>
                                    <div id="pop" class="col-sm-9">
                                        <button  class=" imageresource btn btn-primary" style="margin-bottom: 20px;" type="button">Картинка</button>
                                        <div class="selected-image">
                                            @foreach($media_images as $key =>$value)
                                                @if($value->id == $pages->cover)
                                                    <img src="{{asset('/storage/'.$value->path. $value->uniq_name.'_'.'small'.'.' .$value->ext) }}"
                                                         data_id="{{$value->id}}" class = 'image'>
                                                <input type="hidden" name="cover" value="{{$value->id}}">
                                                @endif
                                            @endforeach
                                                @if(!$pages->cover)
                                                    <input type="hidden" name="cover" >
                                                @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" style="width: 60%; height: 50%;">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Картинки</h4>
                                            </div>
                                            <div id="table_data">
                                                @include('admin.page.page_data')
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-9 col-sm-offset-3">
                                    <div class="pull-right">
                                        <a href="{{route('admin.pages')}}" class="btn btn-default m-r-10 m-t-10"><i class="fa fa-reply"></i> Назад</a>
                                        <button id="submit" type="submit" class="btn btn-success m-t-10"><i class="fa fa-check"></i>Сохранить</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.config.autoParagraph = false;
        CKEDITOR.config.filebrowserBrowseUrl  = '/laravel-filemanager';
        CKEDITOR.replace( 'content');
    </script>
    <script>
        $(document).ready(function(){
            var div;
            var images;
            $(document).on("click",".imageresource", function(e) {
                div =  $(this).parent('.col-sm-9').children('.selected-image');
                images =  div.find('input[type="hidden"]');
                var page = 1;
                var img = $(images).val();
                fetch_data(page,img);
                $('#imagemodal').modal('show');
            });
            $(document).on("click",".modal-body .image", function(elem) {
                elem.preventDefault();
                var array = [];
                if ($(this).hasClass('active-image')) {
                    $(this).removeClass('active-image');
                    $(div).find('img[data_id='+$(this).attr('data_id')+']').remove();
                }else {
                    $('img').removeClass('active-image');
                    $(div).find('.image').remove();
                    $(this).clone().appendTo(div);
                    var images =$(this).attr('data_id');
                    $('.selected-image input').attr('value',images);
                    $(this).addClass('active-image');
                }
                array.push($(this).attr('data_id'));
                $(images).attr('value',array);
            });
            $(document).on('click', '.pagination a', function(event){
                event.preventDefault();
                var page = $(this).attr('href').split('page=')[1];
                var img = $('.selected-image input').val();
                fetch_data(page,img);
            });
            function fetch_data(page,img) {
                $.ajax({
                    url:"/admin_v128_11/pages/fetch_data?page="+page+"&images="+img,
                    success:function(data) {
                        $('#table_data').html(data);
                    }
                });
            }
        });
    </script>
@endsection
