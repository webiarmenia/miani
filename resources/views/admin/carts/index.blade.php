@extends('layouts.appmiani')
@section('title')
    <title>Корзины</title>
@endsection
@section('content')
    <div class="m-b-20 clearfix">
        <div class="page-title pull-left">
            <h3 class="pull-left"><strong>Корзины</strong></h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if (\Session::has('success'))
                <div class="alert alert-success fade in" style="width: 100%">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! \Session::get('success') !!}
                </div>
            @endif
            @if (\Session::has('error'))
                <div class="alert alert-danger fade in" style="width: 100%">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! \Session::get('error') !!}
                </div>
            @endif
            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="no-bd">
                                <tr>
                                    <th class="text-center"><strong>№</strong></th>
                                    <th class="text-center"><strong>Имя</strong></th>
                                    <th class="text-center"><strong>Фамилия</strong></th>
                                    <th class="text-center"><strong>Е-маил</strong></th>
                                    <th class="text-center"><strong>Действия</strong></th>
                                </tr>
                                </thead>
                                <tbody class="no-bd-y">
                                @if($carts->count())
                                    @php
                                        if (!request()->page || request()->page == 1){
                                            $page = false;
                                        }else{
                                            $page = request()->page -1;
                                        }
                                    @endphp
                                    @foreach($carts as $k => $cart)
                                        @php $number = $k+1 @endphp
                                        <tr>
                                            <td class="text-center">{{($page == false)?$number:$page.$number}}</td>
                                            <td class="text-center">{{$cart->user->name}}</td>
                                            <td class="text-center">{{$cart->user->surname}}</td>
                                            <td class="text-center">{{$cart->user->email}}</td>
                                            <td class="text-center">
                                                <a href="{{route('admin.carts.view',$cart->id)}}" class="edit btn btn-sm btn-default" style="margin-top: 5px"><i class="fa fa-eye"></i> Посмотреть</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                            <div class="row">
                                <div class="col-md-12">
                                    {{ $carts->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection