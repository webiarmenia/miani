@extends('layouts.appmiani')
@section('title')
    <title>Пользователь - {{$item->user->email}}</title>
@endsection
@section('content')
    <div class="page-title">
        <div  class="form-group">
            <strong style="font-size: 20px;">Имя</strong>  <small style="font-size: 20px;padding-left: 20px;">{{$item->user->name}}</small>
        </div>
        <div  class="form-group">
            <strong style="font-size: 20px;">Фамилия</strong>  <small style="font-size: 20px;padding-left: 20px;">{{$item->user->surname}}</small>
        </div>
        <div class="form-group">
            <strong style="font-size: 20px;">Эл.адрес</strong>  <small style="font-size: 20px; padding-left: 20px">{{$item->user->email}}</small>
        </div>
    </div>
    <div class="m-b-20 clearfix">
        <div class="page-title pull-left">
            <h1 class="pull-left"><strong>Пожелания</strong></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
                            <table id="products-table" class="table table-tools table-hover">
                                <thead class="no-bd">
                                <tr>
                                    <th class="text-left"><strong>Идентификатор</strong></th>
                                    <th class="text-left"><strong>Название</strong></th>
                                    <th class="text-left"><strong>Размер</strong></th>
                                    <th class="text-left"><strong>Цвет</strong></th>
                                    <th class="text-left"><strong>Действия</strong></th>
                                </tr>
                                </thead>
                                <tbody class="no-bd-y">
{{--                                @foreach($item->wishes as $key =>$user)--}}
{{--                                    <tr>--}}
{{--                                        <td class="text-left">{{'#'.$user->productDetail->id}}</td>--}}
{{--                                        <td class="text-left">{{$user->productDetail->product->name}}</td>--}}
{{--                                        <td class="text-left">{{$user->productDetail->size}}</td>--}}
{{--                                        <td class="text-left">{{$color[$user->productDetail->color]}}</td>--}}
{{--                                        <td class="text-left">--}}
{{--                                            <a href="{{route('admin.products.edit',$user->productDetail->product->id)}}" class="edit btn btn-sm btn-default"><i class="fa fa-eye"></i> Посмотреть</a>--}}
{{--                                        </td>--}}
{{--                                    </tr>--}}
{{--                                @endforeach--}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
