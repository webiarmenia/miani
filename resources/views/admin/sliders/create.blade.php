@extends('layouts.appmiani')
@section('title')
    <title>Новый слайдер</title>
@endsection
@section('css')
    <link href="{{asset('plugins/datetimepicker/jquery.datetimepicker.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/pickadate/themes/default.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/pickadate/themes/default.date.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/pickadate/themes/default.time.css')}}" rel="stylesheet">
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong class="wi-page-title" id="active-sliders-page">Новый слайдер</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <form id="form1" class="form-horizontal" action="{{route('admin.sliders.store')}}" method="POST" data-parsley-validate enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Продукт
                                    </label>
                                    <div class="col-sm-9">
                                        <select name="product_id" class="form-control">
                                            <option selected="selected" value="{{null}}">нет продукта</option>
                                            @foreach($products as $product)
                                                <option value="{{$product->id}}" {{ old('product_id') == $product->id ? 'selected' : '' }}>{{$product->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Картинка <span class="asterisk">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="file" name="image" id="image">
                                        <ul class="parsley-errors-list" id="parsley-id-1143">
                                            @error('image')
                                            <li>{{ $message }}</li>
                                            @enderror
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Порядок </label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" name="sort" id="sort" min="1">
                                        <ul class="parsley-errors-list" id="parsley-id-1143">
                                            @error('sort')
                                            <li>{{ $message }}</li>
                                            @enderror
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-sm-9 col-sm-offset-3">
                                    <div class="pull-right">
                                        <button id="submit" type="submit" class="btn btn-success m-t-10" value='add' name="button">
                                            <i class="fa fa-check"></i>Дабавить
                                        </button>
                                        <a href="{{route('admin.sliders')}}" class="btn btn-default m-r-10 m-t-10">
                                            <i class="fa fa-reply"></i> Назад
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection



