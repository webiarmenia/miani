@extends('layouts.appmiani')
@section('title')
    <title>Слайдеры</title>
@endsection
@section('content')
    <div class="m-b-20 clearfix">
        <div class="page-title pull-left">
            <h3 class="pull-left"><strong class="wi-page-title" id="active-sliders-page">Слайдеры</strong></h3>
        </div>
        <div class="pull-right">
            <a href="{{route('admin.sliders.create')}}" class="btn btn-success m-t-10"><i class="fa fa-plus p-r-10"></i> Добавить Слайдер</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if (\Session::has('errorCreate'))
                <div class="alert alert-danger fade in" style="width: 100%">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! \Session::get('errorCreate') !!}
                </div>
            @endif
            @if (\Session::has('successCreate'))
                <div class="alert alert-success fade in" style="width: 100%">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! \Session::get('successCreate') !!}
                </div>
            @endif
            @if (\Session::has('successUpdate'))
                <div class="alert alert-success fade in" style="width: 100%">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! \Session::get('successUpdate') !!}
                </div>
            @endif
            @if (\Session::has('successDeleteYes'))
                <div class="alert alert-success fade in" style="width: 100%">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! \Session::get('successDeleteYes') !!}
                </div>
            @endif
            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="no-bd">
                                <tr>
                                    <th class="text-center"><strong>№</strong></th>
                                    <th class="text-center"><strong>Продукт</strong></th>
                                    <th class="text-center"><strong>Картинка</strong></th>
                                    <th class="text-center"><strong>Порядок</strong></th>
                                    <th class="text-center"><strong>Действия</strong></th>
                                </tr>
                                </thead>
                                <tbody class="no-bd-y">
                                @if($sliders->count())
                                    @foreach($sliders as $slider)
                                        <tr>
                                            <td class="text-center">{{$slider->id}}</td>
                                            <td class="text-center">
                                                {{($slider->product_id != null) ? $slider->product_id: 'нет продукта'}}
                                            </td>
                                            <td class="text-center">
                                                <img src="{{ asset("storage/sliders/".$slider->image.'_'.'small'.'.' .$slider->ext) }}" width="100" height="50">
{{--                                                C:\xampp\htdocs\Laravel\miani\storage\app\public\sliders\8Zr7RJCPmQOMSNnBASZJFzQXlyPphtlB_large.jpg--}}
                                            </td>
                                            <td class="text-center">{{$slider->sort}}</td>
                                            <td class="text-center">
                                                <a href="{{route('admin.sliders.edit',$slider->id)}}" class="edit btn btn-sm btn-default" style="margin-top: 5px"><i class="fa fa-pencil"></i> Редактировать</a>
                                                <form action="{{route('admin.sliders.destroy',$slider->id)}}" method="POST" style="display: inline-block;margin-bottom: 5px">
                                                    @csrf
                                                    <button type="submit" class="edit btn btn-sm btn-default" data-toggle="tooltip" data-placement="top">
                                                        <i class="fa fa-times-circle"></i> Удалить
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            {{ $sliders->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
