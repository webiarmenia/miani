@extends('layouts.appmiani')

@section('title')
    <title>Список сортируемых категорий</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('content')

    <div data-page="nestable_list">
        <div id="wrapper">
            <!-- BEGIN MAIN CONTENT -->
            <div id="main-content">
                <div class="page-title"> <i class="icon-custom-left"></i>
                    <h3 class="pull-left"><strong class="wi-page-title" id="active-categories-page">Список сортируемых категорий</strong></h3>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body sortable-panel">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="dd nestable">
                                            {!! $html !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-9 col-sm-offset-3">
                    <div class="pull-right">
                        <a href="{{route('admin.categories')}}" class="btn btn-default m-r-10 m-t-10">
                            <i class="fa fa-reply"></i> Назад
                        </a>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT -->
        </div>
    </div>

@endsection

@section('javascript')

    <script src="/plugins/nestable/jquery.nestable.js"></script>

    <script>
        $(document).ready(function () {
            if ($('.nestable').length && $.fn.nestable) {


                $(".nestable").nestable();

                $('.nestable').on('change', function functionToExecute() {

                    var page_id = [];
                    var page_item_order = [];
                    var page_parent_id_array = [];
                    $n = 1;
                    $z = "";
                    $('.nestable > ol > li').each(function () {
                        var $this = $(this);
                        $(this).attr("data-order", $n);
                        $n++;
                        $z = $n;
                        recursion($this);
                    });
                    $('.dd-list li').each(function (event, ui) {
                        if ($(this).has("ol").length) {
                            $(this).find('li').attr("data-parent-id", $(this).attr("id"));
                            $child = 'yes';
                        } else {
                            $(this).find('li').attr("data-parent-id", 0);
                            $child = 'no';
                        }
                        page_item_order.push($(this).attr("data-order"));
                        page_id.push($(this).attr("id"));
                        page_parent_id_array.push($(this).attr("data-parent-id"));
                    });

                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: "sortable",
                        method: "POST",
                        data: {
                            id: page_id,
                            order: page_item_order,
                            data_parent_id: page_parent_id_array,
                        },
                        success: function () {
                            function refresh(){
                                location = ''
                            }
                            refresh();
                        }
                    })
                });
            }

            function recursion($this) {
                if ($this.has("ol").length) {
                    $m = 1;
                    $this.find("> ol > li").each(function () {
                        var $thisa = $(this);
                        $(this).attr("data-order", $m);
                        $m++;
                        $k = $m;
                        recursion($thisa);
                    });
                    $m = $k;
                    $n = $z;
                }
            }
        });
    </script>
@endsection