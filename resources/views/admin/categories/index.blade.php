@extends('layouts.appmiani')
@section('title')
    <title>Категории</title>
@endsection
@section('content')
    <div class="m-b-20 clearfix">
        <div class="page-title pull-left">
            <h3 class="pull-left"><strong class="wi-page-title" id="active-categories-page">Категории</strong></h3>
        </div>
        <div class="pull-right" style="padding: 10px">
            <a href="{{route('admin.categories.create')}}" class="btn btn-success m-t-10"><i class="fa fa-plus p-r-10"></i> Добавить категорию</a>
        </div>
        <div class="pull-right" style="padding: 10px">
            <a href="{{route('admin.categories.sort')}}" class="btn btn-success m-t-10"><i class="fa fa-plus p-r-10"></i> Сортировка</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if (\Session::has('successCreate'))
                <div class="alert alert-success fade in" style="width: 100%">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! \Session::get('successCreate') !!}
                </div>
            @endif
            @if (\Session::has('successUpdate'))
                <div class="alert alert-success fade in" style="width: 100%">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! \Session::get('successUpdate') !!}
                </div>
            @endif
            @if (\Session::has('successDelete'))
                <div class="alert alert-success fade in" style="width: 100%">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! \Session::get('successDelete') !!}
                </div>
            @endif
            @if (\Session::has('error'))
                <div class="alert alert-success fade in" style="width: 100%">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! \Session::get('error') !!}
                </div>
            @endif
            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="no-bd">
                                <tr>
                                    <th class="text-center"><strong>Идентификатор</strong></th>
                                    <th class="text-center"><strong>Заголовок</strong></th>
                                    <th class="text-center"><strong>Slug</strong></th>
                                    <th class="text-center"><strong>Родитель</strong></th>
{{--                                    <th class="text-center"><strong>Cкидка</strong></th>--}}
                                    <th class="text-center"><strong>Действия</strong></th>
                                </tr>
                                </thead>
                                <tbody class="no-bd-y">
                                @if($categories->count())
                                    @foreach($categories as $category)
                                        <tr>
                                            <td class="text-center">{{'#'.$category->id}}</td>
                                            <td class="text-center">{{$category->title}}</td>
                                            <td class="text-center">{{$category->slug}}</td>
                                            <td class="text-center">
                                                @if($category->parent_id == null)
                                                    Null
                                                @else
                                                    {{$category->parent->title}}
                                                @endif
                                            </td>
{{--                                            <td class="text-center">--}}
{{--                                                @if($category->discount_id == null)--}}
{{--                                                    Не задано--}}
{{--                                                @else--}}
{{--                                                    {{$category->discount->title}}--}}
{{--                                                @endif--}}
{{--                                            </td>--}}
                                            <td class="text-center">
                                                <a href="{{route('admin.categories.edit',$category->id)}}" class="edit btn btn-sm btn-default" style="margin-top: 5px"><i class="fa fa-pencil"></i> Редактировать</a>
                                                <form action="{{route('admin.categories.destroy',$category->id)}}" method="POST" style="display: inline-block;margin-bottom: 5px">
                                                    @csrf
                                                    <button type="submit" class="edit btn btn-sm btn-default" data-toggle="tooltip" data-placement="top">
                                                        <i class="fa fa-times-circle"></i> Удалить
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            {{ $categories->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection