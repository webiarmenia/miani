@extends('layouts.appmiani')
@section('title')
    <title>Редактирование категория - {{$item->title}}</title>
@endsection
@section('css')
    <link href="{{asset('plugins/datetimepicker/jquery.datetimepicker.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/pickadate/themes/default.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/pickadate/themes/default.date.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/pickadate/themes/default.time.css')}}" rel="stylesheet">
    <style>
        .modal .active_image{
            border:5px solid #00e7ff85;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong class="wi-page-title" id="active-categories-page">Редактирование категория - </strong>{{$item->title}}</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <form id="form1" class="form-horizontal"  action="{{route('admin.categories.update',$item->id)}}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Заголовок <span class="asterisk">*</span>
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text" data-parsley-minlength="3" class="form-control @error('title') parsley-error @enderror"
                                               data-parsley-id="1143" name="title"
                                               value="{{ old( 'title', $item->title) }}">
                                        <ul class="parsley-errors-list" id="parsley-id-1143">
                                            @error('title')
                                            <li>{{ $message }}</li>
                                            @enderror
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Slug <span class="asterisk">*</span>
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text" data-parsley-minlength="3" class="form-control @error('slug') parsley-error @enderror"
                                               data-parsley-id="1143" name="slug"
                                               value="{{ old( 'slug', $item->slug) }}">
                                        <ul class="parsley-errors-list" id="parsley-id-1143">
                                            @error('slug')
                                            <li>{{ $message }}</li>
                                            @enderror
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Родитель
                                    </label>
                                    <div class="col-sm-9">
                                        <select name="parent_id" class="form-control">
                                            <option value="">No parent</option>
                                            @foreach($categories as $category)
                                                <option value="{{$category->id}}"
                                                        {{ old('parent_id', $item->parent_id) == $category->id ? 'selected' : '' }}
                                                        {{ $category->id == $item->id ? 'style=display:none': '' }}
                                                        {{ $category->parent_id == $item->id ? 'style=display:none': '' }}
                                                >{{$category->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
{{--                                <div class="form-group">--}}
{{--                                    <label class="col-sm-3 control-label">Значок <span class="asterisk">*</span>--}}
{{--                                    </label>--}}
{{--                                    <div class="col-sm-9">--}}
{{--                                        <input type="text" class="form-control @error('icon') parsley-error @enderror" name="icon"--}}
{{--                                               value="{{ old('icon')? old('icon'): $item->icon }}">--}}
{{--                                        <ul class="parsley-errors-list" id="parsley-id-1143">--}}
{{--                                            @error('icon')--}}
{{--                                            <li>{{ $message }}</li>--}}
{{--                                            @enderror--}}
{{--                                        </ul>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="col-sm-3 control-label">Cкидка--}}
{{--                                    </label>--}}
{{--                                    <div class="col-sm-9">--}}
{{--                                        <select name="discount_id" class="form-control">--}}
{{--                                            <option value="">No discount</option>--}}
{{--                                            @foreach($discounts as $discount)--}}
{{--                                                <option value="{{$discount->id}}" {{ $discount->id == $item->discount_id ? 'selected': '' }}{{ $discount->id == $item->id ? 'style=display:none': '' }}>{{$discount->name}}</option>--}}
{{--                                            @endforeach--}}
{{--                                        </select>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="col-sm-3 control-label">Картинка</label>--}}
{{--                                    <div id="pop" class="col-sm-9">--}}
{{--                                        <button  id="imageresource" class="btn btn-primary" style="margin-bottom: 20px;" type="button">Image</button>--}}
{{--                                        <div   class="selected-image">--}}
{{--                                            <img src="{{ asset($media->path. $media->original_name.'_'.'small'.'.' .$media->ext) }}" data_id="{{$media->id}}" class = 'image' width="200" height="200">--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                                <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">--}}
{{--                                    <div class="modal-dialog" style="width: 60%; height: 50%;">--}}
{{--                                        <div class="modal-content">--}}
{{--                                            <div class="modal-header">--}}
{{--                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>--}}
{{--                                                <h4 class="modal-title" id="myModalLabel">Image preview</h4>--}}
{{--                                            </div>--}}
{{--                                            <div class="modal-body text-center">--}}
{{--                                                @foreach($media_cover as $key =>$value)--}}
{{--                                                    <img src="{{ asset($value->path. $value->original_name.'_'.'small'.'.' .$value->ext) }}" data_id="{{$value->id}}" class = 'image' width="200" height="200">--}}
{{--                                                @endforeach--}}
{{--                                            </div>--}}
{{--                                            <div class="row">--}}
{{--                                                <div class="col-md-12">--}}
{{--                                                    {{ $media_cover->links() }}--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                            <div class="modal-footer">--}}
{{--                                                <button class="btn btn-primary button" type="button" data-dismiss="modal">button</button>--}}
{{--                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                                <div class="col-sm-9 col-sm-offset-3">
                                    <div class="pull-right">
                                        <button id="submit" type="submit" class="btn btn-success m-t-10" name="button" value="update">
                                            <i class="fa fa-check"></i>Сахранить
                                        </button>
                                        <a href="{{route('admin.categories')}}" class="btn btn-default m-r-10 m-t-10">
                                            <i class="fa fa-reply"></i> Назад
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
{{--@section('javascript')--}}
{{--    <script>--}}
{{--        $("#pop").on("click", function() {--}}
{{--            $('#imagepreview').attr('src', $('#imageresource').attr('src'));--}}
{{--            $('#imagemodal').modal('show');--}}
{{--        });--}}
{{--        $(".image").on("click", function(e) {--}}
{{--            if ($(this).hasClass('active_image')) {--}}
{{--                $(this).removeClass('active_image');--}}
{{--            } else {--}}
{{--                $('img').removeClass('active_image');--}}
{{--                $(this).addClass('active_image');--}}
{{--            }--}}
{{--            if ($(this).hasClass('active_image')) {--}}
{{--                $(".button").on("click", function(e) {--}}
{{--                    var $image = $(this).clone();--}}
{{--                    $('.selected-image').html($image);--}}
{{--                    if('.selected-image img'){--}}
{{--                        var $att =$('.selected-image img').attr('data_id');--}}
{{--                        $('.selected-image').append( "<input>" );--}}
{{--                        $('.selected-image input').attr('name','cover_id').attr('type','hidden') .attr('value',$att);--}}
{{--                    }--}}
{{--                }.bind(this));--}}
{{--            }--}}
{{--        });--}}
{{--    </script>--}}
{{--@endsection--}}