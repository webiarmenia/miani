@extends('layouts.appmiani')
@section('title')
    <title>Редактирование Коллекции - {{$item->name}}</title>
@endsection
@section('css')
    <link href="{{asset('plugins/datetimepicker/jquery.datetimepicker.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/pickadate/themes/default.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/pickadate/themes/default.date.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/pickadate/themes/default.time.css')}}" rel="stylesheet">
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" media="all">
    <link href="{{asset('plugins\bootstrap-switch\bootstrap-switch.css')}}" rel="stylesheet">
    <meta name="_token" content="{{csrf_token()}}"/>

@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong class="wi-page-title" id="active-collections-page">Коллекция</strong> {{$item->name}}</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <form id="form1" class="form-horizontal" action="{{route('admin.collections.store')}}"
                                  method="POST">
                                @csrf
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Название <span class="asterisk">*</span>
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text" data-parsley-minlength="3"
                                               class="form-control @error('name') parsley-error @enderror"
                                               data-parsley-id="1143" name="name"
                                               value="{{old('name')? old('name'): $item->name}}"
                                        >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Скидка</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" title="Choose one" name="discount"
                                                id="discount"
                                                @if($item->deletable == "no")
                                                disabled
                                                @endif
                                        >
                                            @if($discounts->count())
                                                <option value="{{null}}" selected>Нет Скидки</option>
                                                @foreach($discounts as $discount)
                                                    <option value="{{$discount->id}}" {{($item->discount == $discount->id)?'selected':''}}>{{$discount->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Slug <span class="asterisk">*</span>
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text" data-parsley-minlength="3"
                                               class="form-control @error('slug') parsley-error @enderror"
                                               data-parsley-id="1143" name="slug"
                                               value="{{old('slug')? old('slug'): $item->slug}}"
                                        >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Продукты <span class="asterisk">*</span>
                                    </label>
                                    @if($item->deletable != "no")
                                        <div class="col-sm-9">
                                            <select class="js-example-basic-multiple" multiple
                                                    title="Choose one or more" name="details_selected[]">
                                                @if($products->count())
                                                    @foreach($products as $k => $product)
                                                        @foreach($product->productDetail as $k => $productDetail)
                                                            <option value="{{$productDetail->id}}" {{(in_array($productDetail->id,$selected)?'selected':'')}}>
                                                                {{'#'.$productDetail->id.'. '.$product->name.' - '.$productDetail->size.' - '.$Detail[$productDetail->color]}}
                                                            </option>
                                                        @endforeach
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    @else
                                        <div class="col-sm-9">
                                            <table>
                                            @foreach($products as $k => $product)
                                                <tr>
                                                    <td colspan="3" class="detail">
                                                        <strong>{{$product->name}}</strong>
                                                    </td>
                                                </tr>
                                                @foreach($product->productDetail as $k => $productDetail)
                                                    <tr>
                                                        <td class="detail">
                                                                <span style="margin-left: 30px">
                                                                    #<strong> {{$productDetail->id}}. </strong> Размер - <strong>{{$productDetail->size}}</strong> | Цвет - <strong> {{$Detail[$productDetail->color]}} </strong>
                                                                </span>
                                                         </td>
                                                    </tr>
                                                @endforeach
                                            @endforeach
                                            </table>
                                        </div>
                                    @endif

                                    {{--                                    <div class="col-sm-9">--}}
                                    {{--                                        <div class="row">--}}
                                    {{--                                            <div class="col-md-12">--}}
                                    {{--                                                <div class="row">--}}
                                    {{--                                                    <div class="col-md-12 col-sm-12 col-xs-12" id="product_data">--}}
                                    {{--                                                        @include('admin.collections.product_data')--}}
                                    {{--                                                    </div>--}}
                                    {{--                                                </div>--}}
                                    {{--                                            </div>--}}
                                    {{--                                        </div>--}}
                                    {{--                                    </div>--}}
                                    {{--                                    <hr>--}}
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Описание <span class="asterisk">*</span></label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="description" value = "{{$item->description}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Порядок <span class="asterisk">*</span></label>
                                    <div class="col-sm-9">
                                        <input @if($item->deletable == "no") disabled @endif type="number" name="order" class="form-control" min="1" value="{{$item->order}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3"></div>
                                    <div class="col-sm-9">
                                        <ul class="alert alert-danger alert-danger-form"
                                            style="display:none;float: left">

                                        </ul>
                                    </div>
                                </div>
                                {{--                                <input type="hidden" name="details_selected" value="{{implode(",", $selected)}}">--}}

                                    <div class="col-sm-9 col-sm-offset-3">
                                        <div class="pull-right">
                                            <a href="{{route('admin.collections')}}" class="btn btn-default m-r-10 m-t-10">
                                                <i class="fa fa-reply"></i> Назад
                                            </a>
                                            <button id="submit" type="submit" class="btn btn-success m-t-10"><i class="fa fa-check"></i> Обновить
                                            </button>
                                        </div>
                                    </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('javascript')
    <script src="{{asset('js/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/bootstrap-datepicker.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/classie.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/modalEffects.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/application.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins\bootstrap-switch\bootstrap-switch.js')}}"></script>
    <script>
        $(".top").bootstrapSwitch();
    </script>

    <script>

        $(document).ready(function () {
            jQuery('form').submit(function (e) {
                e.preventDefault();
                jQuery.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                jQuery.ajax({
                    url: "{{ route('admin.collections.update',$item->id) }}",
                    method: 'post',
                    data: $('form').serialize(),
                    success: function (data) {
                        $(".alert-danger-form").empty();
                        if (data.errors) {
                            console.log(data);
                            jQuery.each(data.errors, function (key, value) {
                                jQuery('.alert-danger-form').show();
                                jQuery('.errors').show();
                                jQuery('.alert-danger-form').append('<li class="error-li">' + value + '</li>');

                            });
                        } else if (data.created) {
                            window.location.href = '{{route('admin.collections')}}';

                        }
                    },
                });
            });
            {{--Array.prototype.remove = function () {--}}
            {{--    var what, a = arguments, L = a.length, ax;--}}
            {{--    while (L && this.length) {--}}
            {{--        what = a[--L];--}}
            {{--        while ((ax = this.indexOf(what)) !== -1) {--}}
            {{--            this.splice(ax, 1);--}}
            {{--        }--}}
            {{--    }--}}
            {{--    return this;--}}
            {{--};--}}

            {{--var array = [{{implode(",", $selected)}}];--}}


            {{--$(document).on('click', '.pagination a', function (event) {--}}
            {{--    event.preventDefault();--}}
            {{--    var page = $(this).attr('href').split('page=')[1];--}}

            {{--    var selected = $('input[name="details_selected"]').val();--}}
            {{--    fetch_data(page, selected);--}}
            {{--});--}}

            {{--function fetch_data(page, selected) {--}}
            {{--    $.ajax({--}}
            {{--        url: "/admin/collections/fetch_data?page=" + page + "&selected=" + selected,--}}
            {{--        success: function (data) {--}}
            {{--            $('#product_data').html(data);--}}
            {{--            $(".top").bootstrapSwitch();--}}
            {{--        }--}}
            {{--    });--}}
            {{--}--}}

            {{--$(document).on('switchChange.bootstrapSwitch', '.top', function (event, state) {--}}
            {{--    if (this.checked) {--}}
            {{--        array.push(parseInt($(this).val()));--}}
            {{--    } else {--}}
            {{--        array.remove(parseInt($(this).val()));--}}
            {{--    }--}}
            {{--    $('input[name="details_selected"]').attr('value', array);--}}
            {{--    console.log(array);--}}
            {{--});--}}
            $('.js-example-basic-multiple').select2();
        });
    </script>
@endsection
