@extends('layouts.appmiani')
@section('title')
    <title>Коллекции</title>
@endsection
@section('content')
    <div class="m-b-20 clearfix">
        <div class="page-title pull-left">
            <h3 class="pull-left"><strong class="wi-page-title" id="active-collections-page">Коллекции</strong></h3>
        </div>
        <div class="pull-right">
            <a href="{{route('admin.collections.create')}}" class="btn btn-success m-t-10"><i class="fa fa-plus p-r-10"></i>Дабавить Коллекцию</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if (\Session::has('success'))
                <div class="alert alert-success fade in" style="width: 100%">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! \Session::get('success') !!}
                </div>
            @endif
            @if (\Session::has('error'))
                <div class="alert alert-danger fade in" style="width: 100%">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! \Session::get('error') !!}
                </div>
            @endif
            @if($collections->count() && $collections[0]->deletable != 'no')
                 <div class="alert alert-danger fade in" style="width: 100%">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                     Для корректной работы пожалуйста удалите все коллекции, и добавьте их снова
                </div>
            @endif
            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="no-bd">
                                <tr>
                                    <th class="text-center"><strong>№</strong></th>
                                    <th class="text-center"><strong>Название</strong></th>
                                    <th class="text-center"><strong>Slug</strong></th>
                                    <th class="text-center"><strong>Скидка</strong></th>
                                    <th class="text-center"><strong>Порядок</strong></th>
                                    <th class="text-center"><strong>Действия</strong></th>
                                </tr>
                                </thead>
                                <tbody class="no-bd-y">
                                @if($collections->count())
                                    @foreach($collections as $k => $collection)
                                        <tr>
                                            <td class="text-center">{{$k+1}}</td>
                                            <td class="text-center">{{$collection->name}}</td>
                                            <td class="text-center">{{$collection->slug}}</td>
                                            <td class="text-center">{{(isset($collection->discounts->name))? $collection->discounts->name: 'нет скидки'}}</td>
                                            <td class="text-center">{{$collection->order}}</td>
                                            <td class="text-center">
                                                @if($collection->deletable == "no")
                                                <a href="{{route('admin.collections.edit',$collection->id)}}" class="edit btn btn-sm btn-default" style="margin-top: 5px"><i class="fa fa-pencil"></i> Редактировать</a>
                                                @else
                                                    <a href="{{route('admin.collections.edit',$collection->id)}}" class="edit btn btn-sm btn-default" style="margin-top: 5px"><i class="fa fa-pencil"></i> Редактировать</a>
                                                    <form action="{{route('admin.collections.destroy',$collection->id)}}" method="POST" style="display: inline-block;margin-bottom: 5px">
                                                        @csrf
                                                        <button type="submit" class="edit btn btn-sm btn-default" data-toggle="tooltip" data-placement="top">
                                                            <i class="fa fa-times-circle"></i> Удалить
                                                        </button>
                                                    </form>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                            <div class="row">
                                <div class="col-md-12">
                                    {{ $collections->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
