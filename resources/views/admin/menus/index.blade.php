@extends('layouts.appmiani')
@section('title')
    <title>Меню</title>
@endsection
@section('content')
    <div class="m-b-20 clearfix">
        <div class="page-title pull-left">
            <h3 class="pull-left"><strong class="wi-page-title" id="active-menus-page">Меню</strong></h3>
        </div>
        <div class="pull-right">
            <a href="{{route('admin.menus.create')}}" class="btn btn-success m-t-10"><i class="fa fa-plus p-r-10"></i> Добавить меню</a>
        </div>
{{--        <div class="pull-right">--}}
{{--            <a href="{{route('admin.menus.sort')}}" class="btn btn-success m-t-10"><i class="fa fa-plus p-r-10"></i>Сортировка</a>--}}
{{--        </div>--}}
    </div>
    <div class="row">
        <div class="col-md-12">
            @if (\Session::has('successCreate'))
                <div class="alert alert-success fade in" style="width: 100%">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! \Session::get('successCreate') !!}
                </div>
            @endif
            @if (\Session::has('successUpdate'))
                <div class="alert alert-success fade in" style="width: 100%">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! \Session::get('successUpdate') !!}
                </div>
            @endif
            @if (\Session::has('successDeleteYes'))
                <div class="alert alert-success fade in" style="width: 100%">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! \Session::get('successDeleteYes') !!}
                </div>
            @endif
            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="no-bd">
                                <tr>
                                    <th class="text-center"><strong>Индентификатор</strong></th>
                                    <th class="text-center"><strong>Заголовок</strong></th>
{{--                                    <th class="text-center"><strong>Родитель</strong></th>--}}
{{--                                    <th class="text-center"><strong>Позиция</strong></th>--}}
                                    <th class="text-center"><strong>Действия</strong></th>
                                </tr>
                                </thead>
                                <tbody class="no-bd-y">
                                @if($menus->count())
                                    @foreach($menus as $menu)
                                        <tr>
                                            <td class="text-center">{{$menu->id}}</td>
                                            <td class="text-center">{{$menu->title}}</td>
{{--                                            <td class="text-center">--}}
{{--                                                @if($menu->parent_id == null)--}}
{{--                                                    Null--}}
{{--                                                @else--}}
{{--                                                    {{$menu->parent->title}}--}}
{{--                                                @endif--}}
{{--                                            </td>--}}
{{--                                            <td class="text-center">{{$menu->position}}</td>--}}
                                            <td class="text-center">
                                                <a href="{{route('admin.menus.edit',$menu->id)}}" class="edit btn btn-sm btn-default" style="margin-top: 5px"><i class="fa fa-pencil"></i> Редактировать</a>
                                                <form action="{{route('admin.menus.destroy',$menu->id)}}" method="POST" style="display: inline-block;margin-bottom: 5px">
                                                    @csrf
                                                    <button type="submit" class="edit btn btn-sm btn-default" data-toggle="tooltip" data-placement="top">
                                                        <i class="fa fa-times-circle"></i> Удалить
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            {{ $menus->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection