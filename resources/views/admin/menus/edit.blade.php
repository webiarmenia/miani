@extends('layouts.appmiani')
@section('title')
    <title>Редактирование меню - {{$item->title}}</title>
@endsection
@section('css')
    <link href="{{asset('plugins/datetimepicker/jquery.datetimepicker.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/pickadate/themes/default.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/pickadate/themes/default.date.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/pickadate/themes/default.time.css')}}" rel="stylesheet">
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong class="wi-page-title" id="active-menus-page">Редактирование меню - </strong>{{$item->title}}</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <form id="form1" class="form-horizontal"  action="{{route('admin.menus.update',$item->id)}}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Заголовок <span class="asterisk">*</span>
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text" data-parsley-minlength="3" class="form-control @error('title') parsley-error @enderror"
                                               data-parsley-id="1143" name="title"
                                               value="{{ old( 'title', $item->title) }}">
                                        <ul class="parsley-errors-list" id="parsley-id-1143">
                                            @error('title')
                                            <li>{{ $message }}</li>
                                            @enderror
                                        </ul>
                                    </div>
                                </div>
{{--                                <div class="form-group">--}}
{{--                                    <label class="col-sm-3 control-label">Родитель индентификатор--}}
{{--                                    </label>--}}
{{--                                    <div class="col-sm-9">--}}
{{--                                        <select name="parent_id" class="form-control">--}}
{{--                                            <option value="">No parent</option>--}}
{{--                                            @foreach($menus as $menu)--}}
{{--                                                <option value="{{$menu->id}}" {{ $menu->id == $item->parent_id ? 'selected': '' }}{{ $menu->id == $item->id ? 'style=display:none': '' }}>{{$menu->title}}</option>--}}
{{--                                            @endforeach--}}
{{--                                        </select>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="col-sm-3 control-label">Позиция <span class="asterisk">*</span>--}}
{{--                                    </label>--}}
{{--                                    <div class="col-sm-9">--}}
{{--                                        <select name="position" class="form-control">--}}
{{--                                            <option value="1" {{  $item->position == 1 ? 'selected': '' }}>Header menu</option>--}}
{{--                                            <option value="2" {{  $item->position == 2 ? 'selected': '' }}>Footer menu</option>--}}
{{--                                        </select>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Страница <span class="asterisk">*</span></label>

{{--                                        <label class="col-sm-3 control-label">Страница <span class="asterisk">*</span>--}}
{{--                                    </label>--}}
                                    <div class="col-sm-9">
                                        <select name="address" class="form-control @error('address') parsley-error @enderror">
                                            @foreach($pages as $page)
                                                <option value="{{$page->id}}" {{ old('address', $item->address) == $page->id ? 'selected' : '' }}
{{--                                                        {{ old('address') == $page->id ? 'selected' : '' }}--}}
{{--                                                        {{ $page->id == $item->address ? 'selected': '' }}--}}
                                                >{{$page->title}}</option>
                                            @endforeach
                                        </select>
                                        <ul class="parsley-errors-list" id="parsley-id-1143">
                                            @error('address')
                                            <li>{{ $message }}</li>
                                            @enderror
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Slug <span class="asterisk">*</span>
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text" data-parsley-minlength="3" class="form-control @error('slug') parsley-error @enderror"
                                               data-parsley-id="1143" name="slug"
                                               value="{{ old( 'slug', $item->slug) }}"
                                        >
                                        <ul class="parsley-errors-list" id="parsley-id-1143">
                                            @error('slug')
                                            <li>{{ $message }}</li>
                                            @enderror
                                        </ul>
                                    </div>
                                </div>
{{--                                <div class="form-group">--}}
{{--                                    <label class="col-sm-3 control-label">Slug <span class="asterisk">*</span>--}}
{{--                                    </label>--}}
{{--                                    <div class="col-sm-9">--}}
{{--                                        <input type="text" class="form-control @error('slug') parsley-error @enderror" name="slug"--}}
{{--                                               value="{{ old('slug')? old('slug'): $item->slug }}">--}}
{{--                                        <ul class="parsley-errors-list" id="parsley-id-1143">--}}
{{--                                            @error('slug')--}}
{{--                                            <li>{{ $message }}</li>--}}
{{--                                            @enderror--}}
{{--                                        </ul>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                                <div class="col-sm-9 col-sm-offset-3">
                                    <div class="pull-right">
                                        <button id="submit" type="submit" class="btn btn-success m-t-10" name="button" value="update">
                                            <i class="fa fa-check"></i>Сахранить
                                        </button>
                                        <a href="{{route('admin.menus')}}" class="btn btn-default m-r-10 m-t-10">
                                            <i class="fa fa-reply"></i> Назад
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection