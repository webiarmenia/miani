@extends('layouts.appmiani')
@section('title')
    <title>FAQ</title>
@endsection
@section('content')
    <div class="m-b-20 clearfix">
        <div class="page-title pull-left">
            <h3 class="pull-left"><strong class="wi-page-title" id="active-faqs-page">FAQ</strong></h3>
        </div>
        <div class="pull-right">
            <a href="{{route('admin.faqs.create')}}" class="btn btn-success m-t-10"><i class="fa fa-plus p-r-10"></i> Дабавить FAQ</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if (\Session::has('success'))
                <div class="alert alert-success fade in" style="width: 100%">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! \Session::get('success') !!}
                </div>
            @endif
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
                            <table id="products-table" class="table table-tools table-hover">
                                <thead class="no-bd">
                                <tr>
                                    <th class="text-center"><strong>№</strong></th>
                                    <th class="text-center"><strong>Вопрос</strong></th>
                                    <th class="text-center"><strong>Действия</strong></th>
                                </tr>
                                </thead>
                                <tbody class="no-bd-y">
                                @foreach($faqs as $key => $faq)
                                    <tr>
                                        <td  class="text-center">{{$key+1}}</td>
                                        <td class="text-center">{{$faq->question}}</td>
                                        <td class="text-center">
                                            <a href="{{ route('admin.faqs.edit',$faq->id)}}" class="edit btn btn-sm btn-default"><i class="fa fa-pencil"></i> Редактировать</a>
                                            <form action="{{route('admin.faqs.destroy',$faq->id)}}" method="POST" style="display: inline-grid;">
                                                @csrf
                                                <button type="submit" class="delete btn btn-sm btn-default"><i class="fa fa-times-circle"></i> Удалить </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            {{ $faqs->links() }}
        </div>
    </div>
@endsection
