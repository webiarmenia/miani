@extends('layouts.appmiani')
@section('title')
    <title>Новая FAQ</title>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading" >
                    <h3 class="panel-title"><strong class="wi-page-title" id="active-faqs-page">Новая FAQ</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <form method="post" action="{{ route('admin.faqs.store') }}" id="form1" class="form-horizontal" data-parsley-validate >
                                @csrf
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Вопрос <span class="asterisk">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" data-parsley-minlength="3" class="form-control @error('question') parsley-error @enderror" data-parsley-id="1143" name="question" value="{{ old('question') }}">
                                        <ul class="parsley-errors-list" id="parsley-id-1143">
                                            @error('question')
                                            <li>{{ $message }}</li>
                                            @enderror
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Ответ</label>
                                    <div class="col-sm-9">
                                        <textarea type="text"  data-parsley-minlength="3"  name="answer" id="answer" data-parsley-id="1143" class="form-control">
                                            {{ old('answer') }}
                                        </textarea>
                                        <ul class="parsley-errors-list" id="parsley-id-1143">
                                            @error('answer')
                                            <li>{{ $message }}</li>
                                            @enderror
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-sm-9 col-sm-offset-3">
                                    <div class="pull-right">
                                        <a href="{{route('admin.faqs')}}" class="btn btn-default m-r-10 m-t-10">
                                            <i class="fa fa-reply"></i> Назад
                                        </a>
                                        <button id="submit" type="submit" class="btn btn-success m-t-10"><i class="fa fa-check"></i> Дабавить
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.config.autoParagraph = false;
        CKEDITOR.config.filebrowserBrowseUrl  = '/laravel-filemanager';
        CKEDITOR.replace( 'answer');
    </script>
@endsection