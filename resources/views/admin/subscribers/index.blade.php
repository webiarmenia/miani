@extends('layouts.appmiani')
@section('title')
    <title>Подписчики</title>
@endsection
@section('content')
    <div class="m-b-20 clearfix">
        <div class="page-title pull-left">
            <h3 class="pull-left"><strong class="wi-page-title" id="active-subscribers-page">Подписчики</strong></h3>
        </div>
        <div class="pull-right" style="padding: 10px">
            <a href="{{route('admin.messages.create')}}" class="btn btn-success m-t-10"><i class="fa fa-plus p-r-10"></i>Написать сообщение</a>
        </div>
        <div class="pull-right" style="padding: 10px">
            <a href="{{route('admin.messages')}}" class="btn btn-success m-t-10"><i class="glyph-icon flaticon-pages"></i> Сообщения</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
                            <table id="products-table" class="table table-tools table-hover">
                                <thead class="no-bd">
                                <tr>
                                    <th class="text-center"><strong>№</strong></th>
                                    <th class="text-center"><strong>Эл.адрес</strong></th>
                                    <th class="text-center"><strong>Статус</strong></th>
                                </tr>
                                </thead>
                                <tbody class="no-bd-y">
                                @if($subscribers->count())
                                @foreach($subscribers as $k => $subscriber)
                                    <tr>
                                        <td  class="text-center">{{$k +1}}</td>
                                        <td class="text-center">{{$subscriber->email}}</td>
                                        <td class="text-center">
                                                @if($subscriber->active == true)
                                                    <span class="table-active"><button type="submit" class="btn btn-success  btn-sm my-0 ">Активный</button></span>
                                                @else
                                                    <span class="table-active"><button type="submit" class="btn btn-danger  btn-sm my-0">Неактивный</button></span>
                                                @endif
                                        </td>
                                    </tr>
                                @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            {{ $subscribers->links() }}
        </div>
    </div>
@endsection
