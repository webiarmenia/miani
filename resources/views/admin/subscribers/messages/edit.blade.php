@extends('layouts.appmiani')
@section('title')
    <title>Редактирование сообщение - {{$item->subject}}</title>
@endsection
@section('css')
    <link href="{{asset('plugins/datetimepicker/jquery.datetimepicker.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/pickadate/themes/default.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/pickadate/themes/default.date.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/pickadate/themes/default.time.css')}}" rel="stylesheet">
@endsection
@section('content')
    <input type="hidden" id="refreshed" value="no">

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong class="wi-page-title" id="active-subscribers-page">Редактирование сообщение - </strong>{{$item->subject}}</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <form id="form1" class="form-horizontal"  action="{{route('admin.messages.update',$item->id)}}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Тема <span class="asterisk">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" data-parsley-minlength="3" class="form-control @error('subject') parsley-error @enderror" name="subject" data-parsley-id="1143"
                                               value="{{ old( 'subject', $item->subject) }}">
                                        <ul class="parsley-errors-list" id="parsley-id-1143">
                                            @error('subject')
                                            <li>{{ $message }}</li>
                                            @enderror
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Содержание<span class="asterisk">*</span></label>
                                    <div class="col-sm-9">
                                        <textarea type="text"  data-parsley-minlength="3"  name="messageArea" id="message" class="form-control @error('messageArea') parsley-error @enderror" data-parsley-id="1143">
                                            {{ old( 'messageArea', $item->message) }}
                                        </textarea>
                                        <ul class="parsley-errors-list" id="parsley-id-1143">
                                            @error('messageArea')
                                            <li>{{ $message }}</li>
                                            @enderror
                                        </ul>
                                    </div>
                                </div>
{{--                                <div class="form-group">--}}
{{--                                    <label class="col-sm-3 control-label">Тема <span class="asterisk">*</span></label>--}}
{{--                                    <div class="col-sm-9">--}}
{{--                                        <input type="text" data-parsley-minlength="3" class="form-control @error('subject') parsley-error @enderror" name="subject" data-parsley-id="1143"--}}
{{--                                               value=" {{old('subject')? old('subject'):$item->subject}}">--}}
{{--                                        <ul class="parsley-errors-list" id="parsley-id-1143">--}}
{{--                                            @error('subject')--}}
{{--                                            <li>{{ $message }}</li>--}}
{{--                                            @enderror--}}
{{--                                        </ul>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                                <div class="col-sm-9 col-sm-offset-3">
                                    <div class="pull-right">
                                        <button id="submit" type="submit" class="btn btn-success m-t-10" name="button" value="save">
                                            <i class="fa fa-check"></i>Сахранить
                                        </button>
                                        <a href="{{route('admin.messages')}}" class="btn btn-default m-r-10 m-t-10">
                                            <i class="fa fa-reply"></i> Назад
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.config.autoParagraph = false;
        CKEDITOR.config.filebrowserBrowseUrl  = '/laravel-filemanager';
        CKEDITOR.replace( 'message');
    </script>
@endsection
