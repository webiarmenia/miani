@extends('layouts.appmiani')
@section('title')
    <title>Сообщения</title>
@endsection
@section('content')
    <div class="m-b-20 clearfix">
        <div class="page-title pull-left">
            <h3 class="pull-left"><strong class="wi-page-title" id="active-subscribers-page">Сообщения</strong></h3>

{{--            <h1 class="pull-left"><strong id="active-subscribers-page">Сообщения</strong></h1>--}}
        </div>
        <div class="col-md-12">
            @if (\Session::has('successCreate'))
                <div class="alert alert-success fade in" style="width: 100%">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! \Session::get('successCreate') !!}
                </div>
            @endif
            @if (\Session::has('successUpdate'))
                <div class="alert alert-success fade in" style="width: 100%">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! \Session::get('successUpdate') !!}
                </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
                            <table id="products-table" class="table table-tools table-hover">
                                <thead class="no-bd">
                                <tr>
                                    <th class="text-center"><strong>№</strong></th>
                                    <th class="text-center"><strong>Тема</strong></th>
                                    <th class="text-center"><strong>Действия</strong></th>
                                </tr>
                                </thead>
                                <tbody class="no-bd-y">
                                @if($messages->count())
                                    @foreach($messages as $k => $message)
                                        <tr>
                                            <td  class="text-center">{{$k +1}}</td>
                                            <td class="text-center">{{$message->subject}}</td>
                                            <td class="text-center">
                                                @if($message->sent == true)
                                                    <a href="{{route('admin.messages.view',$message->id)}}" class="edit btn btn-sm btn-default" style="margin-top: 5px"><i class="fa fa-eye"></i> Посмотреть</a>
                                                @else
                                                    <a href="{{route('admin.messages.view',$message->id)}}" class="edit btn btn-sm btn-default" style="margin-top: 5px"><i class="fa fa-eye"></i> Посмотреть</a>
                                                    <a href="{{route('admin.messages.edit',$message->id)}}" class="edit btn btn-sm btn-default" style="margin-top: 5px"><i class="fa fa-pencil"></i> Редактировать</a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table><div class="col-sm-9 col-sm-offset-3">
                                <div class="pull-right">
                                    <a href="{{route('admin.subscribers')}}" class="btn btn-default m-r-10 m-t-10">
                                        <i class="fa fa-reply"></i> Назад
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            {{ $messages->links() }}
        </div>
    </div>
@endsection