@extends('layouts.appmiani')
@section('title')
    <title>Новое Сообщение</title>
@endsection
@section('css')
    <link href="{{asset('plugins/datetimepicker/jquery.datetimepicker.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/pickadate/themes/default.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/pickadate/themes/default.date.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/pickadate/themes/default.time.css')}}" rel="stylesheet">
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong class="wi-page-title" id="active-subscribers-page">Новое Сообщение</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <form id="form1" class="form-horizontal" action="{{route('admin.messages.store')}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Тема <span class="asterisk">*</span>
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control @error('subject') parsley-error @enderror" name="subject" value="{{ old('subject') }}">
                                        <ul class="parsley-errors-list" id="parsley-id-1143">
                                            @error('subject')
                                            <li>{{ $message }}</li>
                                            @enderror
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Сообщение <span class="asterisk">*</span></label>
                                    <div class="col-sm-9">
                                        <textarea type="text" data-parsley-minlength="3"  name="messageArea" id="messageArea" data-parsley-id="1143" class="form-control ckeditor @error('messageArea') parsley-error @enderror">
                                            {{ old('messageArea') }}
                                        </textarea>
                                        <ul class="parsley-errors-list" id="parsley-id-1143">
                                            @error('messageArea')
                                            <li>{{ $message }}</li>
                                            @enderror
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-sm-9 col-sm-offset-3">
                                    <div class="pull-right" style="margin-right: 5px;">
                                        <button type="submit" class="btn btn-danger m-t-10" value="add" name="button"><i class="fa fa-save"></i> Сохранить</button>
                                        <button type="submit" class="btn btn-success m-t-10" value="add_send" name="button"><i class="fa fa-check"></i>Сохранить и отправить</button>
                                        <a href="{{route('admin.subscribers')}}" class="btn btn-default m-r-10 m-t-10">
                                            <i class="fa fa-reply"></i> Назад
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('javascript')
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.config.autoParagraph = false;
        CKEDITOR.config.filebrowserBrowseUrl  = '/laravel-filemanager';
        // CKEDITOR.replace( 'message');
    </script>
    <script type="text/javascript">
        CKEDITOR.replace( 'messageArea',
            {
                customConfig : 'config.js',
                toolbar : 'simple'
            })
    </script>
@endsection