@extends('layouts.appmiani')
@section('title')
    <title>Посмотреть сообщение - {{$item->subject}}</title>
@endsection

@section('content')
    <div class="m-b-20 clearfix">
        <div class="page-title pull-left">
            <h3 class="pull-left"><strong class="wi-page-title" id="active-subscribers-page">Посмотреть сообщение - {{$item->subject}}</strong></h3>
        </div>
    </div>
    <div class="panel-default">
        <div class="panel-body">
            <div class="row">
                <div>
                    <div class="col-sm-3">
                        <span>Тема </span>
                    </div>
                    <div class="col-sm-9">
                        <span>{{$item->subject}}</span>
                    </div>
                </div>
                <div>
                    <div class="col-sm-3">
                        <span>Сообщение </span>
                    </div>
                    <div class="col-sm-9">
                        <span>{!! $item->message !!}</span>
                    </div>
                </div>
                <div class="col-sm-9 col-sm-offset-3">
                    <div class="pull-right">
                        <a href="{{route('admin.messages')}}" class="btn btn-default m-r-10 m-t-10">
                            <i class="fa fa-reply"></i> Назад
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

